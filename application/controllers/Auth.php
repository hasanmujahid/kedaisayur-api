<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Auth extends MY_Controller {

	function __construct()
	{
	   parent::__construct();
	   date_default_timezone_set('Asia/Jakarta');
	}

	public function login()
	{

		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('phone', 'phone', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');

		if ($this->form_validation->run() == TRUE) {

			$checkphone =   $this->db->select('*')
								->from('wilayah_details')
								->where(['phone' => $data['phone']])
								->get()
								->row();

			if($checkphone == null){
				$response['message'] = 'phone tidak ditemukan';
				return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

			}

			$query =   $this->db->select('*')
								->from('wilayah_details')
								->where(['phone' => $data['phone'], 'password' => md5($data['password'])])
								->get()
								->row();

			if($query == null){
				$response['message'] = 'password yang anda masukkan salah';
				return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

			}else if($query->status_aktivasi == 0){

				$response['message'] = 'akun belum di proses';
				return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

			}else if($query->status_aktivasi == 1){

				$response['message'] = 'akun anda sedang di proses';
				return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

			}else if($query->status_aktivasi == -1){

				$response['message'] = 'pendaftaran anda telah di reject';
				return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

			}else if($query->status == 0){

				$response['message'] = 'akun anda telah di hapus';
				return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

			}else{

				$param =   $this->db->select('param_value')
											->where('id', 4)
											->from('params')
											->get()
											->row()
											->param_value;

				$token = md5($data['phone'].rand().date('Y-m-d H:i:s', NOW()));

				$wilayah_details['token'] = $token;
				$wilayah_details['token_expired'] = Carbon::now()->addMinutes($param)->toDateTimeString();
				$wilayah_details['last_login'] = date('Y-m-d H:i:s', NOW());

				$update = $this->db->where(['phone' => $data['phone'],
											'password' => md5($data['password'])])
								   ->update('wilayah_details', $wilayah_details);

				$query =   $this->db->select(['wilayah_details.*', 'wilayah_address_details.id as wilayah_address_details_id', 'ts_grades.plafon_from', 'ts_grades.plafon_to', 'ts_attachments.filename as image'])
								->from('wilayah_details')
								->join('wilayah_address_details', 'wilayah_address_details.wilayah_detail_id = wilayah_details.id AND wilayah_address_details.is_primary = 1 and wilayah_address_details.status=1', 'left')
								->join('ts_grades', 'ts_grades.id = wilayah_details.ts_grade_id', 'left')
								->join('ts_attachments', 'ts_attachments.wilayahdetail_id = wilayah_details.id AND ts_attachments.status = 1 AND ts_attachments.ts_attachment_cat_id = 2', 'left')
								->where(['wilayah_details.phone' => $data['phone'],
										 'wilayah_details.password' => md5($data['password'])])
								->get();
								// ->row();

				//android version code
				$android_version_code = $this->db->get_where('params', array('param_code' => 'TS_ANDROID_VERSION_CODE'))->row();


				$response['status']  = 'success';
				$response['message'] = 'berhasil masuk';
				$response['data'] = $query==null?$query: $query->row();
				$response['base_url_profile'] = $this->base_url_attachment;
				$response['ANDROID_VERSION_CODE'] = $android_version_code->param_value;
			}

		}else{
			$response["message"] = (string) json_encode($this->form_validation->error_array());
			// $response["message"] = $this->form_validation->error_array();

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function logout()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('phone', 'phone', 'required');

		if ($this->form_validation->run() == TRUE) {

				$wilayah_details['token'] = "";

				$update = $this->db->where(['phone' => $data['phone']])
								   ->update('wilayah_details', $wilayah_details);

				$response['status']  = 'success';
				$response['message'] = 'berhasil keluar';

		}else{

			$response["message"] = $this->form_validation->error_array();

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}


	public function forgotPassword()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('phone', 'phone', 'required');

		if ($this->form_validation->run() == TRUE) {

			$query =   $this->db->select('id')
								->from('wilayah_details')
								->where(['phone' => $data['phone']])
								->get();

			if($query->result() == null) {

				$response['message'] = 'phone tidak ditemukan';
				return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

			}else {

				$phone62 = '62'.substr($data['phone'], 1);
				$subject = 'password baru Kedaisayur';
				$activation_code = rand(1111, 9999);

				$keterangan = "Jika anda tidak menerima password baru dalam waktu 1 menit Harap Menghubungi kami.";

			    $detail = "password baru anda [".$activation_code."]";

			    $this->db->trans_begin();
		    	try {

					$sms_queues['phone_number'] = $phone62;
					$sms_queues['subject'] = $subject;
					$sms_queues['detail'] = $detail;
					$sms_queues['keterangan'] = $keterangan;
					$sms_queues['status'] = 1;
					$sms_queues['masking'] = 0;
					$sms_queues['created'] = date('Y-m-d H:i:s', NOW());
					$this->db->insert('sms_queues', $sms_queues);

					$wilayah_details['password'] = md5($activation_code);
					$this->db->where('phone', $data['phone']);
					$this->db->update('wilayah_details', $wilayah_details);


				} catch (Exception $e) {
		    		 $this->db->trans_rollback();
					 $response["message"] = "terjadi kesalahan pada database (rollback)";
		    	}

		    	if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback();
					    $response["message"] = "terjadi kesalahan pada database (rollback)";
				}else {
				        $this->db->trans_commit();
				        $response["status"] = 'success';
				        $response["message"] = 'data berhasil disimpan';
				}

			}

		}else{

			$response["message"] = $this->form_validation->error_array();

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function updateProfile()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('phone', 'phone', 'required');
		$this->form_validation->set_rules('latitude_ts', 'latitude_ts', 'required');
		$this->form_validation->set_rules('longitude_ts', 'longitude_ts', 'required');
		$this->form_validation->set_rules('address_ts', 'address_ts', 'required');
		$this->form_validation->set_rules('g_route_ts', 'g_route_ts', 'required');

		if ($this->form_validation->run() == TRUE) {

			$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

			}

			$query =   $this->db->select('id')
								->from('wilayah_details')
								->where(['phone' => $data['phone']])
								->get();

			if($query->result() == null) {

				$response['message'] = 'phone tidak ditemukan';
				return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

			}else {

				$adm_area_level_1 ='';
				$adm_area_level_2 ='';
				$adm_area_level_3 ='';
				$adm_area_level_4 ='';
				$address = $this->getAddress($data['latitude_ts'], $data['longitude_ts']);

	    		if(count($address) > 1){

					for($i=0; $i<count($address); $i++){

						if($address[$i]['types'][0] == 'administrative_area_level_1'){
							$adm_area_level_1 = $address[$i]['long_name'];
						}
						if($address[$i]['types'][0] == 'administrative_area_level_2'){
							$adm_area_level_2 = $address[$i]['long_name'];
						}
						if($address[$i]['types'][0] == 'administrative_area_level_3'){
							$adm_area_level_3 = $address[$i]['long_name'];
						}
						if($address[$i]['types'][0] == 'administrative_area_level_4'){
							$adm_area_level_4 = $address[$i]['long_name'];
						}

					}
				}

			    $this->db->trans_begin();
		    	try {

					$wilayah_details['g_route'] = $data['g_route_ts'];
					$wilayah_details['adm_area_level_1'] = $adm_area_level_1;
					$wilayah_details['adm_area_level_2'] = $adm_area_level_2;
					$wilayah_details['adm_area_level_3'] = $adm_area_level_3;
					$wilayah_details['adm_area_level_4'] = $adm_area_level_4;
					$wilayah_details['lat'] = $data['latitude_ts'];
					$wilayah_details['lng'] = $data['longitude_ts'];
					$wilayah_details['title'] = $data['name'];
					$wilayah_details['modified_by'] = $session['data']->id;
					$wilayah_details['modified'] = date('Y-m-d H:i:s', NOW());
					$this->db->where('id', $session['data']->id);
					$this->db->update('wilayah_details', $wilayah_details);


				} catch (Exception $e) {
		    		 $this->db->trans_rollback();
					 $response["message"] = "terjadi kesalahan pada database (rollback)";
		    	}

		    	if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback();
					    $response["message"] = "terjadi kesalahan pada database (rollback)";
				}else {
				        $this->db->trans_commit();
				        $response["status"] = 'success';
				        $response["message"] = 'data berhasil disimpan';
				}

			}

		}else{

			$response["message"] = $this->form_validation->error_array();

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function updatePassword()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'name', 'required');
		$this->form_validation->set_rules('newpassword', 'newpassword', 'required');
		$this->form_validation->set_rules('oldpassword', 'oldpassword', 'required');

		if ($this->form_validation->run() == TRUE) {

			$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

			}

			$chekcoldpassword = $this->db->select('*')
										->from('wilayah_details')
										->where(['id' => $session['data']->id, 'password' => md5($data['oldpassword'])])
										->get()
										->row();

			if($chekcoldpassword == null) {

				$response['message'] = 'password lama anda tidak sesuai';
				return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

			}

		    $this->db->trans_begin();
	    	try {

				$wilayah_details['password'] = md5($data['newpassword']);
				$wilayah_details['modified_by'] = $session['data']->id;
				$wilayah_details['modified'] = date('Y-m-d H:i:s', NOW());
				$this->db->where('id', $session['data']->id);
				$this->db->update('wilayah_details', $wilayah_details);


			} catch (Exception $e) {
	    		 $this->db->trans_rollback();
				 $response["message"] = "terjadi kesalahan pada database (rollback)";
	    	}

	    	if ($this->db->trans_status() === FALSE){
				    $this->db->trans_rollback();
				    $response["message"] = "terjadi kesalahan pada database (rollback)";
			}else {
			        $this->db->trans_commit();
			        $response["status"] = 'success';
			        $response["message"] = 'data berhasil disimpan';
			}

		}else{

			$response["message"] = $this->form_validation->error_array();

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function check_android_version_code()
	{
		//$obj = new StdClass();
		$response = array("status" => 'failed', "apps_version_code" => "", "ts_android_version_code" => "", "force_update" => "", "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('apps_version_code', 'apps_version_code', 'required');

		if ($this->form_validation->run() == TRUE) {

			//android version code
			$forceUpdate = 1;
			$current_version_code = $this->db->get_where('params', array('param_code' => 'TS_ANDROID_VERSION_CODE'))->row()->param_value;
			$apps_version_code = $data["apps_version_code"];

			if($apps_version_code >= $current_version_code) {
				$forceUpdate = 0;
				$response["status"] = "success";
			}
			else {
				$response["status"] = "success";
				$response["message"] = "Kami telah memperbaharui aplikasi, silahkan perbaharui aplikasi anda di google playstore";
			}

			$response["apps_version_code"] = $apps_version_code;
			$response["ts_android_version_code"] = $current_version_code;
			$response["force_update"] = $forceUpdate;

		}else{

			$response["message"] = $this->form_validation->error_array();

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

	}

}
