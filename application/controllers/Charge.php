<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Charge extends MY_Controller {

	
	function __construct()
	{
	   parent::__construct();  
	   date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{	
		$SERVER_KEY = $this->SERVER_KEY;
		$ENDPOINT = $this->ENDPOINT;

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		// $data['custom_expiry'] = [
		// 			'order_time' 		=> Carbon::now()->toDateTimeString()." +0700",
		// 			'expiry_duration'   => 60,
		// 			'unit' 		 	    => 'minute'
		// 		  ];
		
		$data = json_encode($data);
		// Mengirimkan request dengan menggunakan CURL
		// HTTP METHOD : POST
		// Header:
		//	Content-Type : application/json
		//	Accept: application/json
		// 	Basic Auth using server_key
		$request = curl_init($ENDPOINT);
		curl_setopt($request, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($request, CURLOPT_POSTFIELDS, $data);
		curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);
		$auth = sprintf('Authorization: Basic %s', base64_encode($SERVER_KEY.':'));
		curl_setopt($request, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Accept: application/json',
			$auth 
			)
		);   

		echo curl_exec($request);

	}

}//end class
