<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('Doku/Doku.php');

use Carbon\Carbon;

class Home extends MY_Controller {

	function __construct()
	{
	   parent::__construct();
	   date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

			$session = $this->checkToken($data['token']);

			if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

				$response["message"] = $session['tokenStatus'];
				$response["data"] = new StdClass();
				return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

			}

			$content = "select
								 d.paket_id,
								 d.paket_picture,
								 d.paket,
								 c.title,
								 f.title as satuan,
								 c.berat_kemasan,
								 e.simbol,
								 e.title as type,
								 sum(d.qty) as total_qty,
								 sum(d.unitprice_ts * d.qty) as total_harga,
								 d.app_code,
								 d.created_by,
								 d.status,
								 d.created
							from (
									select g.created, g.status, g.created_by, g.app_code, g.picture as paket_picture, g.title as paket, a.min_qty, b.id, b.paket_id, b.grade_id, b.product_id, b.qty, a.unitprice_ts
								  		from  (SELECT * FROM `hargas` WHERE `status` = 1 and wilayah_id=".$session['data']->wilayah_id." ) a
												inner join paket_details b on
												a.product_id=b.product_id and a.grade=b.grade_id
												and a.min_qty <= b.qty
												inner join pakets g on g.id=b.paket_id ) d,

												( select a.product_id,max(a.min_qty) jml from (SELECT * FROM `hargas` WHERE `status` = 1 and wilayah_id=".$session['data']->wilayah_id." ) a
												inner join paket_details b on a.product_id=b.product_id and a.grade=b.grade_id
												and a.min_qty <= b.qty
												inner join pakets h on h.id=b.paket_id
												group by a.product_id
												 ) y

						inner join products c on c.id=y.product_id
						inner join product_wilattrs on product_wilattrs.product_id = c.id
                            and product_wilattrs.publish = 1
                            and product_wilattrs.wilayah_id = ".$session['data']->wilayah_id."
                            and product_wilattrs.is_ms = 1
						left join kemasans e on e.id=c.kemasan_id
						left join satuans f on f.id=c.satuan_id

						where d.product_id=y.product_id and d.min_qty=y.jml and (d.created_by = ".$session['data']->id." OR app_code = 0) and d.status = 1



						group by d.paket

						order by d.app_code desc,  d.created desc

						limit 3
					";


			$paket = $this->db->query($content)->result();

			$querycategories =   $this->db->select(['categories.id',
										  'categories.name',
										  'categories.picture',
										])
										->from('categories')
										->where(['categories.status' => 1, 'categories.parent_id' => null, 'categories.is_ts' => 1])
										->order_by('categories.level', 'asc')
										->get()
										->result();

			$categories = $querycategories == null ? $obj : $querycategories;

			$query =   $this->db->select(['products.id as product_id',
										  'products.title',
										  'IFNULL(hargas.unitprice_ts, 0) as unitprice_ts',
										  'productpictures.title as img',
										  'kemasans.title as type',
										  'kemasans.simbol',
										  'products.berat_kemasan',
										  'satuans.title as satuan',
										  'hargas.wilayah_id',
										  'wilayahs.title as wilayah',
										 'IFNULL(hargas.min_qty, 1) as min_qty',
										  'hargas.grade',
										  'harga_grades.title as grade_title',
										  'categories.name as category'
										])
								->from('products')
								->join('hargas', 'hargas.product_id = products.id and hargas.min_qty = 1 and hargas.status =1 and  hargas.wilayah_id = '.$session['data']->wilayah_id, 'left')
								->join('product_wilattrs', 'product_wilattrs.product_id = products.id and product_wilattrs.publish = 1 and product_wilattrs.wilayah_id = '.$session['data']->wilayah_id, 'inner')
								->join('harga_grades', 'harga_grades.id = hargas.grade', 'left')
								->join('wilayahs', 'wilayahs.id = '.$session['data']->wilayah_id, 'left')
								->join('productpictures', 'productpictures.product_id = products.id', 'left')
								->join('kemasans', 'kemasans.id = products.kemasan_id', 'left')
								->join('satuans', 'satuans.id = products.satuan_id', 'left')
								->join('products_categories', 'products_categories.product_id = products.id', 'left')
								->join('categories', 'categories.id = products_categories.category_id', 'left');

			$buah = clone $query;
			$sayur = clone $query;

			$buah = $buah->where(['products_categories.category_id' => 22, 'productpictures.status' => 1, 'products.publish' => 1, 'unitprice_ts !=' => 0,'is_ms' => 1]);
			$buah = $buah->group_by('products.id')->order_by('products.title', 'asc')->limit(4)->get()->result();

			$sayur = $sayur->where(['products_categories.category_id' =>  21, 'productpictures.status' => 1, 'products.publish' => 1, 'unitprice_ts !=' => 0,'is_ms' => 1]);
			$sayur = $sayur->group_by('products.id')->order_by('products.title', 'asc')->limit(4)->get()->result();

			$response['base_url_categories'] = $this->base_url_categories;
			$response['base_url_product'] = $this->base_url_product;
			$response['base_url_package'] = $this->base_url_package;
			$response['base_url_promo'] = $this->base_url_promo;

			$response['status'] = 'success';
			// $response['data'] = array('buah' => $buah, 'sayur' => $sayur, 'categories' => $categories, 'paket' => $paket->result());

			$produk[] = array('id' => 21, 'title' => 'Sayur', 'items' => $sayur);
			$produk[] = array('id' => 22, 'title' => 'Buah - Buahan', 'items' => $buah);

			$current_date =
			$promo = $this->db->query(
									'select
										promos.id, promos.code, promos.title, promos.image,
										promos.intro, promos.nilai, promos.is_percentage, promos.desc,
									    promos.url_terkait, promos.pic_telp,
										DATE_FORMAT(promos.start_date, "%e %M %Y") as start_date, DATE_FORMAT(promos.end_date, "%e %M %Y") as end_date
									from promos
									join promo_wilayahs on promo_wilayahs.promo_id = promos.id
									where promo_wilayahs.wilayah_id = '.$session['data']->wilayah_id.'
										and promo_wilayahs.ts_grade_id = '.$session['data']->ts_grade_id.'
										and start_date <= NOW()
										and end_date >= NOW()
									    and promo_wilayahs.status = 1
									    and promos.status = 1
									    and promos.publish = 1
									    and promos.app_code = 2
										and promos.type_promo = 1'
								)->result();

			$response['data'] = array('product' => $produk, 'categories' => $categories, 'paket' => $paket, 'promo' => $promo);

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function dashboard()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

			$session = $this->checkToken($data['token']);

			if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

				$response["message"] = $session['tokenStatus'];
				$response["data"] = new StdClass();

				return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

			}

			$pesanan_total = $this->db->select(['orders.id','orders.totalAmountInCart'])
									   ->from('orders')
									   ->where(['orders.wilayah_detail_id' => $session['data']->id,
												'orders.app_code' => 1]);

			$pesanan_total = $pesanan_total->count_all_results();

			$pesanan_success = $this->db->select(['orders.id','orders.totalAmountInCart'])
									   ->from('orders')
									   ->where(['orders.wilayah_detail_id' => $session['data']->id,
												'orders.app_code' => 1, 'orders.order_status_code' => $this->order_code_success]);

			$pesanan_success = $pesanan_success->count_all_results();

		    $pesanan_pending = $this->db->select(['id','totalAmountInCart'])
									   ->from('orders')
									   ->where(['wilayah_detail_id' => $session['data']->id,
												'app_code' => 1])
									   ->where_in('order_status_code', $this->code_pending_customer);

			$pesanan_pending = $pesanan_pending->count_all_results();

			$sisa_komisi = $this->sisaKomisi($session['data']->id);
			$sisa_plafon = $this->sisaPlafon($session['data']->id);


		    $customer_pending_order = $this->get_cs_pending_hutang($session['data']->id)->result();


		    // print_r($customer_pending_order); die();

		    			$total_hutang_customer = 0;
						$total_hutang_ts       = 0;

						if($customer_pending_order){

							foreach ($customer_pending_order as $value) {

								    $total_hutang_customer = (
								    	                       ($total_hutang_customer +
								    	                       		($value->totalAmountInCart + $value->ongkir))
								    	                       			-
								    	                       	    ($value->komisi)
								    	                       	    	-
								    	                       	    ($value->nominal_promo)
								    	                     );

							}
 
						}

			$ts_order = $this->total_ts_order_hutang($session['data']->id)->total;

			$persentase =0;
			if($pesanan_success != 0){
				$persentase = doubleval(number_format(($pesanan_success/$pesanan_total) * 100, 2));
			}

			$response['status'] = "success";
			$response['data']['persentase']    = $persentase;
			$response['data']['order_pending'] = (string) $pesanan_pending;
			$response['data']['sisa_plafon']   = $this->formatRp($sisa_plafon);
			$response['data']['sisa_komisi']   = $this->formatRp($sisa_komisi);
			$response['data']['payment_now']   = $this->formatRp($ts_order + $total_hutang_customer);
			$response['data']['ts_order']      = $this->formatRp($ts_order);
			$response['data']['customer_order']= $this->formatRp($total_hutang_customer);

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function detailPaymentNow(){

		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

			$session = $this->checkToken($data['token']);

			if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

				$response["message"] = $session['tokenStatus'];

				return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

			}

		    // $customer_order = $this->db->select(['IFNULL(sum(totalAmountInCart), 0) as totalAmountInCart'])
						// 			   ->from('orders')
						// 			   ->where(['wilayah_detail_id' => $session['data']->id,
						// 						'app_code' => 1, 'payment_type_id' => 1])
						// 			   ->where_in('order_status_code', $this->code_pending_hutang)
						// 			   ->join('auth_members', 'auth_members.id = orders.member_id', 'inner')
						// 			   ->get()
						// 			   ->row();

			$customer_order = $this->get_cs_pending_hutang($session['data']->id)->result();

		    			$total_hutang_customer = 0;
						$total_hutang_ts       = 0;

						if($customer_order){

							foreach ($customer_order as $value) {

								    $total_hutang_customer = (
								    	                       ($total_hutang_customer +
								    	                       		($value->totalAmountInCart + $value->ongkir))
								    	                       			-
								    	                       	    ($value->komisi)
								    	                       	    	-
								    	                       	    ($value->nominal_promo)
								    	                     );

							}

						}

			$detail_customer_order = $this->get_cs_pending_hutang($session['data']->id);
			$detail_ts_order = $this->get_ts_order_hutang($session['data']->id);

			$total_ts_order_hutang = $this->total_ts_order_hutang($session['data']->id);

			$response['status'] = "success";
			$response['data']['payment_now']   = $total_ts_order_hutang->total + $total_hutang_customer;
			$response['data']['ts_order']      = $total_ts_order_hutang->total;
			$response['data']['customer_order']= number_format((float)$total_hutang_customer, 2, '.', '');
			$response['data']['detail_customer_order']= $detail_customer_order->result();
			$response['data']['detail_ts_order']= $detail_ts_order->result();

			$response['data']['status_aktivasi'] = 'available';
			$response['data']['hutang_id'] = '';
			$response['data']['sisa_waktu_order'] = 0;

			$get_status_aktivasi = $this->db->select(['id', 'status_code', 'hutang_id', 'hutang_expired'])
											->from('hutang_payments')
											->where(['wilayah_detail_id' => $session['data']->id])
											->where_in('status_code', $this->status_code_pending)
											->order_by('id', 'desc')
											->get()
											->row();


			if($get_status_aktivasi != null){

						$start  = Carbon::now();
						// date_default_timezone_set('Asia/Jakarta');
						// $awal  = strtotime($get_transaction_time->transaction_time);
						// $akhir = strtotime($response['order_expired']);
						// $diff  = $akhir - $awal;
						// $jam   = floor($diff / (60 * 60));
						// $menit = $diff - $jam * (60 * 60);
						// $response['sisa_waktu_order'] = $jam .  ' jam, ' . floor( $menit / 60 ) . ' menit';

						 $t1 = strtotime($start->toDateTimeString());
        				 $t2 = strtotime($get_status_aktivasi->hutang_expired);

						$dtd = new stdClass();
				        $dtd->interval = $t2 - $t1;
				        $dtd->total_sec = abs($t2-$t1);
				        $dtd->total_min = floor($dtd->total_sec/60);

						$response['data']['sisa_waktu_order'] = intval($dtd->total_min);
						$response['data']['status_aktivasi'] = 'pending';
						$response['data']['hutang_id'] = $get_status_aktivasi->hutang_id;


						if($dtd->interval <= 0){
								$response['data']['sisa_waktu_order'] = floor($dtd->interval/60);
								$response['data']['status_aktivasi'] = 'available';
			   				    $response['data']['hutang_id'] = $get_status_aktivasi->hutang_id;
						}

				// if($get_status_aktivasi->status_code == '201'){
				// 	$response['data']['status_aktivasi'] = 'pending';
				// 	$response['data']['hutang_id'] = $get_status_aktivasi->hutang_id;
				// }
			}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

	}

	public function pickHutang()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}


				$this->db->trans_begin();
		    	try {

		    			$thn = substr(date('Y', NOW()), 3);
				    	$bln = date('m', NOW());
				    	$hutang_id= "H".strtoupper($thn.$bln.$this->random_num(5));

				    	$total_hutang = 0;

		    			$customer_pending_order = $this->get_cs_pending_hutang($session['data']->id)->result();
		    			$ts_pending_order       = $this->get_ts_order_hutang($session['data']->id)->result();

						$response['data']['hutang_id']    = "";
						$response['data']['gross_amount'] = 0;


						if($customer_pending_order == null && $ts_pending_order == null)
						{
							$response["status"] = 'success';
							$response["message"] = 'hutang tiak ada';

							return $this->output->set_content_type('application/json')
									            ->set_status_header(200)
									            ->set_output(json_encode($response));
						}

						$total_hutang_customer = 0;
						$total_hutang_ts       = 0;

						if($customer_pending_order){

							foreach ($customer_pending_order as $value) {

									// $total_hutang_customer = $total_hutang_customer + ($value->totalAmountInCart + $value->ongkir);
								    $total_hutang_customer = (
								    	                       ($total_hutang_customer + ($value->totalAmountInCart + $value->ongkir)) -
								    	                       ($value->komisi)
								    	                     );

									//insert hutang
									$hutang_payments['order_id'] = $value->order_id;
									$hutang_payments['hutang_id'] = $hutang_id;
									$hutang_payments['wilayah_detail_id'] = $session['data']->id;
									$hutang_payments['amount'] = ($value->totalAmountInCart + $value->ongkir) - ($value->komisi);
									$hutang_payments['status_code'] = 100;
									$hutang_payments['log'] = 'hutang '.$hutang_payments['order_id'].' by '.$session['data']->title;
									$hutang_payments['status'] = 0;
									$hutang_payments['type'] = 1;
									$hutang_payments['hutang_expired'] = Carbon::now()->addMinutes($this->minute_expired)->toDateTimeString();
									$hutang_payments['created'] = date('Y-m-d H:i:s', NOW());

									$hutang_payments_x[] = $hutang_payments;

							}

							if($hutang_payments_x != null){

								$this->db->insert_batch('hutang_payments', $hutang_payments_x);

							}

						}

						if($ts_pending_order){

							foreach ($ts_pending_order as $value) {

									$total_hutang_ts = $total_hutang_ts + ($value->totalAmountInCart + $value->ongkir);

									//insert hutang
									$hutang_payments2['order_id'] = $value->order_id;
									$hutang_payments2['hutang_id'] = $hutang_id;
									$hutang_payments2['amount'] = $value->totalAmountInCart + $value->ongkir;
									$hutang_payments2['wilayah_detail_id'] = $session['data']->id;
									$hutang_payments2['status_code'] = 100;
									$hutang_payments2['log'] = 'created hutang '.$hutang_payments2['order_id'].' by '.$session['data']->title;
									$hutang_payments2['status'] = 0;
									$hutang_payments2['type'] = 0;
									$hutang_payments2['hutang_expired'] = Carbon::now()->addMinutes($this->minute_expired)->toDateTimeString();
									$hutang_payments2['created'] = date('Y-m-d H:i:s', NOW());

									$hutang_payments_x2[] = $hutang_payments2;

							}

							if($hutang_payments_x2 != null){

								$this->db->insert_batch('hutang_payments', $hutang_payments_x2);

							}

						}

						$total_hutang = $total_hutang_customer + $total_hutang_ts;

						$hutang_payments_histories['hutang_id'] = $hutang_id;
						$hutang_payments_histories['status_code'] = 100;
						$hutang_payments_histories['remark'] = 'hutang setup payment';
						$hutang_payments_histories['log'] = 'hutang setup payment';
						$hutang_payments_histories['status'] = 1;
						$hutang_payments_histories['created'] = date('Y-m-d H:i:s', NOW());

						$this->db->insert('hutang_payments_histories', $hutang_payments_histories);

						if($total_hutang > 0){

							//DOKU PAYMENT 
				        	Doku_Initiate::$sharedKey = $this->post_sharedKey;
							Doku_Initiate::$mallId = $this->post_mallId;

							$data['hargatotal'] = number_format($total_hutang, 2, '.', '');
							$data['order_id'] = $hutang_id;

							$params = array('amount' => $data['hargatotal'], 
											'invoice' => $data['order_id'],  
											'currency' => 360);

							$words = Doku_Library::doCreateWords($params);

							$customer = array('name' => $session['data']->title,
											  'data_phone' => $session['data']->phone, 
											  'data_email' => $session['data']->email, 
											  'data_address' => $session['data']->address);

							$dataPayment = array('req_mall_id' => $this->post_mallId, 
												 'req_chain_merchant' => 'NB', 
												 'req_amount' => $data['hargatotal'], 
												 'req_words' => $words, 
												 'req_currency' => 360,
												 'req_purchase_currency' => 360,
												 'req_trans_id_merchant' => $params['invoice'], 
												 'req_purchase_amount' => $params['amount'], 
												 'req_request_date_time' => date('YmdHis'), 
												 'req_session_id' => sha1(date('YmdHis')), 
												 'req_email' => $session['data']->email, 
												 'req_name' => $session['data']->title, 
												 'req_basket' => '', 
												 'req_address' => $session['data']->address, 
												 'req_mobile_phone' => $session['data']->phone, 
												 'req_expiry_time' => $this->minute_expired);

							$response_doku = Doku_Api::doGeneratePaycode($dataPayment);

							if($response_doku->res_response_code == '0000'){
							        
							         //insert payment_notifications
									$payment_notifications['order_id'] = $data['order_id'];
									// $payment_notifications['order_status_code'] = 'OS04';
									$payment_notifications['order_id_midtrans'] = $data['order_id'];
									$payment_notifications['channel_names'] = 'alfamart - doku';
									$payment_notifications['channel_payment_code'] = $this->alfamart_code.$response_doku->res_pay_code;
									$payment_notifications['payment_code'] = $response_doku->res_payment_code;
									// $payment_notifications['paid_at'] = $data['hargatotal'];
									// $payment_notifications['store'] = @$data['store'];
									// $payment_notifications['bill_key'] = 1;
									// $payment_notifications['biller_code'] = 1;
									// $payment_notifications['permata_va_number'] = 1;
									// $payment_notifications['approval_code'] = 1;
									// $payment_notifications['masked_card'] = 1;
									// $payment_notifications['bank'] = 1;
									// $payment_notifications['eci'] = 1;
									// $payment_notifications['channel_response_code'] = 1;
									// $payment_notifications['channel_response_message'] = 1;
									$payment_notifications['transaction_time'] = date('Y-m-d H:i:s', NOW());
									$payment_notifications['gross_amount'] = @$data['hargatotal'];
									// $payment_notifications['currency'] = 1;
									$payment_notifications['payment_type'] = @$data['payment_type'];
									// $payment_notifications['signature_key'] = 1;
									$payment_notifications['status_code'] = $response_doku->res_response_code;
									$payment_notifications['transaction_id'] = $response_doku->res_pairing_code;
									$payment_notifications['transaction_status'] = $response_doku->res_response_msg;
									// $payment_notifications['fraud_status'] = 1;
									$payment_notifications['status'] = 0;
									$payment_notifications['status_message'] = $response_doku->res_response_msg;
									// $payment_notifications['detail'] = 1;
									// $payment_notifications['upload_file'] = 1;
									// $payment_notifications['fraud_status'] = 1;
									$payment_notifications['created'] = date('Y-m-d H:i:s', NOW());
									$payment_notifications['log'] = json_encode($response_doku);
									// $payment_notifications['modified'] = ;
									$this->db->insert('payment_notifications', $payment_notifications);

									// //update order
									// $orders['order_expired'] = Carbon::now()->addMinutes($this->minute_expired)->toDateTimeString();
									// $orders['order_status_code'] = $this->order_waiting_payment;
									// $orders['order_id_midtrans'] = $data['order_id'];
									// $update_orders = $this->db->where(['id' => $data['order_id']])
									// 		   					      ->update('orders', $orders);

								 //    //order history
									// $order_histories['order_id'] = $order_id;
									// $order_histories['order_status_code	'] = $this->order_waiting_payment;
									// $order_histories['remark'] = 'order status changed by android';
									// $order_histories['status'] = 1;
									// $order_histories['created'] = date('Y-m-d H:i:s', NOW());
									// $order_histories['created_by'] = $session['data']->id;
									// $this->db->insert('order_histories', $order_histories);

							}else{
									// $response["message"] = $response_doku->res_response_msg;
									$response["message"]['msg'] = 'gagal memverifikasi type pembayaran';
									$response["message"]['detail'] = $response_doku;
									return $this->output->set_content_type('application/json')
									            ->set_status_header(200)
									            ->set_output(json_encode($response));
							}

						}

						$response["status"] = 'success';
						$response['data']['hutang_id']    = $hutang_id;
						$response['data']['gross_amount'] = $total_hutang;  


				} catch (Exception $e) {
		    		 $this->db->trans_rollback();
					 $response["message"] = "terjadi kesalahan pada database (rollback)";
		    	}

		    	if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback();
					    $response["message"] = "terjadi kesalahan pada database (rollback)";
				}else {
				        $this->db->trans_commit();
				        $response["status"] = 'success';
				        $response["message"] = 'data berhasil disimpan';
				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));


	}


	public function listPaymentNow()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$id_payment = $this->list_payment_now;
				$query = $this->db->select(['payment_types.id',
											'payment_types.title',
											'payment_types.image',
											'payment_types.detail'])
								->from('payment_types')
								->where(['payment_types.is_ts' => 1])
								->where_in('id', $id_payment)
								->order_by('payment_types.id', 'desc')
								->get();


				if($query == null){

					$response['message'] = 'list payment tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

	 				$response['status']  = 'success';
					$response['message'] = 'list payment';
					$response['data']    =  $query->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function listPlafon()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$query = $this->db->select(['type_of_transaction',
											'amount_order',
											'remark',
											'created'])
								->from('plafon_histories')
								->where(['wilayah_detail_id' => $session['data']->id])
								->order_by('id', 'desc')
								->get();


				if($query == null){

					$response['message'] = 'list payment tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

	 				$response['status']  = 'success';
					$response['message'] = 'list payment';
					$response['data']    =  $query->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function listKomisi()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$query = $this->db->select(['type_of_transaction',
											'amount as amount_order',
											'remark',
											'created'])
								->from('komisi_histories')
								->where(['wilayah_detail_id' => $session['data']->id])
								->order_by('id', 'desc')
								->get();


				if($query == null){

					$response['message'] = 'Komisi tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

	 				$response['status']  = 'success';
					$response['message'] = 'list payment';
					$response['data']    =  $query->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function detailHourHutang()
	{

		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('order_id', 'order_id', 'required');
		$this->form_validation->set_rules('duration', 'duration', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$query = $this->db->select(['hutang_payments.hutang_id',
											'CAST(sum(hutang_payments.amount) AS SIGNED) as totalAmountInCart'])
								->from('hutang_payments')
								->join('payment_notifications', 'payment_notifications.order_id = hutang_payments.hutang_id', 'inner')
								->where(['hutang_payments.hutang_id' => $data['order_id']])
								->limit(1)
								->order_by('payment_notifications.created', 'desc')
								->group_by('hutang_payments.hutang_id')
								->get();

				if($query == null){

					$response['message'] = 'orders tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

	 				$response['status']    = 'success';
					$response['message']   = 'list order';
					$response['data']= $query->row();
					$response['detail']    =  [];
					$response['payment_code'] = "";
					$response['sisa_waktu_order'] = "";
					$response['order_expired'] = "";
					$response['diffInMinutes'] = $data['duration'];

					$get_transaction_time = $this->db->select(['payment_notifications.transaction_time',
															   'payment_code',
															   'channel_payment_code',
															   'hutang_payments.hutang_expired as order_expired'])
													 ->from('payment_notifications')
													 ->join('hutang_payments', 'hutang_payments.hutang_id = payment_notifications.order_id', 'inner')
													 ->where('payment_notifications.order_id', $data['order_id'])
													 ->order_by('payment_notifications.id', 'desc')
													 ->group_by('hutang_payments.hutang_id')
													 ->limit(1)
													 ->get()
													 ->row();

					if($get_transaction_time){

						$response['order_expired'] = $get_transaction_time->order_expired;

						$start  = Carbon::now();
						$end    = Carbon::parse($response['order_expired']);

						$t1 = strtotime($start->toDateTimeString());
        				$t2 = strtotime($response['order_expired']);

						$dtd = new stdClass();
				        $dtd->interval = $t2 - $t1;
				        // $dtd->total_sec = abs($t2-$t1);
				        $dtd->total_sec = $t2-$t1;
				        $dtd->total_min = floor($dtd->total_sec/60);

						$response['sisa_waktu_order'] = intval($dtd->total_min);

						if($dtd->total_min < 0){
							$response['sisa_waktu_order'] = 0;
						}

						$response['payment_code'] = $get_transaction_time->channel_payment_code;
						$response['diffInMinutes'] = $data['duration'];

					}

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}


	public function listPickHutang()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$query = $this->db->select(['hutang_payments.hutang_id',
											'sum(hutang_payments.amount) as total',
											'hutang_payments.created',
											'hutang_payments.hutang_expired'])
								->from('hutang_payments')
								->where(['wilayah_detail_id' => $session['data']->id])
								->order_by('hutang_payments.created', 'desc')
								->group_by('hutang_payments.hutang_id')
								->get();

				if($query == null){

					$response['message'] = 'Hutang tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

	 				$response['status']    = 'success';
					$response['message']   = 'list order';
					$response['data']      =  $query->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

	}



}//end class
