<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('Doku/Doku.php');

use Carbon\Carbon;

class Order extends MY_Controller {

	function __construct()
	{
	   parent::__construct();
	   date_default_timezone_set('Asia/Jakarta');
	}

	public function pick()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$data['items'] = json_decode($data['items']);

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('hargatotal', 'hargatotal', 'required');
		$this->form_validation->set_rules('komisi', 'komisi', 'required');
		$this->form_validation->set_rules('nominal_promo', 'nominal_promo', 'required');
		// $this->form_validation->set_rules('payment_type_id', 'payment_type_id', 'required');
		$this->form_validation->set_rules('wilayah_address_details_id', 'wilayah_address_details_id', 'required');

		if(count($data['items']) < 1){

			$response['message'] = 'items tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

		}

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				if (isset($session['data']->wilayah_id) && !empty($session['data']->wilayah_id)) {
					$query = $this->db->select(['open_ms', 'closing_ms'])
						->where('id', $session['data']->wilayah_id)
						->from('wilayahs')
						->get()
						->row();
					
					$start = str_replace('.', ':', $query->open_ms);
					$end = str_replace('.', ':', $query->closing_ms);

					date_default_timezone_set("Asia/Jakarta");
					$current_time = date('H:i');
					$date1 = DateTime::createFromFormat('H:i', $current_time);
					$date2 = DateTime::createFromFormat('H:i', $start);
					$date3 = DateTime::createFromFormat('H:i', $end);
					//$date1 = new DateTime();
					//$date2 = new DateTime($start);
					//$date3 = new DateTime($end);

					if (($date1 >= $date2) && ($date1 <= $date3)) {
						/*$response['message'] = 'Bisa order';
						return $this->output->set_content_type('application/json')
							->set_status_header(200)
							->set_output(json_encode($response));*/
					} else {
						//$text = 'JAM PESANAN ORDER SUDAH DITUTUP. ORDER DIBUKA DARI JAM ' . $sunrise . ' - ' . $sunset;
						$response['message'] = 'Jam pesanan order sudah ditutup. Order dibuka dari jam ' . $start . ' - ' . $end;
						return $this->output->set_content_type('application/json')
							->set_status_header(200)
							->set_output(json_encode($response));
					}

				}


				 //$response["message"] = 'Maaf hari ini tidak bisa order, Order dapat di lakukan kembali mulai tgl 1 januari 2019';
				 //	return $this->output->set_content_type('application/json')
			     //        ->set_status_header(200)
			     //        ->set_output(json_encode($response));

				$toko = $this->db->select('*')
									->from('wilayah_address_details')
									->where(['id' => $data['wilayah_address_details_id']])
									->get()
									->row();

				if($toko == null){

					$response["message"] = 'Toko tidak ditemukan';
					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				/*$param =   $this->db->select('param_value')
											->where('id', 5)
											->from('params')
											->get()
											->row()
											->param_value;

				$start  = Carbon::now();

				$t1 = strtotime($start->toDateTimeString());
				$t2 = strtotime($start->toDateString().' '.$param);

				$dtd = new stdClass();
		        $dtd->interval = $t2 - $t1;
		        $dtd->total_sec = abs($t2-$t1);
		        $dtd->total_min = floor($dtd->total_sec/60);

		        if($dtd->interval < 0){
		        	$response["message"] = 'Maaf, anda tidak bisa order lewat jam 22:00';
						return $this->output->set_content_type('application/json')
				            ->set_status_header(200)
				            ->set_output(json_encode($response));
		        }*/


		        if($data['payment_type'] == 5){

		        	$status_code_not_success = ['OS03', 'OS13', 'OS14'];
			        // $status_code_not_success = ['OS04'];
			        $cek_order_not_succes = $this->db->select(['id'])
			        								 ->from('orders')
			        								 ->where(['app_code' => 2, 'member_id' => $session['data']->id])
			        								 ->where_in('order_status_code', $status_code_not_success)
			        								 ->count_all_results();

			        if($cek_order_not_succes > 0){
			        	$response["message"] = 'Maaf anda belum bisa order. Mohon selesaikan pembayaran order sebelumnya';
							return $this->output->set_content_type('application/json')
					            ->set_status_header(200)
					            ->set_output(json_encode($response));
			        }

		        }else{

		        	// $status_code_not_success = ['OS03', 'OS13', 'OS14'];
			        // $status_code_not_success = ['OS04'];
			        $cek_order_not_succes = $this->db->select(['id'])
			        								 ->from('orders')
			        								 ->where(['app_code' => 2,
			        								 		  'member_id' => $session['data']->id, 
			        								 		  'payment_type_id' => 9, 
			        								 		  'order_status_code' => 'OS04'])
			        								 ->count_all_results();

			        if($cek_order_not_succes > 0){
			        	$response["message"] = 'Maaf anda belum bisa order. Mohon selesaikan pembayaran order sebelumnya';
							return $this->output->set_content_type('application/json')
					            ->set_status_header(200)
					            ->set_output(json_encode($response));
			        }

		        }

		        


			   $sisa_plafon = 0;
			   // $data['payment_type '] = 1;

			   $total_cart = ($data['hargatotal'] + $data['komisi'] + $data['nominal_promo']);

			   if($data['payment_type'] == 5 || $data['payment_type'] == 7){

			   		$get_sisa_plafon = $this->db->select(['plafon_histories.total',
                                                      'plafon_histories.id',
                                                      'plafon_histories.total',
                                                      'plafon_histories.created',
                                                      'ts_grades.plafon_to',
                                                      'ts_grades.plafon_from'])
                                            ->from('plafon_histories')
                                            ->join('ts_grades', 'ts_grades.id=plafon_histories.ts_grade_id', 'inner')
                                            ->where(['plafon_histories.wilayah_detail_id' => $session['data']->id])
                                            ->order_by('plafon_histories.id', 'desc')
                                            ->get()
                                            ->row();

			   		$sisa_plafon = $get_sisa_plafon->total;

				    if($data['hargatotal'] > $sisa_plafon){
				    	$response["message"] = 'Dana pinjaman ('.$sisa_plafon.') tidak mencukupi untuk belanja';
						return $this->output->set_content_type('application/json')
				            ->set_status_header(200)
				            ->set_output(json_encode($response));
				    }


				    
				    if( $total_cart < $get_sisa_plafon->plafon_from){
				    	$response["message"] = 'Minimal belanja menggunakan dana  peminjaman '.$this->formatRp($get_sisa_plafon->plafon_from);
						return $this->output->set_content_type('application/json')
				            ->set_status_header(200)
				            ->set_output(json_encode($response));
				    }


				    $today = Carbon::parse(Carbon::today()->toDateString());

				    $total_day = $today->diffInDays($get_sisa_plafon->created);

				    if($total_day > 0){

					    if($get_sisa_plafon->total != $get_sisa_plafon->plafon_to){
					    	$response["message"] = 'Anda belum membayar plafon dari pesanan sebelum nya';
							return $this->output->set_content_type('application/json')
					            ->set_status_header(200)
					            ->set_output(json_encode($response));
					    }

					}

			    }

				$this->db->trans_begin();
		    	try {

					$total_keranjang = 0; //sum total harga di keranjang
					for($i=0; $i< count($data['items']); $i++)
					{

						$query =   $this->db->select('*')
								->from('products')
								->join('hargas', 'hargas.product_id = products.id', 'left')
								->join('wilayahs', 'wilayahs.id = hargas.wilayah_id', 'inner')
								->join('productpictures', 'productpictures.product_id = products.id', 'left')
								->join('kemasans', 'kemasans.id = products.kemasan_id', 'left')
								->join('satuans', 'satuans.id = products.satuan_id', 'left')
								->where(['products.id' => $data['items'][$i]->product_id, 'hargas.id' => $data['items'][$i]->hargas_id,
										 'productpictures.status' => 1])
								->get()
								->row();

						if($query) {
							//
							$current_price = $query->unitprice_ts * $data['items'][$i]->qty;
							$total_keranjang += $current_price;
						}

					}

					// if($total_keranjang != $total_cart) {
					// 	$response["status"] = "error_14";
					// 	$response["message"] = 'Woops...Terdapat perubahan harga di daftar keranjang anda';
					// 	return $this->output->set_content_type('application/json')
					// 		->set_status_header(200)
					// 		->set_output(json_encode($response));
					// }
					// end of testing purpose


		    		$thn = substr(date('Y', NOW()), 3);
		    		$bln = date('m', NOW());

					$orders['code'] = strtoupper($thn.$bln.$this->random_num(5));
					$orders['wilayah_id'] = $toko->wilayah_id;
					$orders['wilayah_detail_id'] = $session['data']->id;
					$orders['wilayah_address_detail_id'] = $data['wilayah_address_details_id'];
					$orders['member_id'] = $session['data']->id;
					$orders['cust_addr_title'] = $toko->title;
					$orders['cust_addr_detail'] = $toko->address;
					$orders['cust_addr_g_route'] = $toko->g_route;
					$orders['cust_addr_adm_area_level_1'] = $toko->adm_area_level_1;
					$orders['cust_addr_adm_area_level_2'] = $toko->adm_area_level_2;
					$orders['cust_addr_adm_area_level_3'] = $toko->adm_area_level_3;
					$orders['cust_addr_country'] = $toko->country;
					$orders['cust_addr_phone'] = $session['data']->phone;
					$orders['cust_addr_postal_code'] = $toko->postal_code;
					$orders['cust_addr_lat'] = $toko->lat;
					$orders['cust_addr_lng'] = $toko->lng;
					// $orders['cust_addr_distance'] = ;
					$orders['ongkir'] = 0;
					$orders['ongkir_tambahan'] = 0;

					//edit by hasan
					// $orders['totalAmountInCart'] = $data['hargatotal'];
					$orders['totalAmountInCart'] = $total_cart;
					$orders['totalPayment'] = $data['hargatotal'];

					$orders['payment_type_id'] = $data['payment_type'];
					// $orders['payment_status_code_dihapus_'] = ;
					$orders['order_status_code'] = 'OS01';
					$orders['pool_id'] = $toko->wilayah_pool_id;
					// $orders['unique_code'] = ;
					$orders['created'] = date('Y-m-d H:i:s', NOW());
					$orders['order_expired'] = Carbon::now()->addDays(1)->toDateTimeString();
					$orders['status'] = 1;
					$orders['komisi'] = $data['komisi'];
					// $orders['remark'] = ;
					// $orders['log'] = ;
					// $orders['lock'] = ;
					// $orders['catatan_kurir'] = ;
					// $orders['sys_log'] = ;
					// $orders['complete_date'] = ;
					// $orders['review'] = ;
					// $orders['jarak_pengiriman'] = 0
					$orders['pool_id'] = $toko->wilayah_pool_id;
					$orders['app_code'] = 2;
					$orders['promo_code'] = $data['promo_code'];
					$orders['nominal_promo'] = $data['nominal_promo'];
					$this->db->insert('orders', $orders);
					$order_id = $this->db->insert_id();

					for($i=0; $i< count($data['items']); $i++)
					{

						$query =   $this->db->select(['products.id as product_id',
										  'hargas.id as hargas_id',
										  'products.title',
										  'products.detail as description',
										  'hargas.unitprice_ts',
										  'productpictures.title as img',
										  'kemasans.title as type',
										  'kemasans.simbol',
										  'products.berat_kemasan',
										  'products.jumlah_kemasan',
										  'satuans.title as satuan',
										  'satuans.simbol as satuan_simbol',
										  'hargas.wilayah_id',
										  'wilayahs.title as wilayah',
										  'hargas.min_qty',
										  'hargas.grade',
										  'hargas.normalprice',
										  'hargas.unitprice',
										  'hargas.unitprice_ts',
										  'hargas.index_ts_profit',
										  'hargas.index_ts',
										  'hargas.index_cust'
										])
								->from('products')
								->join('hargas', 'hargas.product_id = products.id', 'left')
								->join('wilayahs', 'wilayahs.id = hargas.wilayah_id', 'inner')
								->join('productpictures', 'productpictures.product_id = products.id', 'left')
								->join('kemasans', 'kemasans.id = products.kemasan_id', 'left')
								->join('satuans', 'satuans.id = products.satuan_id', 'left')
								->where(['products.id' => $data['items'][$i]->product_id, 'hargas.id' => $data['items'][$i]->hargas_id,
										 'productpictures.status' => 1])
								->get()
								->row();

						if($query) {

							$order_details_x['order_id'] = $order_id;
							$order_details_x['product_id'] = $data['items'][$i]->product_id;
							$order_details_x['product_berat_kemasan'] = $query->berat_kemasan;
							$order_details_x['product_jumlah_kemasan'] = $query->jumlah_kemasan;
							$order_details_x['productpicture_title'] = $query->img;
							$order_details_x['product_grade'] = $query->grade;
							$order_details_x['product_normalprice'] = $query->normalprice;
							$order_details_x['product_price_ts'] = $query->unitprice_ts;
							$order_details_x['product_price_cust'] = $query->unitprice;
							$order_details_x['product_profit_ts'] = $query->index_ts_profit;
							$order_details_x['product_index_ts'] = $query->index_ts;
							$order_details_x['product_index_cust'] = $query->index_cust;
							$order_details_x['product_unitprice'] = $query->unitprice_ts;
							// $order_details_x['profit_ts'] = $order_details_x['product_profit_ts'] * $order_details_x['product_unitprice'];
							$order_details_x['profit_ts'] = 0;
							$order_details_x['product_satuan_simbol'] = $query->satuan_simbol;
							$order_details_x['product_satuan_title'] = $query->satuan;
							$order_details_x['product_kemasan_title'] = $query->type;
							$order_details_x['product_kemasan_simbol'] = $query->simbol;
							$order_details_x['product_qty'] = $data['items'][$i]->qty;
							// $order_details_x['product_catatan'] = $query->grade;
							$order_details_x['created'] = date('Y-m-d H:i:s', NOW());
							$order_details_x['status_detail'] = 1;
							$order_details_x['status_catatan'] = "Createad by Mobile TS";

							// $order_details_x['potongan_harga_ts'] = $data['items'][$i]->potongan_harga_ts;
							// edit nama kolom by hasan
							$order_details_x['potongan_harga'] = $data['items'][$i]->potongan_harga_ts;



							$order_details[] = $order_details_x;
						}

					}

					$this->db->insert_batch('order_details', $order_details);

					//insert ke order payment
					//status = 0 pending
					//order history
					$order_payments['order_id'] = $order_id;
					$order_payments['amount	'] = $orders['totalAmountInCart'] + $orders['ongkir'];
					$order_payments['status'] = 0;
					$order_payments['created'] = date('Y-m-d H:i:s', NOW());
					$order_payments['log'] = 'created status changed by '.$session['data']->title;
					$this->db->insert('order_payments', $order_payments);


					$order_status_code = $this->order_code_new;

					//order history
					$order_histories['order_id'] = $order_id;
					$order_histories['order_status_code	'] = $order_status_code;
					$order_histories['remark'] = 'order status changed by '.$session['data']->title;
					$order_histories['status'] = 1;
					$order_histories['created'] = date('Y-m-d H:i:s', NOW());
					$order_histories['created_by'] = $session['data']->id;
					$this->db->insert('order_histories', $order_histories);

					if($data['payment_type'] == 5 || $data['payment_type'] == 7){ 

						//edit by hasan
						$total_sisa_plafon = $sisa_plafon - $data['hargatotal'];

			   			//insert plafon histories
						$plafon_histories['order_id'] = $order_id;
						$plafon_histories['order_status_code'] = $order_status_code;
						$plafon_histories['wilayah_detail_id'] = $session['data']->id;
						$plafon_histories['ts_grade_id'] = $session['data']->ts_grade_id;
						$plafon_histories['type_of_transaction'] = "1";
						$plafon_histories['total'] = $total_sisa_plafon;
						$plafon_histories['amount_order'] = $data['hargatotal'];
						$plafon_histories['remark'] = 'order by '.$session['data']->title;
						$plafon_histories['status'] = 1;
						$plafon_histories['created'] = date('Y-m-d H:i:s', NOW());
						$plafon_histories['created_by'] = $session['data']->id;
						$this->db->insert('plafon_histories', $plafon_histories);

						$wilayah_details['sisa_plafon'] = $total_sisa_plafon;
						$update_wilyah_details = $this->db->where(['id' => $session['data']->id])
								   					      ->update('wilayah_details', $wilayah_details);


						if($data['payment_type'] == 7){

							$sisa_komisi = 0;
							$total_komisi = 0;

							$sisa_komisi = $this->sisaKomisi($session['data']->id);
						    $total_komisi = $sisa_komisi - $data['komisi'];

			    			//insert komisi histories
							$komisi_histories['order_id'] = $order_id;
							$komisi_histories['order_status_code'] = $order_status_code;
							$komisi_histories['wilayah_detail_id'] = $session['data']->id;
							$komisi_histories['type_of_transaction'] = 1;
							$komisi_histories['total'] = $total_komisi;
							$komisi_histories['amount'] = $data['komisi'];
							$komisi_histories['remark'] = 'menggunakan komisi untuk order'.$order_id.' by '.$session['data']->title;
							$komisi_histories['status'] = 1;
							$komisi_histories['is_cash'] = 0;
							$komisi_histories['created'] = date('Y-m-d H:i:s', NOW());
							$komisi_histories['created_by'] = $session['data']->id;
							$this->db->insert('komisi_histories', $komisi_histories);

						}

			        }elseif ($data['payment_type'] == 9) {

			        	//DOKU PAYMENT 
			        	
			        	Doku_Initiate::$sharedKey = $this->post_sharedKey;
						Doku_Initiate::$mallId = $this->post_mallId;


						$data['hargatotal'] = number_format($data['hargatotal'], 2, '.', '');
						$params = array('amount' => $data['hargatotal'], 
										'invoice' => $order_id,  
										'currency' => 360);

						$words = Doku_Library::doCreateWords($params);

						$customer = array('name' => $session['data']->title,
										  'data_phone' => $session['data']->phone, 
										  'data_email' => $session['data']->email, 
										  'data_address' => $session['data']->address);

						$dataPayment = array('req_mall_id' => $this->post_mallId, 
											 'req_chain_merchant' => 'NB', 
											 'req_amount' => $data['hargatotal'], 
											 'req_words' => $words, 
											 'req_currency' => 360,
											 'req_purchase_currency' => 360,
											 'req_trans_id_merchant' => $params['invoice'], 
											 'req_purchase_amount' => $params['amount'], 
											 'req_request_date_time' => date('YmdHis'), 
											 'req_session_id' => sha1(date('YmdHis')), 
											 'req_email' => $session['data']->email, 
											 'req_name' => $session['data']->title, 
											 'req_basket' => '', 
											 'req_address' => $session['data']->address, 
											 'req_mobile_phone' => $session['data']->phone, 
											 'req_expiry_time' => $this->minute_expired);

						$response_doku = Doku_Api::doGeneratePaycode($dataPayment);

						if($response_doku->res_response_code == '0000'){
						        
						        // print_r($response);
						        // die();

						         //insert payment_notifications
								$payment_notifications['order_id'] = $order_id;
								// $payment_notifications['order_status_code'] = 'OS04';
								$payment_notifications['order_id_midtrans'] = $order_id;
								$payment_notifications['channel_names'] = 'alfamart - doku';
								$payment_notifications['channel_payment_code'] = $this->alfamart_code.$response_doku->res_pay_code;
								$payment_notifications['payment_code'] = $response_doku->res_payment_code;
								// $payment_notifications['paid_at'] = $data['hargatotal'];
								// $payment_notifications['store'] = @$data['store'];
								// $payment_notifications['bill_key'] = 1;
								// $payment_notifications['biller_code'] = 1;
								// $payment_notifications['permata_va_number'] = 1;
								// $payment_notifications['approval_code'] = 1;
								// $payment_notifications['masked_card'] = 1;
								// $payment_notifications['bank'] = 1;
								// $payment_notifications['eci'] = 1; 
								// $payment_notifications['channel_response_code'] = 1;
								// $payment_notifications['channel_response_message'] = 1;
								$payment_notifications['transaction_time'] = date('Y-m-d H:i:s', NOW());
								$payment_notifications['gross_amount'] = @$data['hargatotal'];
								// $payment_notifications['currency'] = 1;
								$payment_notifications['payment_type'] = @$data['payment_type'];
								// $payment_notifications['signature_key'] = 1;
								$payment_notifications['status_code'] = $response_doku->res_response_code;
								$payment_notifications['transaction_id'] = $response_doku->res_pairing_code;
								$payment_notifications['transaction_status'] = $response_doku->res_response_msg;
								// $payment_notifications['fraud_status'] = 1;
								$payment_notifications['status'] = 0;
								$payment_notifications['status_message'] = $response_doku->res_response_msg;
								// $payment_notifications['detail'] = 1;
								// $payment_notifications['upload_file'] = 1;
								// $payment_notifications['fraud_status'] = 1;
								$payment_notifications['created'] = date('Y-m-d H:i:s', NOW());
								$payment_notifications['log'] = json_encode($response_doku);
								// $payment_notifications['modified'] = ;
								$this->db->insert('payment_notifications', $payment_notifications);

								//update order
								$orders['order_expired'] = Carbon::now()->addMinutes($this->minute_expired)->toDateTimeString();
								$orders['order_status_code'] = $this->order_waiting_payment;
								$orders['order_id_midtrans'] = $order_id;
								$update_orders = $this->db->where(['id' => $order_id])
										   					      ->update('orders', $orders);

							    //order history
								$order_histories['order_id'] = $order_id;
								$order_histories['order_status_code	'] = $this->order_waiting_payment;
								$order_histories['remark'] = 'order status changed by android';
								$order_histories['status'] = 1;
								$order_histories['created'] = date('Y-m-d H:i:s', NOW());
								$order_histories['created_by'] = $session['data']->id;
								$this->db->insert('order_histories', $order_histories);

						}else{
								// $response["message"] = $response_doku->res_response_msg;
								$response["message"] = 'gagal memverifikasi type pembayaran';
								return $this->output->set_content_type('application/json')
								            ->set_status_header(200)
								            ->set_output(json_encode($response));
						}
						//end doku

			        }

					//counter penggunaan promo all user
					if(!empty($data['promo_code'])) {
						$counter_pemakaian = "update promos
											  set pemakaian_quota = pemakaian_quota + 1
											  where code = '".$data['promo_code']."'
											  and status = '1'
	  										  and publish = '1'
	  										  and app_code = '2'
	  										  and type_promo = '1'";
						$this->db->query($counter_pemakaian);
					}

				} catch (Exception $e) {
		    		 $this->db->trans_rollback();
					 $response["message"] = "terjadi kesalahan pada database (rollback)";
		    	}

		    	if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback();
					    $response["message"] = "terjadi kesalahan pada database (rollback)";
				}else {
				        $this->db->trans_commit();
				        $response["status"] = 'success';
				        $response["message"] = 'data berhasil disimpan';

				        $response["data"]["order_id"] = $order_id;
				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function changeAddress()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();
		$data['items'] = json_decode($data['items']);

		$this->form_validation->set_rules('token', 'token', 'required');
		// $this->form_validation->set_rules('items', 'items', 'required');
		$this->form_validation->set_rules('wilayah_address_details_id', 'wilayah_address_details', 'required');

		if(count($data['items']) < 1){

			$response['message'] = 'items tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

		}

		if ($this->form_validation->run() == TRUE) {

				$wad = $this->db->select(['wilayah_id'])
								->from('wilayah_address_details')
								->where(['id' => $data['wilayah_address_details_id']])
								->get()
								->row();

				if($wad == null){

					$response['message'] = 'wilayah_address_details_id tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{
					for($i=0; $i< count($data['items']); $i++)
					{
						$tanda = false;
						$harga = null;
						$hargasid = null;
						$available = true;

						$query = $this->db->select(['id', 'unitprice_ts', 'min_qty', 'grade', 'wilayah_id'])
												->from('hargas')
												->where(['id' => $data['items'][$i]->hargas_id])
												->get()
												->row();

						if($query != null){

							$hargasupdate = $this->db->select(['id', 'unitprice_ts', 'min_qty', 'grade'])
												->from('hargas')
												->where(['wilayah_id' => $wad->wilayah_id, 'product_id' => $data['items'][$i]->product_id, 'grade' => $query->grade, 'min_qty' => $query->min_qty, 'status' => 1])
												->get()
												->row();

							if($hargasupdate != null) {
								$harga = $hargasupdate->unitprice_ts;
								$tanda =  true;
								$hargasid = $hargasupdate->id;
							}else{
								$available = false;
							}

						}

						$is_ms = $this->db->select('is_ms')
											->from('product_wilattrs')
											->where([
												'product_id' => $data['items'][$i]->product_id,
												'wilayah_id' => $wad->wilayah_id
											])
											->get()
											->row();

						$listharga[] = array('product_id' => $data['items'][$i]->product_id,
											 'hargas_id_old' => $data['items'][$i]->hargas_id,
											 'hargas_id_update' => $hargasid,
											 'unitprice_ts_old' => $data['items'][$i]->unitprice_ts,
											 'unitprice_ts_update' => intval($harga),
											 'available' => $available,
											 'status' => $tanda,
											 'is_ms' => $is_ms
										  );

	 				 }

	 				$response['status']  = 'success';
					$response['message'] = 'berhasil update harga';
					$response['data']    =  $listharga;

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function getPrice()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();
		$data['items'] = json_decode($data['items']);

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('wilayah_address_details_id', 'wilayah_address_details', 'required');

		if(count($data['items']) < 1){

			$response['message'] = 'items tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

		}

		if ($this->form_validation->run() == TRUE) {

				$wad = $this->db->select(['wilayah_id'])
								->from('wilayah_address_details')
								->where(['id' => $data['wilayah_address_details_id']])
								->get()
								->row();

				if($wad == null){

					$response['message'] = 'wilayah_address_details_id tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

					for($i=0; $i< count($data['items']); $i++)
					{
						$query = $this->db->select(['id', 'product_id', 'IFNULL(unitprice_ts, 0) as unitprice_ts', 'min_qty', 'grade', 'wilayah_id'])
												->from('hargas')
												->where(['min_qty <= ' => $data['items'][$i]->product_qty,
														 'grade' => $data['items'][$i]->grade_id,
														 'wilayah_id' => $wad->wilayah_id,
														 'product_id' => $data['items'][$i]->product_id,
														 'status' => 1 ])
												->order_by('min_qty', 'desc')
												->get()
												->row();

						$listharga[] = array('product_id' => $data['items'][$i]->product_id,
											 'hargas_id' =>  $query->id,
											 'unitprice_ts' =>  $query->unitprice_ts,
											 'grade_id' => $data['items'][$i]->grade_id,
											 'product_qty' => $data['items'][$i]->product_qty,
											 'cart_id' => $data['items'][$i]->cart_id);
					}

	 				$response['status']  = 'success';
					$response['message'] = 'berhasil update harga';
					$response['data']    =  $listharga;

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function listOrderTS()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}


				$query = $this->db->select(['orders.id as order_id',
											'DATE_FORMAT(orders.created, "%d-%m-%Y") as created',
											'order_statuses.ts_title',
											'sum(order_details.product_qty) as sum_product',
											'orders.totalAmountInCart',
											'orders.pool_id',
											'orders.code',
											'orders.order_status_code',
											'wilayah_pools.title as pool',
											'payment_types.title as payment',
											'orders.app_code'])
								->from('orders')
								->join('order_details', 'order_details.order_id = orders.id', 'inner')
								->join('order_statuses', 'order_statuses.code = orders.order_status_code', 'left')
								->join('payment_types', 'payment_types.id = orders.payment_type_id', 'left')
								->join('wilayah_pools', 'wilayah_pools.id = orders.pool_id', 'left')
								// ->where(['orders.app_code' => 2])
								->where(['orders.member_id' => $session['data']->id, 'orders.app_code' => 2])
								->where_in('orders.order_status_code', $this->list_code_ts)
								->group_by('orders.id')
								->order_by('orders.created', 'desc')
								->get();

				if($query == null){

					$response['message'] = 'orders tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

					$pool_id = "";
					$content = [];

					foreach($query->result() as $value) {

						if($pool_id != $value->pool_id){

							$query = $this->db->select(['orders.id as order_id',
											'DATE_FORMAT(orders.created, "%d-%m-%Y") as created',
											'order_statuses.ts_title',
											'sum(order_details.product_qty) as sum_product',
											// 'orders.totalAmountInCart',
											'(IFNULL(orders.totalAmountInCart, 0) - IFNULL(orders.komisi, 0) - IFNULL(orders.nominal_promo, 0) + IFNULL(orders.ongkir, 0))  as totalAmountInCart',
											'orders.pool_id',
											'orders.code',
											'orders.order_status_code',
											'wilayah_pools.title as pool',
											'payment_types.title as payment',
											'orders.app_code',
											'orders.order_status_code',
											'IFNULL((select  status_code from payment_notifications where `order_id_midtrans` = `orders`.`order_id_midtrans` order by id desc limit 1), "100") as status_code'])
								->from('orders')
								->join('order_details', 'order_details.order_id = orders.id', 'inner')
								->join('order_statuses', 'order_statuses.code = orders.order_status_code', 'left')
								->join('payment_types', 'payment_types.id = orders.payment_type_id', 'left')
								->join('wilayah_pools', 'wilayah_pools.id = orders.pool_id', 'left')
								// ->join('payment_notifications', 'payment_notifications.order_id_midtrans = orders.order_id_midtrans', 'left')
								// ->where(['orders.app_code' => 2])
								->where(['orders.member_id' => $session['data']->id, 'orders.app_code' => 2, 'orders.pool_id' => $value->pool_id])
								->where_in('orders.order_status_code', $this->list_code_ts)
								->group_by('orders.id')
								->order_by('orders.created', 'desc')
								->get();

							$content[] = ['id' => $value->pool_id, 'title' => $value->pool, 'items' => $query->result()];
						}

						$pool_id = $value->pool_id;

					}

	 				$response['status']  = 'success';
					$response['message'] = 'list order';
					$response['data']    =  $content;

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function listHistoryOrderTS()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$query = $this->db->select(['orders.id as order_id',
											'DATE_FORMAT(orders.created, "%d-%m-%Y") as created',
											'order_statuses.ts_title',
											'sum(order_details.product_qty) as sum_product',
											// '(orders.totalAmountInCart + orders.ongkir) as totalAmountInCart',
											'(IFNULL(orders.totalAmountInCart, 0) - IFNULL(orders.komisi, 0) - IFNULL(orders.nominal_promo, 0) + IFNULL(orders.ongkir, 0))  as totalAmountInCart',
											'orders.order_status_code',
											'orders.code',
											'payment_types.title as payment',
											'orders.app_code'])
								->from('orders')
								->join('order_details', 'order_details.order_id = orders.id', 'inner')
								->join('order_statuses', 'order_statuses.code = orders.order_status_code', 'left')
								->join('payment_types', 'payment_types.id = orders.payment_type_id', 'left')
								->where(['orders.wilayah_detail_id' => $session['data']->id, 'orders.app_code' => 2])
								// ->where_in('orders.order_status_code', $list_code_customer)
								->group_by('orders.id')
								->order_by('orders.created', 'desc')
								->get();

				if($query == null){

					$response['message'] = 'orders tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}


	 				$response['status']  = 'success';
					$response['message'] = 'list order';
					$response['data']    =  $query->result();



		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function listOrderCustomer()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}


				$query = $this->db->select(['orders.id as order_id',
											'DATE_FORMAT(orders.created, "%d-%m-%Y") as created',
											'order_statuses.ts_title',
											'sum(order_details.product_qty) as sum_product',
											'orders.totalAmountInCart',
											'orders.pool_id',
											'orders.code',
											'orders.order_status_code',
											'wilayah_pools.title as pool',
											'payment_types.title as payment',
											'orders.app_code'])
								->from('orders')
								->join('order_details', 'order_details.order_id = orders.id and order_details.status_detail = 1', 'inner')
								->join('order_statuses', 'order_statuses.code = orders.order_status_code', 'left')
								->join('payment_types', 'payment_types.id = orders.payment_type_id', 'left')
								->join('wilayah_pools', 'wilayah_pools.id = orders.pool_id', 'left')
								// ->where(['orders.app_code' => 2])
								->where(['orders.wilayah_detail_id' => $session['data']->id, 'orders.app_code' => 1])
								->where_in('orders.order_status_code', $this->code_pending_customer)
								->group_by('orders.id')
								->order_by('orders.created', 'desc')
								->get();

				if($query == null){

					$response['message'] = 'orders tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

					$pool_id = "";
					$content = [];

					foreach($query->result() as $value) {

						if($pool_id != $value->pool_id){

							$query = $this->db->select(['orders.id as order_id',
											'DATE_FORMAT(orders.created, "%d-%m-%Y") as created',
											'order_statuses.ts_title',
											'sum(order_details.product_qty) as sum_product',
											'(orders.totalAmountInCart + orders.ongkir - IFNULL(orders.nominal_promo,0)) as totalAmountInCart',
											'orders.pool_id',
											'orders.code',
											'orders.order_status_code',
											'wilayah_pools.title as pool',
											'payment_types.title as payment',
											'orders.app_code',
											'orders.order_status_code'])
								->from('orders')
								->join('order_details', 'order_details.order_id = orders.id and order_details.status_detail = 1', 'inner')
								->join('order_statuses', 'order_statuses.code = orders.order_status_code', 'left')
								->join('payment_types', 'payment_types.id = orders.payment_type_id', 'left')
								->join('wilayah_pools', 'wilayah_pools.id = orders.pool_id', 'left')
								// ->where(['orders.app_code' => 2])
								->where(['orders.wilayah_detail_id' => $session['data']->id, 'orders.app_code' => 1])
								->where_in('orders.order_status_code', $this->code_pending_customer)
								->group_by('orders.id')
								->order_by('orders.created', 'desc')
								->get();

							$content[] = ['id' => $value->pool_id, 'title' => $value->pool, 'items' => $query->result()];
						}

						$pool_id = $value->pool_id;

					}

	 				$response['status']  = 'success';
					$response['message'] = 'list order';
					$response['data']    =  $content;

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	// public function listOrderCustomer()
	// {
	// 	// $obj = new StdClass();
	// 	$obj = [];
	// 	$response = array("status" => 'failed', "data" => $obj, "message" => '');

	// 	$_POST = json_decode(file_get_contents("php://input"), true);
	// 	$data = $this->input->post();

	// 	$this->form_validation->set_rules('token', 'token', 'required');

	// 	if ($this->form_validation->run() == TRUE) {

	// 			$session = $this->checkToken($data['token']);
	// 			if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

	// 				$response["message"] = $session['tokenStatus'];

	// 				return $this->output->set_content_type('application/json')
	// 		            ->set_status_header(200)
	// 		            ->set_output(json_encode($response));

	// 			}

	// 			// $list_code_customer = array('OS01', 'OS05', 'OS07', 'OS08', 'OS09', 'OS02', 'OS10', 'OS09');
	// 			$query = $this->db->select(['orders.id as order_id',
	// 										'DATE_FORMAT(orders.created, "%d-%m-%Y") as created',
	// 										'order_statuses.ts_title',
	// 										'sum(order_details.product_qty) as sum_product',
	// 										'(orders.totalAmountInCart + orders.ongkir) as totalAmountInCart',
	// 										'orders.code',
	// 										'orders.order_status_code',
	// 										'payment_types.title as payment',
	// 										'orders.app_code'])
	// 							->from('orders')
	// 							->join('order_details', 'order_details.order_id = orders.id', 'inner')
	// 							->join('order_statuses', 'order_statuses.code = orders.order_status_code', 'left')
	// 							->join('payment_types', 'payment_types.id = orders.payment_type_id', 'left')
	// 							->where(['orders.wilayah_detail_id' => $session['data']->id, 'orders.app_code' => 1])
	// 							->where_in('orders.order_status_code', $this->code_pending_customer)
	// 							->group_by('orders.id')
	// 							->order_by('orders.created', 'desc')
	// 							->get();

	// 			if($query == null){

	// 				$response['message'] = 'orders tidak ditemukan';
	// 				return $this->output->set_content_type('application/json')
	// 					            ->set_status_header(200)
	// 					            ->set_output(json_encode($response));
	// 			}else{

	//  				$response['status']  = 'success';
	// 				$response['message'] = 'list order';
	// 				$response['data']    =  $query->result();

	// 			}

	// 	}else{

	// 		$response["message"] = (string) json_encode($this->form_validation->error_array());

	// 	}

	// 	return $this->output->set_content_type('application/json')
	// 	            ->set_status_header(200)
	// 	            ->set_output(json_encode($response));
	// }

	public function listHistoryOrderCustomer()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				// $list_code_customer = array('OS01', 'OS05', 'OS07', 'OS08', 'OS09', 'OS02', 'OS10', 'OS09');
				$query = $this->db->select(['orders.id as order_id',
											'DATE_FORMAT(orders.created, "%d-%m-%Y") as created',
											'order_statuses.ts_title',
											'sum(order_details.product_qty) as sum_product',
											// '(orders.totalAmountInCart + orders.ongkir) as totalAmountInCart',
											'(orders.totalAmountInCart + orders.ongkir - IFNULL(orders.nominal_promo,0)) as totalAmountInCart',
											'orders.order_status_code',
											'orders.code',
											'payment_types.title as payment',
											'orders.app_code'])
								->from('orders')
								->join('order_details', 'order_details.order_id = orders.id', 'inner')
								->join('order_statuses', 'order_statuses.code = orders.order_status_code', 'left')
								->join('payment_types', 'payment_types.id = orders.payment_type_id', 'left')
								->where(['orders.wilayah_detail_id' => $session['data']->id, 'orders.app_code' => 1])
								// ->where_in('orders.order_status_code', $list_code_customer)
								->group_by('orders.id')
								->order_by('orders.created', 'desc')
								->get();

				if($query == null){

					$response['message'] = 'orders tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

	 				$response['status']  = 'success';
					$response['message'] = 'list order';
					$response['data']    =  $query->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function detail()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('order_id', 'order_id', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$query = $this->db->select(['orders.id as order_id',
											'orders.created',
											'orders.code',
											'orders.order_expired',
											'order_statuses.ts_title',
											'sum(order_details.product_qty) as sum_product',
											'orders.cust_addr_title',
											'orders.order_status_code',
											'wilayah_pools.lat',
											'wilayah_pools.lng',
											'wilayah_pools.title as pool',
											'wilayah_pools.delivery_time',
											'wilayah_pools.address',
											'wilayah_pools.g_route',
											'(IFNULL(orders.totalAmountInCart, 0) - IFNULL(orders.komisi, 0) - IFNULL(orders.nominal_promo, 0) + IFNULL(orders.ongkir, 0))  as totalAmountInCart',
											'wilayah_details.phone',
											'payment_types.title as payment_type',
											'payment_types.title as payment',
											'orders.app_code',
											'orders.promo_code',
											'ifnull(orders.komisi, 0) as total_komisi',
											'ifnull(orders.nominal_promo, 0) as nominal_promo',
											'(orders.totalAmountInCart) as nilai_awal'
										])
								->from('orders')
								->join('order_details', 'order_details.order_id = orders.id', 'inner')
								->join('wilayah_pools', 'wilayah_pools.id = orders.pool_id', 'left')
								->join('wilayah_details', 'wilayah_details.id = orders.member_id', 'left')
								->join('order_statuses', 'order_statuses.code = orders.order_status_code', 'left')
								->join('payment_types', 'payment_types.id = orders.payment_type_id', 'left')
								->where(['orders.id' => $data['order_id']])
								->limit(1)
								->order_by('orders.created', 'desc')
								->group_by('order_id')
								->get();

				if($query == null){

					$response['message'] = 'orders tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

							$detail =   $this->db->select(['products.id as product_id',
														  'products.title',
														  'IFNULL(order_details.product_price_ts, 0) as unitprice_ts',
														  'order_details.productpicture_title as img',
														  'order_details.product_satuan_simbol',
														  'order_details.product_berat_kemasan as berat_kemasan',
														  'order_details.product_satuan_title as satuan',
														  'order_details.product_kemasan_title as type',
														  'order_details.product_kemasan_simbol as simbol',
														  'order_details.product_qty'
														])
												->from('order_details')
												->join('products', 'order_details.product_id = products.id', 'join')
												->join('productpictures', 'productpictures.product_id = products.id', 'left')
												->where(['order_details.order_id' => $data['order_id'], 'productpictures.status' => 1 ])
												->group_by('order_details.id')
												->get();

					$response['base_url']  = $this->base_url.'sayur/app/webroot/files/product/';
	 				$response['status']    = 'success';
					$response['message']   = 'list order';
					$response['data']      =  $query->row();
					$response['detail']    =  $detail->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}


	public function detailHour()
	{

		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('order_id', 'order_id', 'required');
		$this->form_validation->set_rules('duration', 'duration', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$query = $this->db->select(['orders.id as order_id',
											'orders.created',
											'orders.code',
											'orders.order_expired',
											'order_statuses.ts_title',
											'sum(order_details.product_qty) as sum_product',
											'orders.cust_addr_title',
											'wilayah_pools.lat',
											'wilayah_pools.lng',
											'wilayah_pools.address',
											'wilayah_pools.g_route',
											'CAST(orders.totalAmountInCart AS SIGNED) as totalAmountInCart',
											'wilayah_details.phone',
											'orders.order_status_code',
											'payment_types.title as payment_type',
											'payment_types.title as payment',
											'orders.app_code',
											'payment_notifications.channel_names',
										    'payment_notifications.transaction_time',
										    'payment_notifications.transaction_status',
										    'payment_notifications.payment_type',
										    'payment_notifications.payment_code',
										    'payment_notifications.status_code'])
								->from('orders')
								->join('order_details', 'order_details.order_id = orders.id', 'inner')
								->join('wilayah_pools', 'wilayah_pools.id = orders.pool_id', 'left')
								->join('wilayah_details', 'wilayah_details.id = orders.member_id', 'left')
								->join('order_statuses', 'order_statuses.code = orders.order_status_code', 'left')
								->join('payment_types', 'payment_types.id = orders.payment_type_id', 'left')
								->join('payment_notifications', 'payment_notifications.order_id = orders.id', 'left')
								->where(['orders.id' => $data['order_id']])
								->limit(1)
								->order_by('payment_notifications.created', 'desc')
								->group_by('order_id')
								->get();

				if($query == null){

					$response['message'] = 'orders tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

							$detail =   $this->db->select(['products.id as product_id',
														  'products.title',
														  'CAST(IFNULL(order_details.product_price_ts, 0) AS SIGNED) as unitprice_ts',
														  'order_details.productpicture_title as img',
														  'order_details.product_satuan_simbol',
														  'order_details.product_berat_kemasan as berat_kemasan',
														  'order_details.product_satuan_title as satuan',
														  'order_details.product_kemasan_title as type',
														  'order_details.product_kemasan_simbol as simbol',
														  'order_details.product_qty'
														])
												->from('order_details')
												->join('products', 'order_details.product_id = products.id', 'join')
												->join('productpictures', 'productpictures.product_id = products.id', 'left')
												->where(['order_details.order_id' => $data['order_id'], 'productpictures.status' => 1 ])
												->group_by('order_details.id')
												->get();

					$response['base_url']  = $this->base_url.'sayur/app/webroot/files/product/';
	 				$response['status']    = 'success';
					$response['message']   = 'list order';
					$response['data']      =  $query->row();
					$response['detail']    =  $detail->result();

					$response['payment_code'] = "";
					$response['sisa_waktu_order'] = "";
					$response['order_expired'] = "";
					$response['diffInMinutes'] = $data['duration'];

					$get_transaction_time = $this->db->select(['payment_notifications.transaction_time',
															   'payment_code',
															   'orders.order_expired',
															   'channel_payment_code'])
													 ->from('payment_notifications')
													 ->join('orders', 'orders.id = payment_notifications.order_id', 'inner')
													 ->where('payment_notifications.order_id', $data['order_id'])
													 ->order_by('payment_notifications.id', 'desc')
													 ->limit(1)
													 ->get()
													 ->row();

					if($get_transaction_time){

						$response['order_expired'] = $get_transaction_time->order_expired;

						$start  = Carbon::now();
						$end    = Carbon::parse($response['order_expired']);

						// date_default_timezone_set('Asia/Jakarta');
						// $awal  = strtotime($get_transaction_time->transaction_time);
						// $akhir = strtotime($response['order_expired']);
						// $diff  = $akhir - $awal;
						// $jam   = floor($diff / (60 * 60));
						// $menit = $diff - $jam * (60 * 60);
						// $response['sisa_waktu_order'] = $jam .  ' jam, ' . floor( $menit / 60 ) . ' menit';

						 $t1 = strtotime($start->toDateTimeString());
        				 $t2 = strtotime($response['order_expired']);

						$dtd = new stdClass();
				        $dtd->interval = $t2 - $t1;
				        // $dtd->total_sec = abs($t2-$t1);
				        $dtd->total_sec = $t2-$t1;
				        $dtd->total_min = floor($dtd->total_sec/60);

				        // print_r($dtd->total_min);die();

						$response['sisa_waktu_order'] = intval($dtd->total_min);


						if($dtd->total_min < 0){
							$response['sisa_waktu_order'] = 0;
						}

						// $response['sisa_waktu_order'] = $start->diffInHours($end) . ':' . $start->diff($end)->format('%I:%S');

						$response['payment_code'] = $get_transaction_time->channel_payment_code;
						$response['diffInMinutes'] = $data['duration'];

						// $transaction_time = $get_transaction_time->transaction_time;

						// $date = Carbon::parse($transaction_time);
						// // $expired = $date->addMinutes($data['duration'])->toDateTimeString();
						// $expired = $date->toDateTimeString();

						// $now = Carbon::now()->toDateTimeString();

						// $start  = new Carbon($now);
						// $end    = new Carbon($expired);


						// if(Carbon::now() > $expired){
						// 	$diff = 0;
						// }else{
						// 	$diff = $end->diffInMinutes($start);
						// }

					}



				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function detailCustomer()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('order_id', 'order_id', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$query = $this->db->select(['orders.id as order_id',
											'orders.created',
											'orders.code',
											'orders.order_expired',
											'order_statuses.ts_title',
											'sum(order_details.product_qty) as sum_product',
											'orders.cust_addr_title',
											'concat(orders.cust_addr_detail, ", ", orders.cust_addr_adm_area_level_1, ", ", orders.cust_addr_adm_area_level_2, ", ", orders.cust_addr_adm_area_level_3, ", ", orders.cust_addr_adm_area_level_4, ", ", orders.cust_addr_postal_code) as cust_addr_detail',
											'orders.cust_addr_lat',
											'orders.cust_addr_lng',
											'CAST(( (sum(order_details.profit_ts)) + ((orders.ongkir + orders.ongkir_tambahan) * orders.index_ongkir_ts) ) AS SIGNED) as komisi',
											// '( (sum(order_details.profit_ts)) + (orders.ongkir * orders.index_ongkir_ts) )  as komisi',
											'orders.cust_addr_g_route',
											'orders.order_status_code',
											'(orders.totalAmountInCart + orders.ongkir - IFNULL(orders.nominal_promo,0)) as totalAmountInCart',
											'orders.cust_addr_phone as phone',
											'auth_members.fullname',
											'payment_types.title as payment_type',
											'payment_types.title as payment',
											'orders.app_code',
											// 'ifnull(orders.komisi, 0) as komisi',
											'orders.promo_code',
											'ifnull(orders.nominal_promo, 0) as nominal_promo'])
								->from('orders')
								->join('order_details', 'order_details.order_id = orders.id and order_details.status_detail = 1', 'inner')
								->join('wilayah_details', 'wilayah_details.id = orders.member_id', 'left')
								->join('order_statuses', 'order_statuses.code = orders.order_status_code', 'left')
								->join('auth_members', 'auth_members.id = orders.member_id', 'left')
								->join('payment_types', 'payment_types.id = orders.payment_type_id', 'left')
								->where(['orders.id' => $data['order_id']])
								->limit(10)
								->order_by('orders.created', 'desc')
								->group_by('order_id')
								->get();

				if($query == null){

					$response['message'] = 'orders tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

							$detail =   $this->db->select(['products.id as product_id',
														  'products.title',
														  '(IFNULL(order_details.product_unitprice, 0) * order_details.product_qty) as unitprice_ts',
														  'order_details.productpicture_title as img',
														  'order_details.product_satuan_simbol',
														  'order_details.product_berat_kemasan as berat_kemasan',
														  'order_details.product_satuan_title as satuan',
														  'order_details.product_kemasan_title as type',
														  'order_details.product_kemasan_simbol as simbol',
														  'order_details.product_qty'
														])
												->from('order_details')
												->join('products', 'order_details.product_id = products.id', 'join')
												->join('productpictures', 'productpictures.product_id = products.id', 'left')
												->where(['order_details.order_id' => $data['order_id'], 'productpictures.status' => 1, 'order_details.status_detail' => 1 ])
												->group_by('order_details.id')
												->get();

					$response['base_url']  = $this->base_url.'sayur/app/webroot/files/product/';
	 				$response['status']    = 'success';
					$response['message']   = 'list order';
					$response['data']      =  $query->row();
					$response['detail']    =  $detail->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function listPayment()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$query = $this->db->select(['payment_types.id',
											'payment_types.title',
											'payment_types.image',
											'payment_types.detail'])
								->from('payment_types')
								->where(['payment_types.is_ts' => 1])
								->order_by('payment_types.id', 'desc')
								->get();

				$sisa_plafon = 0;
				$get_sisa_plafon = $this->db->select(['plafon_histories.total',
			   											  'plafon_histories.id',
			   											  'plafon_histories.total',
			   											  'plafon_histories.created',
			   											  'ts_grades.plafon_to'])
												->from('plafon_histories')
												->join('ts_grades', 'ts_grades.id=plafon_histories.ts_grade_id', 'inner')
												->where(['plafon_histories.wilayah_detail_id' => $session['data']->id])
												->order_by('plafon_histories.id', 'desc')
												->get()
												->row();

					if($get_sisa_plafon){
						$sisa_plafon = $get_sisa_plafon->total;
				    }

				$sisa_komisi = 0;
				$get_sisa_komisi = $this->db->select(['komisi_histories.total',
		   											  'komisi_histories.id',
		   											  'komisi_histories.created'])
												->from('komisi_histories')
												->where(['komisi_histories.wilayah_detail_id' => $session['data']->id])
												->order_by('komisi_histories.id', 'desc')
												->get()
												->row();

					if($get_sisa_komisi){
						$sisa_komisi = $get_sisa_komisi->total;
				    }

				if($query == null){

					$response['message'] = 'list payment tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

	 				$response['status']  = 'success';
					$response['message'] = 'list payment';
					$response['data']    =  $query->result();
					$response['sisa_plafon'] = $sisa_plafon;
					$response['sisa_komisi'] = $sisa_komisi;

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function pickBetweenOrder()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();


		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('order_id', 'order_id', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$query = $this->db->select(['orders.id as order_id'])
								->from('orders')
								->where(['orders.id' => $data['order_id']])
								->get()
								->row();

				if($query == null){

					$response['message'] = 'orders tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}

				$this->db->trans_begin();
		    	try {

		    		$code="";
		    		if($data["status"]==$this->order_code_sent){

		    			$code = $this->order_code_sent;

		    		}
		    		else if($data["status"]==$this->order_code_pick){

		    			$code = $this->order_code_pick;

		    		}elseif ($data["status"]==$this->order_code_cancel) {

		    			$code = $this->order_code_cancel;
		    			$get_amount = $this->db->select(['totalAmountInCart', 'komisi', 'nominal_promo'])
		    							      ->from('orders')
		    							      ->where(['id' => $data['order_id']])
		    							      ->get()
		    							      ->row();

		    			$sisa_plafon = $this->sisaPlafon($session['data']->id);
		    			$total_sisa_plafon = ($get_amount->totalAmountInCart - $get_amount->komisi - $get_amount->nominal_promo) + $sisa_plafon;

		    			//insert plafon histories
						$plafon_histories['order_id'] = $data['order_id'];
						$plafon_histories['order_status_code'] = $code;
						$plafon_histories['wilayah_detail_id'] = $session['data']->id;
						$plafon_histories['ts_grade_id'] = $session['data']->ts_grade_id;
						$plafon_histories['type_of_transaction'] = "0";
						$plafon_histories['total'] = $total_sisa_plafon;
						$plafon_histories['amount_order'] = $get_amount->totalAmountInCart -  $get_amount->komisi - $get_amount->nominal_promo;
						$plafon_histories['remark'] = 'order by '.$session['data']->title;
						$plafon_histories['status'] = 1;
						$plafon_histories['created'] = date('Y-m-d H:i:s', NOW());
						$plafon_histories['created_by'] = $session['data']->id;
						$this->db->insert('plafon_histories', $plafon_histories);

						$wilayah_details['sisa_plafon'] = $total_sisa_plafon;
						$update_wilyah_details = $this->db->where(['id' => $session['data']->id])
								   					      ->update('wilayah_details', $wilayah_details);

						$total_komisi = 0;	   

						//insert komisi histories
						$last_total_komisi = $this->db->select('total')
													  ->from('komisi_histories')
													  ->where(['komisi_histories.wilayah_detail_id' => $session['data']->id])
													  ->order_by('id', 'desc')
													  ->get()
													  ->row();

						if($last_total_komisi){
		    				$total_komisi = $last_total_komisi->total;
		    			}

						$komisi_histories['order_id'] = $data['order_id'];
						$komisi_histories['order_status_code'] = $code;
						$komisi_histories['wilayah_detail_id'] = $session['data']->id;
						$komisi_histories['type_of_transaction'] = 0;
						$komisi_histories['total'] = $total_komisi + $get_amount->komisi;
						$komisi_histories['amount'] = $get_amount->komisi;
						$komisi_histories['remark'] = 'cancel order by '.$session['data']->title;
						$komisi_histories['status'] = 1;
						$komisi_histories['is_cash'] = 0;
						$komisi_histories['created'] = date('Y-m-d H:i:s', NOW());
						$komisi_histories['created_by'] = $session['data']->id;
						if($get_amount->komisi > 0) {
							$this->db->insert('komisi_histories', $komisi_histories);
						}

						//counter penggunaan promo all user
						$promo = $this->db->select(['promo_code'])
										  ->from('orders')
										  ->where('id', $data['order_id'])
										  ->get()
										  ->row()
										  ->promo_code;
						if(!empty($promo)) {
							$current_pemakaian = $this->db->select(['pemakaian_quota'])
															  ->from('promos')
															  ->where([
																  'code' => $promo,
																  'status' => '1',
																  'publish' => '1',
						  										  'app_code' => '2',
						  										  'type_promo' => '1'
															  ])
															  ->get()
															  ->row()->pemakaian_quota;
							if($current_pemakaian <= 0) {
								$counter_pemakaian = "update promos
													  set pemakaian_quota = pemakaian_quota - 1
													  where code = '".$promo."'
													  and status = '1'
			  										  and publish = '1'
			  										  and app_code = '2'
			  										  and type_promo = '1'";
								$this->db->query($counter_pemakaian);
							}
						}

		    		}elseif ($data["status"]==$this->order_code_sent_success) {

		    			$code = $this->order_code_sent_success;

		    		}elseif ($data["status"]=="4") {

		    			//setor duit COD
		    // 			$code = 'OS20';

		    // 			$komisi = 0;
		    // 			$last_komisi = 0;
		    // 			$total_komisi = 0;

		    // 			$get_last_komisi = $this->db->select(['komisi_histories.total'])
		    // 										->from('komisi_histories')
		    // 										->where(['komisi_histories.wilayah_detail_id' => $session['data']->id])
		    // 										->order_by('komisi_histories.id', 'desc')
		    // 										->limit(1)
		    // 										->get()
		    // 										->row();

		    // 			if($get_last_komisi){
		    // 				$last_komisi = $get_last_komisi->total;
		    // 			}

		    // 			$get_komisi = $this->db->select(['orders.total_komisi'])
		    // 										->from('orders')
		    // 										->where(['orders.id' => $data['order_id']])
		    // 										->limit(1)
		    // 										->get()
		    // 										->row();

		    // 			if($get_komisi){
		    // 				$komisi = $get_komisi->total_komisi;
		    // 			}

		    // 			$total_komisi = $last_komisi + $komisi;

		    // 			//insert komisi histories
						// $komisi_histories['order_id'] = $data['order_id'];
						// $komisi_histories['order_status_code'] = $code;
						// $komisi_histories['wilayah_detail_id'] = $session['data']->id;
						// $komisi_histories['type_of_transaction'] = 0;
						// $komisi_histories['total'] = $total_komisi;
						// $komisi_histories['amount'] = $data['hargatotal'];
						// $komisi_histories['remark'] = 'order by '.$session['data']->title;
						// $komisi_histories['status'] = 1;
						// $komisi_histories['is_cash'] = 0;
						// $komisi_histories['created'] = date('Y-m-d H:i:s', NOW());
						// $komisi_histories['created_by'] = $session['data']->id;
						// $this->db->insert('komisi_histories', $komisi_histories);
		    		}

					//order history
					$order_histories['order_id'] = $data['order_id'];
					$order_histories['order_status_code	'] = $code;
					$order_histories['remark'] = 'order status changed by '.$session['data']->title;
					$order_histories['status'] = 1;
					$order_histories['created'] = date('Y-m-d H:i:s', NOW());
					$order_histories['created_by'] = $session['data']->id;
					$this->db->insert('order_histories', $order_histories);

					$orders['order_status_code'] = $code;
					$this->db->where(['id' => $data['order_id']])->update('orders', $orders);

				} catch (Exception $e) {
		    		 $this->db->trans_rollback();
					 $response["message"] = "terjadi kesalahan pada database (rollback)";
		    	}

		    	if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback();
					    $response["message"] = "terjadi kesalahan pada database (rollback)";
				}else {
				        $this->db->trans_commit();
				        $response["status"] = 'success';
				        $response["message"] = 'data berhasil disimpan';
				}

		}else{

			$response["message"] = json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function pickPaymentNotification()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		// $this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('order_id', 'order_id', 'required');
		// $this->form_validation->set_rules('duration', 'duration', 'required');

		if ($this->form_validation->run() == TRUE) {

				// $session = $this->checkToken($data['token']);
				// if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

				// 	$response["message"] = $session['tokenStatus'];

				// 	return $this->output->set_content_type('application/json')
			 //            ->set_status_header(200)
			 //            ->set_output(json_encode($response));

				// }

				$query = $this->db->select(['orders.id','orders.member_id'])
								->from('orders')
								->where(['orders.id' => $data['order_id']])
								->limit(1)
								->order_by('orders.created', 'desc')
								->get();

				if($query == null){

					$response['message'] = 'orders tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}


				$this->db->trans_begin();
		    	try {
		    			//insert payment_notifications
						$payment_notifications['order_id'] = $data['order_id'];
						// $payment_notifications['order_status_code'] = 'OS04';
						$payment_notifications['order_id_midtrans'] = $data['order_id'];
						$payment_notifications['channel_names'] = @$data['store'];
						$payment_notifications['channel_payment_code'] = @$data['payment_code'];
						$payment_notifications['payment_code'] = @$data['payment_code'];
						// $payment_notifications['paid_at'] = $data['hargatotal'];
						$payment_notifications['store'] = @$data['store'];
						// $payment_notifications['bill_key'] = 1;
						// $payment_notifications['biller_code'] = 1;
						// $payment_notifications['permata_va_number'] = 1;
						// $payment_notifications['approval_code'] = 1;
						// $payment_notifications['masked_card'] = 1;
						// $payment_notifications['bank'] = 1;
						// $payment_notifications['eci'] = 1;
						// $payment_notifications['channel_response_code'] = 1;
						// $payment_notifications['channel_response_message'] = 1;
						$payment_notifications['transaction_time'] = @$data['transaction_time'];
						$payment_notifications['gross_amount'] = @$data['gross_amount'];
						// $payment_notifications['currency'] = 1;
						$payment_notifications['payment_type'] = @$data['payment_type'];
						// $payment_notifications['signature_key'] = 1;
						$payment_notifications['status_code'] = @$data['status_code'];
						$payment_notifications['transaction_id'] = @$data['transaction_id'];
						$payment_notifications['transaction_status'] = @$data['transaction_status'];
						// $payment_notifications['fraud_status'] = 1;
						$payment_notifications['status'] = 0;
						$payment_notifications['status_message'] = @$data['status_message'];
						// $payment_notifications['detail'] = 1;
						// $payment_notifications['upload_file'] = 1;
						// $payment_notifications['fraud_status'] = 1;
						$payment_notifications['created'] = date('Y-m-d H:i:s', NOW());
						$payment_notifications['log'] = json_encode($data);
						// $payment_notifications['modified'] = ;
						$this->db->insert('payment_notifications', $payment_notifications);

						$order_id = $data['order_id']; 

		    			if('H' == substr(strtoupper($order_id), 0,1)) {

		    				$get_hutang_payment = $this->db->select(['*'])
		    											   ->from('hutang_payments')
		    											   ->where(['hutang_id' => $data['order_id']])
		    											   ->get();

		    				if($get_hutang_payment != null){

		    					foreach ($get_hutang_payment as $key => $value) {

									$hutang_payments['hutang_id'] = $data['order_id'];
									$hutang_payments['status_code'] = @$data['status_code'];
									$hutang_payments['log'] = 'update  to '.@$data['status_code'];
									$hutang_payments['hutang_expired'] = Carbon::now()->addMinutes($this->minute_expired)->toDateTimeString();
									$hutang_payments['modified'] = date('Y-m-d H:i:s', NOW());

									$hutang_payments_x[] = $hutang_payments;
			    				}

			    				if($hutang_payments_x != null){

									$this->db->update_batch('hutang_payments', $hutang_payments_x, 'hutang_id');

								}

			    				$hutang_payments_histories['hutang_id'] = $data['order_id'];
								$hutang_payments_histories['status_code'] = @$data['status_code'];
								$hutang_payments_histories['remark'] = 'hutang setup payment';
								$hutang_payments_histories['log'] = 'hutang setup payment';
								$hutang_payments_histories['status'] = 1;
								$hutang_payments_histories['created'] = date('Y-m-d H:i:s', NOW());

								$this->db->insert('hutang_payments_histories', $hutang_payments_histories);

		    				}


		    			}else{

		    				$orders['order_expired'] = Carbon::now()->addMinutes($this->minute_expired)->toDateTimeString();
							$orders['order_status_code'] = $this->order_waiting_payment;
							$orders['order_id_midtrans'] = $data['order_id'];
							$update_orders = $this->db->where(['id' => $data['order_id']])
									   					      ->update('orders', $orders);

						    //order history
							$order_histories['order_id'] = $data['order_id'];
							$order_histories['order_status_code	'] = $this->order_waiting_payment;
							$order_histories['remark'] = 'order status changed by android';
							$order_histories['status'] = 1;
							$order_histories['created'] = date('Y-m-d H:i:s', NOW());
							$order_histories['created_by'] = $query->row()->member_id;
							$this->db->insert('order_histories', $order_histories);

		    			}

				} catch (Exception $e) {
		    		 $this->db->trans_rollback();
					 $response["message"] = "terjadi kesalahan pada database (rollback)";
		    	}

		    	if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback();
					    $response["message"] = "terjadi kesalahan pada database (rollback)";
				}else {
				        $this->db->trans_commit();
				        $response["status"] = 'success';
				        $response["message"] = 'data berhasil disimpan';

				        $response["data"]["order_id"] = $data['order_id'];
				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function totalOrder()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$total = 0;

				$query = $this->db->select(['orders.id'])
								->from('orders')
								->where(['orders.wilayah_detail_id' => $session['data']->id, 'orders.app_code' => 1])
								->where_in('orders.order_status_code', $this->code_pending_customer)
								->count_all_results();

				$query2 = $this->db->select(['orders.id'])
								->from('orders')
								->where(['orders.member_id' => $session['data']->id, 'orders.app_code' => 2])
								->where_in('orders.order_status_code', $this->list_code_ts)
								->count_all_results();

				$total = $query + $query2;

 				$response['status']  = 'success';
				$response['message'] = 'Total order';
				$response['data']    =  $total;



		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	//list available promo
	public function listAvailablePromo()
	{
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$wilayah_id = $session['data']->wilayah_id;
				$ts_grade_id = $session['data']->ts_grade_id;

				$query = $this->db->select([
										'promos.id', 'promos.code', 'promos.title', 'promos.image',
										'promos.intro', 'promos.nilai', 'promos.is_percentage',
										'promos.desc', 'promos.url_terkait', 'promos.start_date',
										'promos.end_date', 'promos.pic_telp'
									])
									->from('promos')
									->join('promo_wilayahs', 'promo_wilayahs.promo_id = promos.id')
									->where([
										'promo_wilayahs.wilayah_id' => $wilayah_id,
										'promo_wilayahs.ts_grade_id' => $ts_grade_id,
										'promo_wilayahs.status' => 1,
										'promos.status' => 1,
										'promos.publish' => 1,
										'promos.app_code' => 2,
										'promos.type_promo' => 1
									])
									->get()
									->result();


 				$response['status']  = 'success';
				$response['message'] = 'List Available Promo';
				$response['data']    =  $query;

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	// check promo ticket
	public function checkPromo()
	{
		$obj = new StdClass();
		$list_potongan = array();
		$response = array("status" => 'failed', "data" => $obj, "message" => '', "list_potongan" => $list_potongan);

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();
		$list_order = json_decode($data['list_order']);

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));
				}

				$is_valid = false;
				$nominal_order = $data['nominal_order'];
				$promo_code = $data['promo_code'];
				$wilayah_id = $session['data']->wilayah_id;
				$ts_grade_id = $session['data']->ts_grade_id;
				$user_id = $session['data']->id;

				$query = $this->db->select([
										'promos.id', 'promos.code', 'promos.title', 'promos.image',
										'promos.intro', 'promos.nilai', 'promos.is_percentage',
										'promos.min_order', 'promos.maks_potongan', 'promos.maks_penggunaan',
										'promos.jumlah_quota', 'promos.pemakaian_quota',
										'categories.url_title AS categories_url_title', 'categories.name AS categories_name',
										'promos.desc', 'promos.url_terkait', 'promos.start_date',
										'promos.end_date', 'promos.pic_telp', 'promos.category_id'
									])
									->from('promos')
									->join('promo_wilayahs', 'promo_wilayahs.promo_id = promos.id')
									->join('categories', 'categories.id = promos.category_id','left')
									->where([
										'promos.code' => $promo_code,
										'promo_wilayahs.wilayah_id' => $wilayah_id,
										'promo_wilayahs.ts_grade_id' => $ts_grade_id,
										'promo_wilayahs.status' => 1,
										'promos.status' => 1,
										'promos.publish' => 1,
										'promos.app_code' => 2,
										'promos.type_promo' => 1
									])
									->get()
									->row();
				if(isset($query)) {
					$start_date = strtotime($query->start_date);
					$end_date = strtotime($query->end_date);
					$current_date = strtotime(date('Y-m-d H:i:s'));
					$minimal_order = $query->min_order;
					$maksimal_potongan = $query->maks_potongan;
					$maksimal_penggunaan = $query->maks_penggunaan; //per user
					$jumlah_quota = $query->jumlah_quota; //all user
					$pemakaian_quota = $query->pemakaian_quota;
					$nilai_promo = $query->nilai;
					$is_percentage = $query->is_percentage;
					$category_id = $query->category_id;
					$nominal_by_cat = 0;
					$is_cat_found = false;

					//cek range waktu
					if($current_date < $start_date) {
						$response["message"] = 'Promo belum dimulai';
						$response["data"] = null;
						return $this->output->set_content_type('application/json')
				            ->set_status_header(200)
				            ->set_output(json_encode($response));
					}elseif($current_date > $end_date) {
						$response["message"] = 'Promo telah berakhir';
						$response["data"] = null;
						return $this->output->set_content_type('application/json')
				            ->set_status_header(200)
				            ->set_output(json_encode($response));
					}

					//cek minimal order
					if($minimal_order > 0) {
						if($nominal_order < $minimal_order) {
							$response["message"] = 'Minimal pembelian tidak terpenuhi';
							return $this->output->set_content_type('application/json')
					            ->set_status_header(200)
					            ->set_output(json_encode($response));
						}
					}

					//cek maksimal penggunaan all user
					if($jumlah_quota > 0) {
						if($pemakaian_quota >= $jumlah_quota) {
							$response["message"] = 'Batas penggunaan promo telah tercapai';
							return $this->output->set_content_type('application/json')
					            ->set_status_header(200)
					            ->set_output(json_encode($response));
						}
					}

					//cek maksimal penggunaan per user
					$count_curr_used = $this->db->where([
														'wilayah_detail_id' => $user_id,
														'promo_code' => $promo_code,
														'order_status_code !=' => "OS00"
													])
													->get('orders')
													->num_rows();

					if($count_curr_used >= $maksimal_penggunaan) {
						$response["message"] = 'Batas penggunaan promo anda telah tercapai.';
						return $this->output->set_content_type('application/json')
							->set_status_header(200)
							->set_output(json_encode($response));
					}

					//cek category
					if(!empty($category_id)) {
						$list_available_cat = $this->db->select('id')
													  ->from('categories')
													  ->where('id', $category_id)
													  ->or_where('parent_id', $category_id)
													  ->get()
													  ->result();

						foreach ($list_order as $key => $value) {
							$get_cat_id = $this->db->select('category_id')
												   ->from('products_categories')
												   ->where('product_id', $value->product_id)
												   ->get()
												   ->result();

							foreach ($list_available_cat as $key1 => $value1) {
								foreach ($get_cat_id as $key2 => $value2) {
									if($value2->category_id == $value1->id) {
										$is_cat_found = true;
										$nominal_by_cat += $value->total_amount;
										if($is_percentage == 1) {
											$list_potongan[] = array(
												'product_id' => $value->product_id,
												'amount' => $value->total_amount
											);
										}
										break;
									}
								}
								if($is_cat_found) {
									break;
								}
							}
						}

						if($is_cat_found == false) {
							$response["message"] = 'Kategori voucher tidak sesuai.';
							return $this->output->set_content_type('application/json')
								->set_status_header(200)
								->set_output(json_encode($response));
						}
					}
					else {
						foreach ($list_order as $key => $value) {
							if($is_percentage == 1) {
								$list_potongan[] = array(
									'product_id' => $value->product_id,
									'amount' => $value->total_amount
								);
							}
						}
					}

					//
					$is_valid = true;

					if($is_cat_found) {
						$nominal_order = $nominal_by_cat;
					}

					if($is_percentage == 1) {
						$nilai_promo = $nilai_promo * $nominal_order / 100;
						//potongan per item
						$jml_item_discount = count($list_potongan);

						for($i = 0; $i < $jml_item_discount; $i++) {
							$list_potongan[$i]['amount'] = ($query->nilai * $list_potongan[$i]['amount'] / 100);
						}

						//cek maksimal potongan (khusus promo yg menggunakan prosentase)
						if($maksimal_potongan > 0) {
							if($nilai_promo > $maksimal_potongan) {

								if($jml_item_discount > 0) {
									//tidak proporsional
									// $tambahan_potongan = 0;
									// $selisih = $nilai_promo - $maksimal_potongan;
									// if($selisih > 0) {
									// 	$tambahan_potongan = $selisih / $jml_item_discount;
									// }
									// for($i = 0; $i < $jml_item_discount; $i++) {
									// 	$list_potongan[$i]['amount'] = $list_potongan[$i]['amount'] - $tambahan_potongan;
									// }

									//proporsional
									for($i = 0; $i < $jml_item_discount; $i++) {
										$new_pot = ($list_potongan[$i]['amount']/$nilai_promo) * $maksimal_potongan;
										$list_potongan[$i]['amount'] = number_format($new_pot,2, '.', '');
									}
								}

								$nilai_promo = $maksimal_potongan;
							}
						}
					}
					else {
						//cek maksimal potongan (khusus promo yg tdk menggunakan prosentase)
						if($nilai_promo > $nominal_order) {
							$nilai_promo = $nominal_order;
						}
					}
				}

				if($is_valid) {
					$data_promo = array(
						'id' => $query->id,
						'code' => $query->code,
						'title' => $query->title,
						'image' => $query->image,
						'intro' => $query->intro,
						'nilai' => $nilai_promo,
						'desc' => $query->desc,
						'url_terkait' => $query->url_terkait,
						'start_date' => $query->start_date,
						'end_date' => $query->end_date,
						'pic_telp' => $query->pic_telp,
						'categories_url_title' => $query->categories_url_title,
						'categories_name' => $query->categories_name
					);

					$status = 'success';
					$message = 'Promo valid';
					$data = $data_promo;
				}
				else {
					$status = 'failed';
					$message = 'Promo tidak ditemukan';
				}

				//

 				$response['status']  = $status;
				$response['message'] = $message;
				$response['data']    = $data;
				$response['list_potongan'] = $list_potongan;
		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	//perbaikan totalAmountInCart
	public function fixTableAmountInCart()
	{
		$content = "
			SELECT
				order_id,
				sum(product_unitprice * product_qty) as newTotalAmountInCart,
				totalAmountInCart as totalPayment
			FROM order_details
			JOIN orders ON order_details.order_id = orders.id
			WHERE status_detail = '1'
			GROUP BY order_id;
		";

		$get_total_in_cart = $this->db->query($content)->result();
		foreach ($get_total_in_cart as $key => $value) {
			$data['totalAmountInCart'] = $value->newTotalAmountInCart;
			$data['totalPayment'] = $value->totalPayment;
			$data['id'] = $value->order_id;
			$data_update[] = $data;
		}

		$this->db->trans_begin();
		try {
			//kabel data di eril
			//query update;
			$this->db->update_batch('orders', $data_update, 'id');

			print_r("success update");
		} catch (Exception $e) {
			$this->db->trans_rollback();
			print_r('gagal update');
		}
		$this->db->trans_commit();

	}

}
