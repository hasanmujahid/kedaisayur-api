<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Package extends MY_Controller {

    function __construct()
	{
	   parent::__construct();
	   date_default_timezone_set('Asia/Jakarta');
	}

	public function add()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$data['items'] = json_decode($data['items']);

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('title_package', 'title_package', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);

				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$wad = $this->db->select(['wilayah_id'])
								->from('wilayah_address_details')
								->where(['wilayah_detail_id' => $session['data']->id, 'is_primary' => 1])
								->limit(1)
								->get()
								->row();

				if($wad == null){

					$response['message'] = 'wilayah_address_details_id tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

					$this->db->trans_begin();
			    	try {

			    		// $pakets['url_tile'] = "";
						$pakets['title'] = $data['title_package'];
						$pakets['picture'] = "5ba242a2e6f21.jpg";
						// $pakets['deskripsi'] = "";
						$pakets['publish'] = 1;
						$pakets['status']  = 1;
						$pakets['app_code']  = 2;
						$pakets['created'] = date('Y-m-d H:i:s', NOW());
						$pakets['created_by'] = $session['data']->id;
						$this->db->insert('pakets', $pakets);
						$paket_id = $this->db->insert_id();


			    		for($i=0; $i< count($data['items']); $i++)
						{
							$paket_details[] = array('paket_id' => $paket_id,
													 'grade_id' => $data['items'][$i]->grade_id,
													 'product_id' => $data['items'][$i]->product_id,
													 'qty' => $data['items'][$i]->product_qty);
		 				}

		 				$this->db->insert_batch('paket_details', $paket_details);

			    	} catch (Exception $e) {
			    		 $this->db->trans_rollback();
						 $response["message"] = "terjadi kesalahan pada database (rollback)";
			    	}

			    	if ($this->db->trans_status() === FALSE){
						    $this->db->trans_rollback();
						    $response["message"] = "terjadi kesalahan pada database (rollback)";
					}else {
					        $this->db->trans_commit();
					        $response["status"] = 'success';
					        $response["message"] = 'data berhasil disimpan';
					}

					$get_paket = $this->db->select(['*'])
										->from('pakets')
										->where(['id' => $paket_id])
										->get();

	 				$response['status']  = 'success';
					$response['message'] = 'berhasil menambahkan paket';
					$response['data']    = $get_paket->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function addItem()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$data['items'] = json_decode($data['items']);

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('product_id', 'product_id', 'required');
		$this->form_validation->set_rules('grade_id', 'grade_id', 'required');
		$this->form_validation->set_rules('product_qty', 'product_qty', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);

				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}


				$wad = $this->db->select(['wilayah_id'])
								->from('wilayah_address_details')
								->where(['wilayah_detail_id' => $session['data']->id, 'is_primary' => 1])
								->limit(1)
								->get()
								->row();

				if($wad == null){

					$response['message'] = 'wilayah_address_details_id tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

					$this->db->trans_begin();
			    	try {

			    		for($i=0; $i< count($data['items']); $i++)
						{
							$cekdetail = $this->db->select(['id', 'qty'])
											->from('paket_details')
											->where(['paket_id' => $data['items'][$i]->paket_id,
													 'grade_id' => $data['grade_id'],
													 'product_id' => $data['product_id']])
											->limit(1)
											->get()
											->row();


							if($cekdetail == null){

								$paket_details['paket_id'] = $data['items'][$i]->paket_id;
								$paket_details['grade_id'] = $data['grade_id'];
								$paket_details['product_id'] = $data['product_id'];
								$paket_details['qty'] = $data['product_qty'];

								$this->db->insert('paket_details', $paket_details);

							}else{

								$paket_details['qty'] = $data['product_qty'] + $cekdetail->qty;

								$update = $this->db->where(['paket_id' => $data['items'][$i]->paket_id,
													 'grade_id' => $data['grade_id'],
													 'product_id' => $data['product_id']])
												   ->update('paket_details', $paket_details);

							}

		 				}

			    	} catch (Exception $e) {
			    		 $this->db->trans_rollback();
						 $response["message"] = "terjadi kesalahan pada database (rollback)";
			    	}

			    	if ($this->db->trans_status() === FALSE){
						    $this->db->trans_rollback();
						    $response["message"] = "terjadi kesalahan pada database (rollback)";
					}else {
					        $this->db->trans_commit();
					        $response["status"] = 'success';
					        $response["message"] = 'data berhasil disimpan';
					}

					// $get_paket = $this->db->select(['*'])
					// 					->from('pakets')
					// 					->where(['id' => $paket_id])
					// 					->get();

	 				$response['status']  = 'success';
					$response['message'] = 'berhasil mengubah paket';
					$response['data']    = "";

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}


	public function addRawItem()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$data['items'] = json_decode($data['items']);

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('paket_id', 'paket_id', 'required');
		$this->form_validation->set_rules('title_package', 'title_package', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);

				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}


				$wad = $this->db->select(['wilayah_id'])
								->from('wilayah_address_details')
								->where(['wilayah_detail_id' => $session['data']->id, 'is_primary' => 1])
								->limit(1)
								->get()
								->row();

				if($wad == null){

					$response['message'] = 'wilayah_address_details_id tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

					$this->db->trans_begin();
			    	try {

			    		$paket_id = $data['paket_id'];
						$pakets['title'] = $data['title_package'];

						$update_paket = $this->db->where(['id' => $paket_id])
								   				 ->update('pakets', $pakets);

			    		for($i=0; $i< count($data['items']); $i++)
						{
							$paket_details[] = array('paket_id' => $paket_id,
													 'grade_id' => $data['items'][$i]->grade_id,
													 'product_id' => $data['items'][$i]->product_id,
													 'qty' => $data['items'][$i]->product_qty);
		 				}

		 				$this->db->insert_batch('paket_details', $paket_details);


			    	} catch (Exception $e) {
			    		 $this->db->trans_rollback();
						 $response["message"] = "terjadi kesalahan pada database (rollback)";
			    	}

			    	if ($this->db->trans_status() === FALSE){
						    $this->db->trans_rollback();
						    $response["message"] = "terjadi kesalahan pada database (rollback)";
					}else {
					        $this->db->trans_commit();
					        $response["status"] = 'success';
					        $response["message"] = 'data berhasil disimpan';
					}

					$get_paket = $this->db->select(['*'])
										->from('pakets')
										->where(['id' => $paket_id])
										->get();

	 				$response['status']  = 'success';
					$response['message'] = 'berhasil mengubah paket';
					$response['data']    = $get_paket->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function update()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$data['items'] = json_decode($data['items']);

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('title_package', 'title_package', 'required');
		$this->form_validation->set_rules('paket_id', 'paket_id', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);

				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$paket = $this->db->select(['id'])
								->from('pakets')
								->where(['id' => $data['paket_id']])
								->limit(1)
								->get()
								->row();

				if($paket == null){

					$response['message'] = 'paket tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

			    }

				$wad = $this->db->select(['wilayah_id'])
								->from('wilayah_address_details')
								->where(['wilayah_detail_id' => $session['data']->id, 'is_primary' => 1])
								->limit(1)
								->get()
								->row();

				if($wad == null){

					$response['message'] = 'wilayah_address_details_id tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

					$this->db->trans_begin();
			    	try {

			    		$paket_id = $data['paket_id'];

			    		// $pakets['url_tile'] = "";
						$pakets['title'] = $data['title_package'];
						$pakets['picture'] = "5ba242a2e6f21.jpg";
						// $pakets['deskripsi'] = "";
						// $pakets['publish'] = 1;
						// $pakets['status']  = 1;
						// $pakets['app_code']  = 2;
						$pakets['created'] = date('Y-m-d H:i:s', NOW());
						$pakets['created_by'] = $session['data']->id;

						$update_paket = $this->db->where(['id' => $paket_id])
								   				 ->update('pakets', $pakets);

						$this->db->delete('paket_details', ['paket_id' => $paket_id]);

			    		for($i=0; $i< count($data['items']); $i++)
						{
							$paket_details[] = array('paket_id' => $paket_id,
													 'grade_id' => $data['items'][$i]->grade_id,
													 'product_id' => $data['items'][$i]->product_id,
													 'qty' => $data['items'][$i]->product_qty);
		 				}

		 				$this->db->insert_batch('paket_details', $paket_details);

			    	} catch (Exception $e) {
			    		 $this->db->trans_rollback();
						 $response["message"] = "terjadi kesalahan pada database (rollback)";
			    	}

			    	if ($this->db->trans_status() === FALSE){
						    $this->db->trans_rollback();
						    $response["message"] = "terjadi kesalahan pada database (rollback)";
					}else {
					        $this->db->trans_commit();
					        $response["status"] = 'success';
					        $response["message"] = 'data berhasil disimpan';
					}

					$get_paket = $this->db->select(['*'])
										->from('pakets')
										->where(['id' => $paket_id])
										->get();

	 				$response['status']  = 'success';
					$response['message'] = 'berhasil mengubah paket';
					$response['data']    = $get_paket->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function delete()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();


		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('paket_id', 'paket_id', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);

				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$paket = $this->db->select(['id'])
								->from('pakets')
								->where(['id' => $data['paket_id']])
								->limit(1)
								->get()
								->row();

				if($paket == null){

					$response['message'] = 'paket tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));

			    }

				$wad = $this->db->select(['wilayah_id'])
								->from('wilayah_address_details')
								->where(['wilayah_detail_id' => $session['data']->id, 'is_primary' => 1])
								->limit(1)
								->get()
								->row();

				if($wad == null){

					$response['message'] = 'wilayah_address_details_id tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

					$this->db->trans_begin();
			    	try {

						$pakets['status']  = 0;

						$update_paket = $this->db->where(['id' => $data['paket_id']])
								   				 ->update('pakets', $pakets);


			    	} catch (Exception $e) {
			    		 $this->db->trans_rollback();
						 $response["message"] = "terjadi kesalahan pada database (rollback)";
			    	}

			    	if ($this->db->trans_status() === FALSE){
						    $this->db->trans_rollback();
						    $response["message"] = "terjadi kesalahan pada database (rollback)";
					}else {
					        $this->db->trans_commit();
					        $response["status"] = 'success';
					        $response["message"] = 'data berhasil disimpan';
					}

					$get_paket = $this->db->select(['*'])
										->from('pakets')
										->where(['id' => $data['paket_id']])
										->get();

	 				$response['status']  = 'success';
					$response['message'] = 'berhasil menghapus paket';
					$response['data']    = $get_paket->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	// //update package
	// public function update()
	// {
	// 	$obj = new StdClass();
	// 	$response = array("status" => 'failed', "data" => $obj, "message" => '');

	// 	$_POST = json_decode(file_get_contents("php://input"), true);
	// 	$data = $this->input->post();

	// 	$this->form_validation->set_rules('token', 'token', 'required');

	// 	if ($this->form_validation->run() == TRUE) {

	// 			$session = $this->checkToken($data['token']);

	// 			if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

	// 				$response["message"] = $session['tokenStatus'];

	// 				return $this->output->set_content_type('application/json')
	// 		            ->set_status_header(200)
	// 		            ->set_output(json_encode($response));

	// 			}

	// 			$wad = $this->db->select(['wilayah_id'])
	// 							->from('wilayah_address_details')
	// 							->where(['wilayah_detail_id' => $session['data']->id, 'is_primary' => 1])
	// 							->limit(1)
	// 							->get()
	// 							->row();

	// 			if($wad == null){

	// 				$response['message'] = 'wilayah_address_details_id tidak ditemukan';
	// 				return $this->output->set_content_type('application/json')
	// 					            ->set_status_header(200)
	// 					            ->set_output(json_encode($response));
	// 			}else{

	// 				$this->db->trans_begin();
	// 		    	try {
	// 					$paket = array(
	// 						'title' 	  => $data['title_package'],
	// 						'modified' 	  => date('Y-m-d H:i:s', NOW()),
	// 						'modified_by' => $session['data']->id
	// 					)
	// 					$this->db->where('id', $data['paket_id']);
	// 					$this->db->update('pakets', $pakets);

	// 		    	} catch (Exception $e) {
	// 		    		 $this->db->trans_rollback();
	// 					 $response["message"] = "terjadi kesalahan pada database (rollback)";
	// 		    	}

	// 		    	if ($this->db->trans_status() === FALSE){
	// 					 $this->db->trans_rollback();
	// 					 $response["message"] = "terjadi kesalahan pada database (rollback)";
	// 				}else {
	// 				     $this->db->trans_commit();
	// 				     $response["status"] = 'success';
	// 				     $response["message"] = 'data paket berhasil diubah';
	// 				}

	//  				$response['status']  = 'success';
	// 				$response['message'] = 'data paket berhasil diubah';
	// 			}

	// 	}else{

	// 		$response["message"] = (string) json_encode($this->form_validation->error_array());

	// 	}

	// 	return $this->output->set_content_type('application/json')
	// 	            ->set_status_header(200)
	// 	            ->set_output(json_encode($response));
	// }

	// //tambah item dalam package
	// public function addItem()
	// {
	// 	$obj = new StdClass();
	// 	$response = array("status" => 'failed', "data" => $obj, "message" => '');

	// 	$_POST = json_decode(file_get_contents("php://input"), true);
	// 	$data = $this->input->post();

	// 	$this->form_validation->set_rules('token', 'token', 'required');

	// 	if ($this->form_validation->run() == TRUE) {

	// 			$session = $this->checkToken($data['token']);

	// 			if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

	// 				$response["message"] = $session['tokenStatus'];

	// 				return $this->output->set_content_type('application/json')
	// 		            ->set_status_header(200)
	// 		            ->set_output(json_encode($response));

	// 			}

	// 			$wad = $this->db->select(['wilayah_id'])
	// 							->from('wilayah_address_details')
	// 							->where(['wilayah_detail_id' => $session['data']->id, 'is_primary' => 1])
	// 							->limit(1)
	// 							->get()
	// 							->row();

	// 			if($wad == null){

	// 				$response['message'] = 'wilayah_address_details_id tidak ditemukan';
	// 				return $this->output->set_content_type('application/json')
	// 					            ->set_status_header(200)
	// 					            ->set_output(json_encode($response));
	// 			}else{

	// 				$this->db->trans_begin();
	// 		    	try {
	// 					$item = array(
	// 						'paket_id' => $data['paket_id'],
	// 						'grade_id' => $data['grade_id'],
	// 						'product_id' => $data['product_id'],
	// 						'qty' => $data['qty']
	// 					)

	// 					$this->db->insert('paket_details', $item);
	// 		    	} catch (Exception $e) {
	// 		    		 $this->db->trans_rollback();
	// 					 $response["message"] = "terjadi kesalahan pada database (rollback)";
	// 		    	}

	// 		    	if ($this->db->trans_status() === FALSE){
	// 					 $this->db->trans_rollback();
	// 					 $response["message"] = "terjadi kesalahan pada database (rollback)";
	// 				}else {
	// 				     $this->db->trans_commit();
	// 				     $response["status"] = 'success';
	// 				     $response["message"] = 'item berhasil ditambahkan';
	// 				}

	//  				$response['status']  = 'success';
	// 				$response['message'] = 'item berhasil ditambahkan';
	// 			}

	// 	}else{

	// 		$response["message"] = (string) json_encode($this->form_validation->error_array());

	// 	}

	// 	return $this->output->set_content_type('application/json')
	// 	            ->set_status_header(200)
	// 	            ->set_output(json_encode($response));
	// }

	//ubah qty item
	public function updateQtyItem()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);

				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$wad = $this->db->select(['wilayah_id'])
								->from('wilayah_address_details')
								->where(['wilayah_detail_id' => $session['data']->id, 'is_primary' => 1])
								->limit(1)
								->get()
								->row();

				if($wad == null){

					$response['message'] = 'wilayah_address_details_id tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

					$this->db->trans_begin();
			    	try {
						$item_id = $data['item_id'];
						$item_qty = $data['item_qty'];

						$this->db->set('qty', $item_qty);
						$this->db->where('id', $item_id);
						$this->db->update('paket_details');

			    	} catch (Exception $e) {
			    		 $this->db->trans_rollback();
						 $response["message"] = "terjadi kesalahan pada database (rollback)";
			    	}

			    	if ($this->db->trans_status() === FALSE){
						 $this->db->trans_rollback();
						 $response["message"] = "terjadi kesalahan pada database (rollback)";
					}else {
					     $this->db->trans_commit();
					     $response["status"] = 'success';
					     $response["message"] = 'data paket berhasil diubah';
					}

	 				$response['status']  = 'success';
					$response['message'] = 'data paket berhasil diubah';
				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	//delete item pada package
	public function deleteItem()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);

				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$wad = $this->db->select(['wilayah_id'])
								->from('wilayah_address_details')
								->where(['wilayah_detail_id' => $session['data']->id, 'is_primary' => 1])
								->limit(1)
								->get()
								->row();

				if($wad == null){

					$response['message'] = 'wilayah_address_details_id tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

					$this->db->trans_begin();
			    	try {
						$item_id = $data['item_id'];
						$this->db->where('id', $item_id);
						$this->db->delete('paket_details');
			    	} catch (Exception $e) {
			    		 $this->db->trans_rollback();
						 $response["message"] = "terjadi kesalahan pada database (rollback)";
			    	}

			    	if ($this->db->trans_status() === FALSE){
						 $this->db->trans_rollback();
						 $response["message"] = "terjadi kesalahan pada database (rollback)";
					}else {
					     $this->db->trans_commit();
					     $response["status"] = 'success';
					     $response["message"] = 'item berhasil dihapus';
					}

	 				$response['status']  = 'success';
					$response['message'] = 'item berhasil dihapus

					';
				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	//delete semua item pada package
	public function deleteItemALl()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);

				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$wad = $this->db->select(['wilayah_id'])
								->from('wilayah_address_details')
								->where(['wilayah_detail_id' => $session['data']->id, 'is_primary' => 1])
								->limit(1)
								->get()
								->row();

				if($wad == null){

					$response['message'] = 'wilayah_address_details_id tidak ditemukan';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

					$this->db->trans_begin();
			    	try {
						$paket_id = $data['paket_id'];
						$this->db->where('paket_id', $paket_id);
						$this->db->delete('paket_details');
			    	} catch (Exception $e) {
			    		 $this->db->trans_rollback();
						 $response["message"] = "terjadi kesalahan pada database (rollback)";
			    	}

			    	if ($this->db->trans_status() === FALSE){
						 $this->db->trans_rollback();
						 $response["message"] = "terjadi kesalahan pada database (rollback)";
					}else {
					     $this->db->trans_commit();
					     $response["status"] = 'success';
					     $response["message"] = 'item berhasil dihapus';
					}

	 				$response['status']  = 'success';
					$response['message'] = 'item berhasil dihapus

					';
				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function listPackage()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);

				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$content = "select
								 d.paket_id,
								 d.paket_picture,
				                 d.paket,
								 c.title,
								 f.title as satuan,
								 c.berat_kemasan,
								 e.simbol,
								 e.title as type,
								 sum(d.qty) as total_qty,
								 sum(d.unitprice_ts * d.qty) as total_harga,
								 d.app_code,
								 d.created_by
							from (
									select g.status, g.created_by, g.app_code, g.picture as paket_picture, g.title as paket, a.min_qty, b.id, b.paket_id, b.grade_id, b.product_id, b.qty, a.unitprice_ts
								  		from  (SELECT * FROM `hargas` WHERE `status` = 1 and wilayah_id=".$session['data']->wilayah_id." ) a
												inner join paket_details b on
												a.product_id=b.product_id and a.grade=b.grade_id
												and a.min_qty <= b.qty
												inner join pakets g on g.id=b.paket_id ) d,

												( select a.product_id,max(a.min_qty) jml from (SELECT * FROM `hargas` WHERE `status` = 1 and wilayah_id=".$session['data']->wilayah_id." ) a
												inner join paket_details b on a.product_id=b.product_id and a.grade=b.grade_id
												and a.min_qty <= b.qty
												inner join pakets h on h.id=b.paket_id
												group by a.product_id
												 ) y

						inner join products c on c.id=y.product_id
                        inner join product_wilattrs on product_wilattrs.product_id = c.id
                            and product_wilattrs.publish = 1
                            and product_wilattrs.wilayah_id = ".$session['data']->wilayah_id."
                            and product_wilattrs.is_ms = 1
						left join kemasans e on e.id=c.kemasan_id
						left join satuans f on f.id=c.satuan_id

						where d.product_id=y.product_id and d.min_qty=y.jml and (d.created_by = ".$session['data']->id." OR app_code = 0) and d.status = 1

						group by d.paket
					";


				$query = $this->db->query($content);

				$response['base_url']         = $this->base_url_product;
				$response['base_url_package'] = $this->base_url_package;

				if($query == null){

					$response['message'] = 'paket tidak ada';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

	 				$response['status']  = 'success';
					$response['message'] = 'list paket';
					$response['data']    = $query->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function listPackageAdmin()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);

				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$content = "select
								 d.paket_id,
								 d.paket_picture,
				                 d.paket,
								 c.title,
								 f.title as satuan,
								 c.berat_kemasan,
								 e.simbol,
								 e.title as type,
								 sum(d.qty) as total_qty,
								 sum(d.unitprice_ts * d.qty) as total_harga,
								 IFNULL(d.app_code, 0) as app_code,
								 d.created_by
							from (
									select g.status, g.created_by, g.app_code, g.picture as paket_picture, g.title as paket, a.min_qty, b.id, b.paket_id, b.grade_id, b.product_id, b.qty, a.unitprice_ts
								  		from  (SELECT * FROM `hargas` WHERE `status` = 1 and wilayah_id=".$session['data']->wilayah_id." ) a
												inner join paket_details b on
												a.product_id=b.product_id and a.grade=b.grade_id
												and a.min_qty <= b.qty
												inner join pakets g on g.id=b.paket_id ) d,

												( select a.product_id,max(a.min_qty) jml from (SELECT * FROM `hargas` WHERE `status` = 1 and wilayah_id=".$session['data']->wilayah_id." ) a
												inner join paket_details b on a.product_id=b.product_id and a.grade=b.grade_id
												and a.min_qty <= b.qty
												inner join pakets h on h.id=b.paket_id
												group by a.product_id
												 ) y

						inner join products c on c.id=y.product_id
                        inner join product_wilattrs on product_wilattrs.product_id = c.id
                            and product_wilattrs.publish = 1
                            and product_wilattrs.wilayah_id = ".$session['data']->wilayah_id."
                            and product_wilattrs.is_ms = 1
						left join kemasans e on e.id=c.kemasan_id
						left join satuans f on f.id=c.satuan_id

						where d.product_id=y.product_id and d.min_qty=y.jml and d.app_code = 1 and d.status = 1

						group by d.paket
					";


				$query = $this->db->query($content);

				$response['base_url']         = $this->base_url_product;
				$response['base_url_package'] = $this->base_url_package;

				if($query == null){

					$response['message'] = 'paket tidak ada';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

	 				$response['status']  = 'success';
					$response['message'] = 'list paket';
					$response['data']    = $query->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function listPackageDetail()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('paket_id', 'paket_id', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);

				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$wilayah_address_details = $this->db->select('wilayah_id')
									   ->from('wilayah_address_details')
									   ->where(['wilayah_detail_id' => $session['data']->id, 'is_primary' => 1])
									   ->get()
									   ->row();

				$content = "select d.*,
								   c.title,
								   f.title as satuan,
								   c.berat_kemasan,
								   e.simbol,
								   i.title as img,
								   e.title as type,
								   (qty * d.unitprice_ts) as subtotal
							from (
										select i.title as grade_title, a.id as harga_id, a.min_qty, b.id, b.paket_id, b.grade_id as grade, b.product_id, b.qty, a.unitprice_ts
									  		from (SELECT * FROM `hargas` WHERE `status` = 1 and wilayah_id=".$session['data']->wilayah_id." ) a
									inner join paket_details b on
									a.product_id=b.product_id and a.grade=b.grade_id
									and a.min_qty <= b.qty
									inner join harga_grades i on i.id=a.grade) d,

									(select a.product_id,max(a.min_qty) jml from (SELECT * FROM `hargas` WHERE `status` = 1 and wilayah_id=".$session['data']->wilayah_id." ) a

						inner join paket_details b on a.product_id=b.product_id and a.grade=b.grade_id
									and a.min_qty <= b.qty
									group by a.product_id) y

						inner join products c on c.id=y.product_id
                        inner join product_wilattrs on product_wilattrs.product_id = c.id
                            and product_wilattrs.publish = 1
                            and product_wilattrs.wilayah_id = ".$session['data']->wilayah_id."
                            and product_wilattrs.is_ms = 1
						left join kemasans e on e.id=c.kemasan_id
						left join satuans f on f.id=c.satuan_id
						left join productpictures i on i.product_id=c.id  and
							 i.status = 1

						where d.product_id=y.product_id and
							 d.min_qty=y.jml and
							 d.paket_id=".$data['paket_id']."
						order by d.id";


				$query = $this->db->query($content);
				$response['base_url']         = $this->base_url_product;
				$response['base_url_package'] = $this->base_url_package;

				if($query == null){

					$response['message'] = 'paket tidak ada';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

	 				$response['status']  = 'success';
					$response['message'] = 'list paket';
					$response['data']    = $query->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

}//end class
