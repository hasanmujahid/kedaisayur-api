<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Pool extends MY_Controller {

	function __construct()
	{
	   parent::__construct();  
	   date_default_timezone_set('Asia/Jakarta');
	}

	public function listpool()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);

				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$query = $this->db->select(['wilayah_address_details.wilayah_pool_id',
											'wilayah_pools.title',
										    'wilayah_pools.address',
											'wilayah_pools.phone',
										    'wilayah_pools.g_route',
										    'wilayah_pools.adm_area_level_1',
										    'wilayah_pools.adm_area_level_2',
										    'wilayah_pools.adm_area_level_3',
											'wilayah_pools.adm_area_level_4'])
										->from('wilayah_address_details')
										->join('wilayah_pools', 'wilayah_pools.id = wilayah_address_details.wilayah_pool_id', 'left')
										->where(['wilayah_pools.status' => 1, 'wilayah_address_details.wilayah_detail_id' => $session['data']->id])
										->group_by('wilayah_address_details.wilayah_pool_id', 'asc')
										->get();

				if($query == null){

					$response['message'] = 'paket tidak ada';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

	 				$response['status']  = 'success';
					$response['message'] = 'list paket';
					$response['data']    = $query->result();

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function detail()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('wilayah_pool_id', 'wilayah_pool_id', 'required');

		if ($this->form_validation->run() == TRUE) {

				$session = $this->checkToken($data['token']);

				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

				}

				$query = $this->db->select(['wilayah_pools.id as wilayah_pool_id',
											'wilayah_pools.title',
										    'wilayah_pools.address',
											'wilayah_pools.phone',
										    'wilayah_pools.g_route',
										    'wilayah_pools.pic',
										    'wilayah_pools.lat',
										    'wilayah_pools.lng',
										    'wilayah_pools.phone',
										    'wilayah_pools.adm_area_level_1',
										    'wilayah_pools.adm_area_level_2',
										    'wilayah_pools.adm_area_level_3',
											'wilayah_pools.adm_area_level_4'])
										->from('wilayah_pools')
										->where(['wilayah_pools.status' => 1, 'wilayah_pools.id' => $data['wilayah_pool_id']])
										->get()
										->row();

				if($query == null){

					$response['message'] = 'wilayah pool tidak ada';
					return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
				}else{

	 				$response['status']  = 'success';
					$response['message'] = 'detail paket';
					$response['data']    = $query;

				}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

}//end class
