<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Product extends MY_Controller {

	function __construct()
	{
	   parent::__construct();
	   date_default_timezone_set('Asia/Jakarta');
	}

	public function listProduct()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('wilayah_id', 'wilayah_id', 'required');
		$this->form_validation->set_rules('category', 'category', 'required');

		if ($this->form_validation->run() == TRUE) {

			$session = $this->checkToken($data['token']);

			if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

				$response["message"] = $session['tokenStatus'];

				return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

			}

			//$data['wilayah_id'] adalah wilayah_address_detail
			$get_wilayah_id = $this->db->select('*')
									   ->from('wilayah_address_details')
									   ->where(['id' => $data['wilayah_id']])
									   ->get()
									   ->row();

			if($get_wilayah_id == null){

				$response["message"] = 'wilayah tidak ditemukan';
				return $this->output->set_content_type('application/json')
				            ->set_status_header(200)
				            ->set_output(json_encode($response));


			}

			$response['base_url'] = $this->base_url_product;

			// $query =   $this->db->select(['products.id as product_id',
			// 							  'products.title',
			// 							  'IFNULL(hargas.unitprice_ts, 0) as unitprice_ts',
			// 							  'productpictures.title as img',
			// 							  'kemasans.title as type',
			// 							  'kemasans.simbol',
			// 							  'products.berat_kemasan',
			// 							  'satuans.title as satuan',
			// 							  'hargas.wilayah_id',
			// 							  'wilayahs.title as wilayah',
			// 							  'hargas.min_qty',
			// 							  'hargas.grade',
			// 							  'harga_grades.title as grade_title',
			// 							  'categories.name as category'
			// 							])
			// 					->from('products')
			// 					->join('hargas', 'hargas.product_id = products.id', 'left')
			// 					->join('harga_grades', 'harga_grades.id = hargas.grade', 'inner')
			// 					->join('wilayahs', 'wilayahs.id = hargas.wilayah_id', 'inner')
			// 					->join('productpictures', 'productpictures.product_id = products.id', 'left')
			// 					->join('kemasans', 'kemasans.id = products.kemasan_id', 'left')
			// 					->join('satuans', 'satuans.id = products.satuan_id', 'left')
			// 					->join('products_categories', 'products_categories.product_id = products.id', 'left')
			// 					->join('categories', 'categories.id = products_categories.category_id', 'left')
			// 					->where(['hargas.min_qty' => 1, 'hargas.wilayah_id' => $get_wilayah_id->wilayah_id, 'hargas.status' => 1]);

			$query =   $this->db->select(['products.id as product_id',
										  'products.title',
										  'IFNULL(hargas.unitprice_ts, 0) as unitprice_ts',
										  'productpictures.title as img',
										  'kemasans.title as type',
										  'kemasans.simbol',
										  'products.berat_kemasan',
										  'satuans.title as satuan',
										  'hargas.wilayah_id',
										  'wilayahs.title as wilayah',
										  'IFNULL(hargas.min_qty, 1) as min_qty',
										  'hargas.grade',
										  'harga_grades.title as grade_title',
										  'categories.name as category',
										  'products.status as product_status',
										  'hargas.status as hargas_status',
										  'products.publish'
										])
								->from('products')
								->join('hargas', 'hargas.product_id = products.id and hargas.min_qty = 1 and hargas.status =1 and  hargas.wilayah_id = '.$get_wilayah_id->wilayah_id, 'left')
								->join('product_wilattrs', 'product_wilattrs.product_id = products.id and product_wilattrs.publish = 1 and product_wilattrs.wilayah_id = '.$get_wilayah_id->wilayah_id, 'inner')
								->join('harga_grades', 'harga_grades.id = hargas.grade', 'left')
								->join('wilayahs', 'wilayahs.id = '.$get_wilayah_id->wilayah_id, 'left')
								->join('productpictures', 'productpictures.product_id = products.id', 'left')
								->join('kemasans', 'kemasans.id = products.kemasan_id', 'left')
								->join('satuans', 'satuans.id = products.satuan_id', 'left')
								->join('products_categories', 'products_categories.product_id = products.id', 'left')
								->join('categories', 'categories.id = products_categories.category_id', 'left');

			if($data['category'] != "all"){

				$query = $query->where('products_categories.category_id', $data['category']);

			}

			$query = $query->where(['productpictures.status' => 1,
									'products.publish' => 1,
									'products.status' => 1,
									'hargas.status' => 1,
									'unitprice_ts !=' => 0,
									'is_ms' => 1]);

			$query = $query->group_by('products.id')->order_by('products.title', 'asc')->limit(300)->get()->result();

			$response['status'] = 'success';
			$response['data']   = $query == null ? $obj : $query;

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function search()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('wilayah_id', 'wilayah_id', 'required');
		$this->form_validation->set_rules('search', 'search', 'required');

		if ($this->form_validation->run() == TRUE) {

			$session = $this->checkToken($data['token']);

			if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

				$response["message"] = $session['tokenStatus'];

				return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

			}

			$get_wilayah_id = $this->db->select('*')
									   ->from('wilayah_address_details')
									   ->where(['id' => $data['wilayah_id']])
									   ->get()
									   ->row();

			if($get_wilayah_id == null){

				$response["message"] = 'wilayah tidak ditemukan';
				return $this->output->set_content_type('application/json')
				            ->set_status_header(200)
				            ->set_output(json_encode($response));


			}

			$response['base_url'] = $this->base_url_product;

			// $query =   $this->db->select(['products.id as product_id',
			// 							  'products.title',
			// 							  'IFNULL(hargas.unitprice_ts, 0) as unitprice_ts',
			// 							  'productpictures.title as img',
			// 							  'kemasans.title as type',
			// 							  'kemasans.simbol',
			// 							  'products.berat_kemasan',
			// 							  'satuans.title as satuan',
			// 							  'hargas.wilayah_id',
			// 							  'wilayahs.title as wilayah',
			// 							  'hargas.min_qty',
			// 							  'hargas.grade',
			// 							  'harga_grades.title as grade_title'
			// 							])
			// 					->from('products')
			// 					->join('hargas', 'hargas.product_id = products.id', 'left')
			// 					->join('harga_grades', 'harga_grades.id = hargas.grade', 'inner')
			// 					->join('wilayahs', 'wilayahs.id = hargas.wilayah_id', 'inner')
			// 					->join('productpictures', 'productpictures.product_id = products.id', 'left')
			// 					->join('kemasans', 'kemasans.id = products.kemasan_id', 'left')
			// 					->join('satuans', 'satuans.id = products.satuan_id', 'left')
			// 					->where(['hargas.min_qty' => 1, 'hargas.wilayah_id' => $get_wilayah_id->wilayah_id, 'hargas.status' => 1])

			$query =   $this->db->select(['products.id as product_id',
										  'products.title',
										  'IFNULL(hargas.unitprice_ts, 0) as unitprice_ts',
										  'productpictures.title as img',
										  'kemasans.title as type',
										  'kemasans.simbol',
										  'products.berat_kemasan',
										  'satuans.title as satuan',
										  'hargas.wilayah_id',
										  'wilayahs.title as wilayah',
										  'IFNULL(hargas.min_qty, 1) as min_qty',
										  'hargas.grade',
										  'harga_grades.title as grade_title',
										  'categories.name as category',
										  'products.status as product_status',
										  'hargas.status as hargas_status',
										  'products.publish'
										])
								->from('products')
								->join('hargas', 'hargas.product_id = products.id and hargas.min_qty = 1 and hargas.status =1 and  hargas.wilayah_id = '.$get_wilayah_id->wilayah_id, 'left')
								->join('product_wilattrs', 'product_wilattrs.product_id = products.id and product_wilattrs.publish = 1 and product_wilattrs.wilayah_id = '.$get_wilayah_id->wilayah_id, 'inner')
								->join('harga_grades', 'harga_grades.id = hargas.grade', 'left')
								->join('wilayahs', 'wilayahs.id = '.$get_wilayah_id->wilayah_id, 'left')
								->join('productpictures', 'productpictures.product_id = products.id', 'left')
								->join('kemasans', 'kemasans.id = products.kemasan_id', 'left')
								->join('satuans', 'satuans.id = products.satuan_id', 'left')
								->join('products_categories', 'products_categories.product_id = products.id', 'left')
								->join('categories', 'categories.id = products_categories.category_id', 'left')
								->like(['products.title' => $data['search']])
								->where(['productpictures.status' => 1,
										 'products.publish' => 1,
										 'hargas.status' => 1,
										 'products.status' => 1,
										 'unitprice_ts !=' => 0,
	 									'is_ms' => 1])
								->group_by('products.id')
								->get();

			$response['status'] = 'success';
			$response['data']   = $query->result();

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function detail()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('product_id', 'product_id', 'required');
		$this->form_validation->set_rules('wilayah_id', 'wilayah_id', 'required');
		$this->form_validation->set_rules('grade', 'grade', 'required');
		$this->form_validation->set_rules('min_qty', 'min_qty', 'required');

		if ($this->form_validation->run() == TRUE) {

			$session = $this->checkToken($data['token']);

			if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

				$response["message"] = $session['tokenStatus'];

				return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

			}

			$response['base_url'] = $this->base_url_product;

			$null = null;

			$get_wilayah_id = $this->db->select('*')
									   ->from('wilayah_address_details')
									   ->where(['id' => $data['wilayah_id']])
									   ->get()
									   ->row();

			if($get_wilayah_id == null){

				$response["message"] = 'wilayah tidak ditemukan';
				return $this->output->set_content_type('application/json')
				            ->set_status_header(200)
				            ->set_output(json_encode($response));


			}

			$query =   $this->db->select(['products.id as product_id',
										  'hargas.id as hargas_id',
										  'products.title',
										  'products.detail as description',
										  'productpictures.title as img',
										  'kemasans.title as type',
										  'kemasans.simbol',
										  'products.berat_kemasan',
										  'satuans.title as satuan',
										  'hargas.wilayah_id',
										  'wilayahs.title as wilayah',
										  'hargas.min_qty',
										  'hargas.grade',
										  'harga_grades.title as grade_title',
										  'IFNULL(hargas.unitprice_ts, 0) as unitprice_ts',
										  '(select delivery_time from wilayah_pools where id = "'.$get_wilayah_id->wilayah_pool_id.'") as delivery_time',
										  'products.viewer'
										])
								->from('products')
								->join('hargas', 'hargas.product_id = products.id and hargas.min_qty <= '.$data['min_qty'].' and hargas.status =1 and  hargas.wilayah_id = '.$get_wilayah_id->wilayah_id.' and hargas.grade = '.$data['grade'], 'left')
								->join('harga_grades', 'harga_grades.id = hargas.grade', 'left')
								->join('wilayahs', 'wilayahs.id = '.$get_wilayah_id->wilayah_id, 'left')
								->join('productpictures', 'productpictures.product_id = products.id', 'left')
								->join('kemasans', 'kemasans.id = products.kemasan_id', 'left')
								->join('satuans', 'satuans.id = products.satuan_id', 'left')
								->where(['products.id' => $data['product_id'], 'productpictures.status' => 1])
								->order_by('hargas.min_qty', 'desc')
								->limit(1)
								->get();

			$query_grosir =   $this->db->select(['products.id as product_id',
										  'hargas.id as harga_id',
										  'hargas.min_qty',
										  'IFNULL(hargas.unitprice_ts, 0) as unitprice_ts',
										  'hargas.grade',
										  'products.summary_in_weight',
										  'kemasans.simbol',
										  'products.berat_kemasan',
										  'satuans.title as satuan'
										])
								->from('hargas')
								->join('products', 'products.id = hargas.product_id', 'left')
								->join('kemasans', 'kemasans.id = products.kemasan_id', 'left')
								->join('satuans', 'satuans.id = products.satuan_id', 'left')
								->where(['products.id' => $data['product_id'],
										 'hargas.wilayah_id' => $get_wilayah_id->wilayah_id,
										 'hargas.grade' => $data['grade'],
										 'hargas.status' => 1])
								->distinct('hargas.min_qty')
								->order_by('min_qty', 'DESC')
								->get();

			$query_grade =   $this->db->select(['hargas.grade',
										  'harga_grades.title as grade_title'
										])
								->from('hargas')
								->join('harga_grades', 'harga_grades.id = hargas.grade', 'inner')
								->where(['hargas.product_id' => $data['product_id'], 'hargas.wilayah_id' => $get_wilayah_id->wilayah_id])
								->distinct('hargas.grade')
								->group_by('hargas.grade')
								->get();

			$i=0;
			$grosir = [];
			foreach ($query_grosir->result() as $key => $value) {

					if($key == 0){
						$grosir[] =  array(
										   'harga_id' => $value->harga_id,
										   'min_qty' => $value->min_qty,
										   'max_qty' => 100,
									       'unitprice_ts' => $value->unitprice_ts,
									   	   'grade' => $value->grade,
									   	   'simbol' => $value->simbol,
									   	   'berat_kemasan' => $value->berat_kemasan,
									   	   'satuan' => $value->satuan,
									   	   'summary_in_weight' => $value->summary_in_weight);
					}else {
						$grosir[] =  array(
										   'harga_id' => $value->harga_id,
										   'min_qty' => $value->min_qty,
										   'max_qty' => $i-1,
									       'unitprice_ts' => $value->unitprice_ts,
									   	   'grade' => $value->grade,
									   	   'simbol' => $value->simbol,
									   	   'berat_kemasan' => $value->berat_kemasan,
									   	   'satuan' => $value->satuan,
									   	   'summary_in_weight' => $value->summary_in_weight);
					}

				   $i = $value->min_qty;
			}

			$response['status'] = 'success';
			$response['data']   = $query->row();
			$response['grosir'] = $grosir;
			$response['grade'] = $query_grade->result();

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}


	public function listToko()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

			$session = $this->checkToken($data['token']);

			if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

				$response["message"] = $session['tokenStatus'];

				return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

			}

			$query =   $this->db->select(['wilayah_address_details.id as wilayah_address_detail_id',
										  'wilayah_address_details.wilayah_id',
										  'wilayah_address_details.wilayah_detail_id',
										  'wilayah_address_details.wilayah_pool_id',
										  'wilayah_address_details.title',
										  'wilayah_address_details.is_receiveorder',
										  'wilayah_address_details.is_primary',
										  'wilayah_address_details.address',
										  'wilayah_address_details.g_route',
										  'wilayah_pools.title as pool',
										  'wilayah_pools.address as pool_address',
										  'wilayah_pools.pic as pool_pic',
										  'IF(wilayah_address_details.status_detail_aktivasi=2,"Telah dikonfirmasi",
										  	 IF(wilayah_address_details.status_detail_aktivasi=1,"Akun Sedang diproses",  IF(wilayah_address_details.status_detail_aktivasi=0,"Akun baru ditambahkan menunggu diproses", "Lapak telah di reject"))) as status',
										  'wilayah_address_details.status_detail_aktivasi'
										])
								->from('wilayah_address_details')
								->join('wilayah_pools', 'wilayah_pools.id = wilayah_address_details.wilayah_pool_id')
								->where(['wilayah_address_details.wilayah_detail_id' => $session['data']->id])
								->get();

			$response['status'] = 'success';
			$response['data']   = $query->result();

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function listGrade()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

			$session = $this->checkToken($data['token']);

			if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

				$response["message"] = $session['tokenStatus'];

				return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

			}

			$query =   $this->db->select(['id', 'title'])
								->from('harga_grades')
								->where(['publish' => 1])
								->get();

			$response['status'] = 'success';
			$response['data']   = $query->result();

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

    }

    public function categories()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

			$session = $this->checkToken($data['token']);

			if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

				$response["message"] = $session['tokenStatus'];

				return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

			}

			$response['base_url'] = $this->base_url_product;

			$query =   $this->db->select(['categories.id',
										  'categories.name',
										  'categories.picture',
										  'categories.level'
										])
								->from('categories')
								->where(['categories.status' => 1]);

			$query = $query->get()->result();

			$response['status'] = 'success';
			$response['data']   = $query == null ? $obj : $query;

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function updateStatusToko()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');

		if ($this->form_validation->run() == TRUE) {

			$session = $this->checkToken($data['token']);

			if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

				$response["message"] = $session['tokenStatus'];

				return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));

			}

			$data_upd = array(
				'is_receiveorder' => $data['is_receiveorder']
			);
			$query = $this->db->where(['id' => $data['wilayah_address_detail_id']])->update('wilayah_address_details', $data_upd);

			if($this->db->affected_rows() == false) {
				$response['status'] = 'failed';
				$response['message']   = 'data tidak berubah';
			}
			else {
				if($data['is_receiveorder'] == 1) {
					$response['status'] = 'success';
					$response['message']   = 'berhasil mengaktifkan toko';
				}
				else {
					$response['status'] = 'success';
					$response['message'] = 'berhasil menonaktifkan toko';
				}

			}


		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

}//end class
