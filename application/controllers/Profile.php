<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Profile extends MY_Controller {

	function __construct()
	{
	   parent::__construct();  
	   date_default_timezone_set('Asia/Jakarta');
	}

	public function addToko()
	{
		$obj = new StdClass();
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('token', 'token', 'required');
		$this->form_validation->set_rules('latitude_toko', 'latitude_toko', 'required');
		$this->form_validation->set_rules('longitude_toko', 'longitude_toko', 'required');
		$this->form_validation->set_rules('address_toko', 'address_toko', 'required');
		$this->form_validation->set_rules('g_route_toko', 'g_route_toko', 'required');
		$this->form_validation->set_rules('title', 'title', 'required');

		if ($this->form_validation->run() == TRUE) {


			$session = $this->checkToken($data['token']);
				if($session['tokenStatus'] == 'Invalid token' || $session['tokenStatus'] == 'Token expired'){

					$response["message"] = $session['tokenStatus'];

					return $this->output->set_content_type('application/json')
			            ->set_status_header(200)
			            ->set_output(json_encode($response));

			}

			
			try {
					$adm_area_level_1 ='';
					$adm_area_level_2 ='';
					$adm_area_level_3 ='';
					$adm_area_level_4 ='';
					$address = $this->getAddress($data['latitude_toko'], $data['longitude_toko']);

		    		if(count($address) > 1){

						for($i=0; $i<count($address); $i++){

							if($address[$i]['types'][0] == 'administrative_area_level_1'){
								$adm_area_level_1 = $address[$i]['long_name'];
							}
							if($address[$i]['types'][0] == 'administrative_area_level_2'){
								$adm_area_level_2 = $address[$i]['long_name'];
							}
							if($address[$i]['types'][0] == 'administrative_area_level_3'){
								$adm_area_level_3 = $address[$i]['long_name'];
							}
							if($address[$i]['types'][0] == 'administrative_area_level_4'){
								$adm_area_level_4 = $address[$i]['long_name'];
							}
							
						}
					}
		    		
	    	} catch (Exception $e) {
	    		 
				 $response["message"] = "terjadi kesalahan ketika menterjemahkan alamat";
				 return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
	    	}
				

		    $this->db->trans_begin();
	    	try {

    			$wilayah_address_details['wilayah_detail_id'] = $session['data']->id;
				$wilayah_address_details['phone'] = $session['data']->phone;
				$wilayah_address_details['g_route'] = $data['g_route_toko']; 
				$wilayah_address_details['title'] = $data['title']; 
				$wilayah_address_details['adm_area_level_1'] = $adm_area_level_1; 
				$wilayah_address_details['adm_area_level_2'] = $adm_area_level_2;
				$wilayah_address_details['adm_area_level_3'] = $adm_area_level_3;
				$wilayah_address_details['adm_area_level_4'] = $adm_area_level_4;
				$wilayah_address_details['lat'] = $data['latitude_toko'];
				$wilayah_address_details['lng'] = $data['longitude_toko'];
				$wilayah_address_details['address'] = $data['address_toko'];
				$wilayah_address_details['created_by'] = $session['data']->id;
				$wilayah_address_details['created'] = date('Y-m-d H:i:s', NOW());
				$this->db->insert('wilayah_address_details', $wilayah_address_details);

			} catch (Exception $e) {
	    		 $this->db->trans_rollback();
				 $response["message"] = "terjadi kesalahan pada database (rollback)";
	    	}

	    	if ($this->db->trans_status() === FALSE){
				    $this->db->trans_rollback();
				    $response["message"] = "terjadi kesalahan pada database (rollback)";
			}else {
			        $this->db->trans_commit();
			        $response["status"] = 'success';
			        $response["message"] = 'data berhasil disimpan';
			}

		}else{

			$response["message"] = (string) json_encode($this->form_validation->error_array());

		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

}
