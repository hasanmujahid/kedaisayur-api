<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Registration extends MY_Controller {

	function __construct()
	{
	   parent::__construct();  
	   date_default_timezone_set('Asia/Jakarta');
	}

	public function getOtpTukangSayur()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj,"message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('phone', 'phone', 'required');
		$this->form_validation->set_rules('latitude_ts', 'latitude_ts', 'required');
		$this->form_validation->set_rules('longitude_ts', 'longitude_ts', 'required');
		$this->form_validation->set_rules('address_ts', 'address_ts', 'required');
		$this->form_validation->set_rules('g_route_ts', 'g_route_ts', 'required');
		$this->form_validation->set_rules('latitude_toko', 'latitude_toko', 'required');
		$this->form_validation->set_rules('longitude_toko', 'longitude_toko', 'required');
		$this->form_validation->set_rules('address_toko', 'address_toko', 'required');
		$this->form_validation->set_rules('g_route_toko', 'g_route_toko', 'required');
		$this->form_validation->set_rules('imei', 'imei', 'required');
		$this->form_validation->set_rules('iccid', 'iccid', 'required');
		$this->form_validation->set_rules('foto', 'foto', 'required');
		$this->form_validation->set_rules('device_os', 'device_os', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			$this->db->where('phone', $data['phone']);
			$this->db->from('wilayah_details');
		    $check_no_phone = $this->db->count_all_results();

		    if($check_no_phone == 0){

		    	try {
						$adm_area_level_1 ='';
						$adm_area_level_2 ='';
						$adm_area_level_3 ='';
						$adm_area_level_4 ='';
						$address = $this->getAddress($data['latitude_ts'], $data['longitude_ts']);

			    		if(count($address) > 1){

							for($i=0; $i<count($address); $i++){

								if($address[$i]['types'][0] == 'administrative_area_level_1'){
									$adm_area_level_1 = $address[$i]['long_name'];
								}
								if($address[$i]['types'][0] == 'administrative_area_level_2'){
									$adm_area_level_2 = $address[$i]['long_name'];
								}
								if($address[$i]['types'][0] == 'administrative_area_level_3'){
									$adm_area_level_3 = $address[$i]['long_name'];
								}
								if($address[$i]['types'][0] == 'administrative_area_level_4'){
									$adm_area_level_4 = $address[$i]['long_name'];
								}
								
							}
						}

						$adm_area_level_1_toko ='';
						$adm_area_level_2_toko ='';
						$adm_area_level_3_toko ='';
						$adm_area_level_4_toko ='';
						$address_toko = $this->getAddress($data['latitude_toko'], $data['longitude_toko']);

			    		if(count($address) > 1){

							for($i=0; $i<count($address); $i++){

								if($address[$i]['types'][0] == 'administrative_area_level_1'){
									$adm_area_level_1_toko = $address_toko[$i]['long_name'];
								}
								if($address[$i]['types'][0] == 'administrative_area_level_2'){
									$adm_area_level_2_toko = $address_toko[$i]['long_name'];
								}
								if($address[$i]['types'][0] == 'administrative_area_level_3'){
									$adm_area_level_3_toko = $address_toko[$i]['long_name'];
								}
								if($address[$i]['types'][0] == 'administrative_area_level_4'){
									$adm_area_level_4_toko = $address_toko[$i]['long_name'];
								}
								
							}
						}
			    		
			    	} catch (Exception $e) {
			    		 
						 $response["message"] = "terjadi kesalahan ketika menterjemahkan alamat";
						 return $this->output->set_content_type('application/json')
								            ->set_status_header(200)
								            ->set_output(json_encode($response));
			    	}

			    //convert image
			    $image = $this->saveFoto($data['foto'], $data['phone']);

		    	$this->db->trans_begin();
		    	try {

		    		$phone62 = '62'.substr($data['phone'], 1);
					$subject = 'Kode Aktivasi Kedaisayur';
					$activation_code = rand(1111, 9999);
					
					$keterangan = "Jika anda tidak menerima kode yang kami kirimkan dalam waktu 1 menit Harap Menggunakan No telpon lain atau hubungi kami.";
		    		
					$date = new DateTime("now");
 					$curr_date = $date->format('Y-m-d');
		    		
		    		$this->db->where('phone_number', $phone62);
		    		$this->db->where('Date(created)',  $curr_date);
					$this->db->from('sms_queues');
				    $cek = $this->db->count_all_results();

				    $detail = "NO Aktivasi [".$activation_code."]";

				    $data_to_post['phone_number'] = $phone62;
					$data_to_post['masking'] = 0;
					$data_to_post['msg'] = $detail;

					$sms = $this->pushSms($data_to_post);	

						//save history push
					    $log_sms_queues['phone_number'] = $phone62;
						$log_sms_queues['subject'] = $subject;
						$log_sms_queues['detail'] = $detail;
						$log_sms_queues['keterangan'] = json_encode($sms); 
						$log_sms_queues['status'] = 1;
						$log_sms_queues['masking'] = 0;
						$log_sms_queues['created'] = date('Y-m-d H:i:s', NOW());
						
						$this->db->insert('log_sms_queues', $log_sms_queues);

					if($sms['status'] != 200){
						$response["message"] = "Nomer hp anda tidak bisa digunakan, gunakan nomer yang lain ";
		    			return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
					}

					if($sms['code'] == "0"){
						$response["message"] = "Data anda tidak lengkap";
		    			return $this->output->set_content_type('application/json')
						            ->set_status_header(200)
						            ->set_output(json_encode($response));
					}

		    		// if($cek > 3){
		    		// 	$response["message"] = "Mohon Maaf apabila anda masih belum menerima Kode Aktivasi pada no telepon, ".$this->input->post('phone')." Harap Menggunakan No telpon lain atau hubungi kami.";
		    		// 	return $this->output->set_content_type('application/json')
						  //           ->set_status_header(200)
						  //           ->set_output(json_encode($response));
		    		// }
		    		// else {

		    			$param =   $this->db->select('param_value')
											->where('id', 3)
											->from('params')
											->get()
											->row()
											->param_value;

			    		
						$log_otp_request['phone'] = $data['phone'];
						$log_otp_request['otp'] = $activation_code;
						$log_otp_request['idgcm'] = "";
						$log_otp_request['imei'] = $data['imei'];
						$log_otp_request['iccid'] = $data['iccid']; 
						$log_otp_request['device_os'] = $data['device_os'];
						$log_otp_request['expired_date'] = Carbon::now()->addMinutes($param)->toDateTimeString();
						$log_otp_request['created_date'] = date('Y-m-d H:i:s', NOW());
						
						$this->db->insert('log_otp_request', $log_otp_request);

						// $sms_queues['phone_number'] = $phone62;
						// $sms_queues['subject'] = $subject;
						// $sms_queues['detail'] = $detail;
						// $sms_queues['keterangan'] = $keterangan; 
						// $sms_queues['status'] = 1;
						// $sms_queues['masking'] = 0;
						// $sms_queues['created'] = date('Y-m-d H:i:s', NOW());
						
						// $this->db->insert('sms_queues', $sms_queues);

						//delete dummy
						$this->db->delete('wilayah_detail_dummys', ['phone' => $data['phone']] ); 

						$wilayah_detail_dummys['name'] = $data['name'];
						$wilayah_detail_dummys['phone'] = $data['phone'];
						$wilayah_detail_dummys['g_route'] = $data['g_route_ts'];
						$wilayah_detail_dummys['g_route_toko'] = $data['g_route_toko'];
						$wilayah_detail_dummys['adm_area_level_1'] = $adm_area_level_1; 
						$wilayah_detail_dummys['adm_area_level_2'] = $adm_area_level_2;
						$wilayah_detail_dummys['adm_area_level_3'] = $adm_area_level_3;
						$wilayah_detail_dummys['adm_area_level_4'] = $adm_area_level_4;
						$wilayah_detail_dummys['adm_area_level_1_toko'] = $adm_area_level_1_toko; 
						$wilayah_detail_dummys['adm_area_level_2_toko'] = $adm_area_level_2_toko;
						$wilayah_detail_dummys['adm_area_level_3_toko'] = $adm_area_level_3_toko;
						$wilayah_detail_dummys['adm_area_level_4_toko'] = $adm_area_level_4_toko;
						$wilayah_detail_dummys['lat'] = $data['latitude_ts'];
						$wilayah_detail_dummys['lng'] = $data['longitude_ts'];
						$wilayah_detail_dummys['lat_toko'] = $data['latitude_toko'];
						$wilayah_detail_dummys['lng_toko'] = $data['longitude_toko'];
						$wilayah_detail_dummys['idgcm'] = "";
						$wilayah_detail_dummys['imei'] = $data['imei'];
						$wilayah_detail_dummys['iccid'] = $data['iccid'];
						$wilayah_detail_dummys['imsi'] = $data['imsi'];
						$wilayah_detail_dummys['address'] = $data['address_ts'];
						$wilayah_detail_dummys['address_toko'] = $data['address_toko'];
						$wilayah_detail_dummys['foto'] = $image;
						$wilayah_detail_dummys['created'] = date('Y-m-d H:i:s', NOW());
						
						$this->db->insert('wilayah_detail_dummys', $wilayah_detail_dummys);
		    		// }
		    		
		    	} catch (Exception $e) {
		    		 $this->db->trans_rollback();
					 $response["message"] = "terjadi kesalahan pada database (rollback)";
		    	}

		    	if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback();
					    $response["message"] = "terjadi kesalahan pada database (rollback)";
				}else {
				        $this->db->trans_commit();
				        $response["status"] = 'success';
				        $response["message"] = 'data berhasil disimpan';
				}
		    }else {
			    $response["message"] = 'nomer telepon sudah pernah diinputkan';
		    }

		} else {
			$response["message"] = (string) json_encode($this->form_validation->error_array());
		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	public function registerTukangSayur()
	{
		// $obj = new StdClass();
		$obj = [];
		$response = array("status" => 'failed', "data" => $obj, "message" => '');

		$_POST = json_decode(file_get_contents("php://input"), true);
		$data = $this->input->post();

		$this->form_validation->set_rules('otpcode', 'otpcode', 'required');
		$this->form_validation->set_rules('phone', 'phone', 'required');

		if ($this->form_validation->run() == TRUE)
		{


				$query =  $this->db->select('*')
							       ->where(['otp' => $data['otpcode'], 'phone' => $data['phone']])
								   ->from('log_otp_request')
								   ->get()
								   ->row();

				if($query == null){

					$response['message'] = 'Kode Aktivasi tidak ditemukan, silahkan ulangi proses registrasi';
					return $this->output->set_content_type('application/json')
							            ->set_status_header(200)
							            ->set_output(json_encode($response));

				}

			    $check_no_phone =  $this->db->select('id')
							       ->where('phone', $query->phone)
								   ->from('wilayah_details')
								   ->count_all_results();

			    if($check_no_phone > 0){

			    	$response["message"] = 'nomer telepon sudah pernah diinputkan, silahkan ulangi proses registrasi';
			    	return $this->output->set_content_type('application/json')
							            ->set_status_header(200)
							            ->set_output(json_encode($response));

			    }

				$date = Carbon::parse($query->expired_date);
			    $now = Carbon::now();
			    $diff = $date->diffInMinutes($now);

				$id = $query->id;		

			    if($diff < 0){

			    	$response['message'] = 'Kode Aktivasi telah expired';

			    }else{

			    	$dummy =  $this->db->select('*')
							       ->where('phone', $query->phone)
								   ->from('wilayah_detail_dummys')
								   ->get()
								   ->row();

					if($dummy == null){

						$response['message'] = 'Phone tidak ditemukan, silahkan ulangi proses registrasi';
						return $this->output->set_content_type('application/json')
								            ->set_status_header(200)
								            ->set_output(json_encode($response));

				    }

				    $phone62 = '62'.substr($data['phone'], 1);
					$subject = 'kata sandi Kedaisayur';
					$activation_code = rand(1111, 9999);
					$keterangan = "Jika anda tidak menerima kata sandi baru dalam waktu 1 menit Harap Menghubungi kami.";
				    $detail = "kata sandi [".$activation_code."]";

				    $this->db->trans_begin();
			    	try {

			    		$data_to_post['phone_number'] = $phone62;
						$data_to_post['masking'] = 0;
						$data_to_post['msg'] = $detail;

			    		$sms = $this->pushSms($data_to_post);	

						//save history push
					    $log_sms_queues['phone_number'] = $phone62;
						$log_sms_queues['subject'] = $subject;
						$log_sms_queues['detail'] = $detail;
						$log_sms_queues['keterangan'] = json_encode($sms); 
						$log_sms_queues['status'] = 1;
						$log_sms_queues['masking'] = 0;
						$log_sms_queues['created'] = date('Y-m-d H:i:s', NOW());
						
						$this->db->insert('log_sms_queues', $log_sms_queues);

						
						if($sms['status'] != 200){
							$response["message"] = "Nomer hp anda tidak bisa digunakan, gunakan nomer yang lain ";
			    			return $this->output->set_content_type('application/json')
							            ->set_status_header(200)
							            ->set_output(json_encode($response));
						}

						if($sms['code'] == "0"){
							$response["message"] = "Data anda tidak lengkap";
			    			return $this->output->set_content_type('application/json')
							            ->set_status_header(200)
							            ->set_output(json_encode($response));
						}

			            $data_x = array(
							'phone' => $dummy->phone,
							'registered_date' => date('Y-m-d H:i:s', NOW())
						);

						$this->db->where('id', $query->id);
						$this->db->where('otp', $data['otpcode']);
						$this->db->update('log_otp_request', $data_x);

						// $sms_queues['phone_number'] = $phone62;
						// $sms_queues['subject'] = $subject;
						// $sms_queues['detail'] = $detail;
						// $sms_queues['keterangan'] = $keterangan; 
						// $sms_queues['status'] = 1;
						// $sms_queues['masking'] = 0;
						// $sms_queues['created'] = date('Y-m-d H:i:s', NOW());
						// $this->db->insert('sms_queues', $sms_queues);

						$wilayah_details['password'] = md5($activation_code);
						$wilayah_details['title'] = $dummy->name;
						$wilayah_details['phone'] = $dummy->phone;
						$wilayah_details['g_route'] = $dummy->g_route; 
						$wilayah_details['adm_area_level_1'] = $dummy->adm_area_level_1; 
						$wilayah_details['adm_area_level_2'] = $dummy->adm_area_level_2;
						$wilayah_details['adm_area_level_3'] = $dummy->adm_area_level_3;
						$wilayah_details['adm_area_level_4'] = $dummy->adm_area_level_4;
						$wilayah_details['lat'] = $dummy->lat;
						$wilayah_details['lng'] = $dummy->lng;
						$wilayah_details['address'] = $dummy->address;
						$wilayah_details['country'] = 'Indonesia';
						$wilayah_details['created'] = date('Y-m-d H:i:s', NOW());
						$this->db->insert('wilayah_details', $wilayah_details);
						$wilayah_detail_id = $this->db->insert_id();

						$wilayah_address_details['pic'] = $dummy->name;
						$wilayah_address_details['title'] = "Lapak ".$dummy->name;
						$wilayah_address_details['wilayah_detail_id'] = $wilayah_detail_id;
						$wilayah_address_details['phone'] = $dummy->phone;
						$wilayah_address_details['g_route'] = $dummy->g_route_toko; 
						$wilayah_address_details['adm_area_level_1'] = $dummy->adm_area_level_1_toko; 
						$wilayah_address_details['adm_area_level_2'] = $dummy->adm_area_level_2_toko;
						$wilayah_address_details['adm_area_level_3'] = $dummy->adm_area_level_3_toko;
						$wilayah_address_details['adm_area_level_4'] = $dummy->adm_area_level_4_toko;
						$wilayah_address_details['lat'] = $dummy->lat_toko;
						$wilayah_address_details['lng'] = $dummy->lng_toko;
						$wilayah_address_details['address'] = $dummy->address_toko;
						$wilayah_address_details['created'] = date('Y-m-d H:i:s', NOW());
						$wilayah_address_details['is_primary'] = 1;
						$this->db->insert('wilayah_address_details', $wilayah_address_details);

						$ts_attachments['filename'] = $dummy->foto;
						$ts_attachments['wilayahdetail_id'] = $wilayah_detail_id;
						$ts_attachments['status'] = 1; 
						$ts_attachments['ts_attachment_cat_id'] = 2; //ktp 
						$ts_attachments['created'] = date('Y-m-d H:i:s', NOW());
						$this->db->insert('ts_attachments', $ts_attachments);
			    		
			    	} catch (Exception $e) {
			    		 $this->db->trans_rollback();
						 $response["message"] = "terjadi kesalahan pada database (rollback)";
			    	}

			    	if ($this->db->trans_status() === FALSE){
						    $this->db->trans_rollback();
						    $response["message"] = "terjadi kesalahan pada database (rollback)";
					}else {
					        $this->db->trans_commit();
					        $response["status"] = 'success';
					        $response["data"] = $activation_code;
					        $response["message"] = 'data berhasil disimpan';
					}
				}

		} else {
			$response["message"] = (string) json_encode($this->form_validation->error_array());
		}

		return $this->output->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode($response));
	}

	protected function saveFoto($foto, $phone){
      
		// $destinationPath = './uploads/foto/'. (string) $phone .'/';
        $destinationPath = 	'../sayur/app/webroot/files/TsAttachment/';
        if (!is_dir($destinationPath))
            {
                mkdir($destinationPath, 0777, true);
            }
		$img = str_replace('data:image/png;base64,', '', $foto);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$file = uniqid() . '.JPG';
		$success = file_put_contents( $destinationPath.$file, $data);
		
		return  $file;

    }

}
