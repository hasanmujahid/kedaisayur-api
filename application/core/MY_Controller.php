<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Carbon\Carbon;

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        //dev
        // $this->base_url = "http://devs.kedaisayur.com/";

        //prod
        // $this->base_url = "http://149.129.220.241/";
         $this->base_url = "http://devs.kedaisayur.com/";

        //status order new
        $this->order_code_new              = 'OS01';

        //status TS
        $this->order_code_success          = 'OS03';
        $this->order_receive_pool          = 'OS16';
        $this->order_in_pool               = 'OS09';
        $this->order_on_proses             = 'OS05';
        $this->order_on_packaging          = 'OS07';
        $this->order_sent_to_pool          = 'OS08';
        $this->order_waiting_payment       = 'OS04';
        $this->order_code_pick             = 'OS10';
        $this->order_code_sent             = 'OS02';
        $this->order_code_sent_success     = 'OS11';
        $this->order_code_receive_customer = 'OS12';
        $this->order_code_cancel           = 'OS14';

        //end point
        $this->endpoint_sms = $this->base_url.'sayur/crons/smsDirect';

        //map
        $this->key_map = "AIzaSyCr_WwMbamhTyOhpHXRV9LwWy58QtABbvI";

        //base_url
        $this->base_url_product        = $this->base_url."sayur/app/webroot/files/product/";
        $this->base_url_attachment     = $this->base_url."sayur/app/webroot/files/TsAttachment/";
        $this->base_url_categories     = $this->base_url."sayur/app/webroot/files/category/";
        $this->base_url_package        = $this->base_url."sayur/app/webroot/files/paket/";
        $this->base_url_promo        = $this->base_url."sayur/app/webroot/files/Promo/";

        //midtrans
        $this->SERVER_KEY   = "SB-Mid-server-vFcDo61R7k03YVnnFYCK6HVU";
        $this->ENDPOINT     = "https://app.sandbox.midtrans.com/snap/v1/transactions";

        $this->list_payment_now = array(4);  //4 = indomaret

        $this->payment_type_hutang = array(5,7);

        //$list_code_customer = array('OS01', 'OS05', 'OS07', 'OS08', 'OS09', 'OS02', 'OS10',  'OS11', 'OS16');
        $this->code_pending_customer = array($this->order_code_new,
                                              $this->order_code_pick,
                                              $this->order_on_proses,
                                              $this->order_on_packaging,
                                              $this->order_sent_to_pool,
                                              $this->order_in_pool,
                                              $this->order_code_sent,
                                              $this->order_code_pick,
                                              $this->order_code_sent_success,
                                              $this->order_receive_pool,
                                              $this->order_code_receive_customer
                                              );

        $this->code_pending_hutang = array( $this->order_code_sent_success,
                                            $this->order_code_receive_customer
                                          );

        // $this->list_code_ts = array('OS01', 'OS05', 'OS07', 'OS08', 'OS09');
        // $this->list_code_ts = array('OS01', 'OS05', 'OS07', 'OS08', 'OS09', 'OS016');

         $this->list_code_ts = array( $this->order_code_new,
                                      $this->order_on_proses,
                                      $this->order_on_packaging,
                                      $this->order_sent_to_pool,
                                      $this->order_in_pool,
                                      $this->order_receive_pool,
                                      $this->order_waiting_payment,
                                      $this->order_code_pick
                                    );

         $this->minute_expired = 60;

         $this->status_code_pending = ['201', '100'];

         //DOKU

         //Dev
         //alfamart code
         $this->alfamart_code = '88888884';
         $this->post_mallId = '6752';
         $this->post_sharedKey = 'Y5DCTenyb256';

    }


    function autonumber_q()
    {
            $awalan  = "Q";
            $lebar = 6;

            $hasil=  $this->db->query("select id from t_order order by id desc limit 1");
            $jumlahrecord = $hasil->num_rows();
            if($jumlahrecord == 0)
            $nomor=1;
            else
            {
                    foreach ($hasil->result() as $rows){
                         $row['id'] = $rows->id;
                    }
                    $nomor=intval(substr($row['id'],strlen($awalan)))+1;
            }
            if($lebar>0)
                    $angka = $awalan.str_pad($nomor,$lebar,"0",STR_PAD_LEFT);
            else
            $angka = $awalan.$nomor;

            return $angka;
    }

    public function getAddress($lat, $long)
    {
        $key=$this->key_map;
        $latlong = $lat.",".$long;
        $url="https://maps.googleapis.com/maps/api/geocode/json?latlng=".$latlong."&key=".$key;
        $address = "";
        $result = file_get_contents($url);
        $x= json_decode($result, true);
        if($x['status'] == "OK"){
            $address= $x['results'][0]['address_components'];
            return $address;
        }
        else{
            return $address;
        }
    }

    public function checkToken ($token)
    {

        $query =   $this->db->select(['wilayah_address_details.id as wilayah_address_details_id', 'wilayah_address_details.wilayah_id as wilayah_id', 'wilayah_details.*'])
                            ->from('wilayah_details')
                            ->join('wilayah_address_details', 'wilayah_address_details.wilayah_detail_id = wilayah_details.id AND wilayah_address_details.is_primary = 1 and wilayah_address_details.status=1', 'inner')
                            ->where(['wilayah_details.token' => $token,
                                     'wilayah_address_details.is_primary' => 1])
                            ->get()
                            ->row();

        $result = array('tokenStatus' => '');

        if ($query ==  null) {
            $result['tokenStatus'] = "Invalid token";
            return $result;
        } elseif (Carbon::now() >= $query->token_expired) {
            $result['tokenStatus'] = "Token expired";
            return $result;
        } elseif ($query->status == 0) {
            $result['tokenStatus'] = "Account Deleted";
            return $result;
        } else{

            $result['data'] = $query;
            return $result;
        }
    }

    public function random_num($size)
    {
       return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $size);
    }

    public function convert_km($lat, $long, $lat2, $long2)
    {
        $theta = $long - $long2;
        $dist = sin(deg2rad($lat)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        return number_format($miles);

    }

    public function pushSms($data_to_post){
        // Sets our destination URL
        $endpoint_url = $this->endpoint_sms;

        $options = [
            CURLOPT_URL        => $endpoint_url,
            CURLOPT_POST       => true,
            CURLOPT_POSTFIELDS => $data_to_post,
            CURLOPT_RETURNTRANSFER => true,
        ];

       // Initiates the cURL object
        $curl = curl_init();
        curl_setopt_array($curl, $options);

        $results = curl_exec($curl);

        // Be kind, tidy up!
        curl_close($curl);

        return json_decode($results, true);
    }

    public function formatRp($value){

        $result = "Rp " . number_format($value,0,',','.');
        return $result;

    }

    public function sisaKomisi($id){
        $sisa_komisi = 0;
        $total_komisi = 0;
        $get_sisa_komisi = $this->db->select(['komisi_histories.total',
                                              'komisi_histories.id',
                                              'komisi_histories.created'])
                                        ->from('komisi_histories')
                                        ->where(['komisi_histories.wilayah_detail_id' => $id])
                                        ->order_by('komisi_histories.id', 'desc')
                                        ->get()
                                        ->row();

        if($get_sisa_komisi){
            $sisa_komisi = $get_sisa_komisi->total;
        }

        return $sisa_komisi;
    }

    public function sisaPlafon($id){
        //saldo
        $sisa_plafon = 0;
        $get_sisa_plafon = $this->db->select(['plafon_histories.total',
                                                      'plafon_histories.id',
                                                      'plafon_histories.total',
                                                      'plafon_histories.created',
                                                      'ts_grades.plafon_to',
                                                      'ts_grades.plafon_from'])
                                            ->from('plafon_histories')
                                            ->join('ts_grades', 'ts_grades.id=plafon_histories.ts_grade_id', 'inner')
                                            ->where(['plafon_histories.wilayah_detail_id' => $id])
                                            ->order_by('plafon_histories.id', 'desc')
                                            ->get()
                                            ->row();

                if($get_sisa_plafon){
                    $sisa_plafon = $get_sisa_plafon->total;
                }

        return $sisa_plafon;

    }

    public function plafonDibayar($id){
        //saldo
        $sisa_plafon = 0;
        $get_sisa_plafon = $this->db->select(['plafon_histories.total',
                                                      'plafon_histories.id',
                                                      'plafon_histories.total',
                                                      'plafon_histories.created',
                                                      'ts_grades.plafon_to',
                                                      'ts_grades.plafon_from'])
                                            ->from('plafon_histories')
                                            ->join('ts_grades', 'ts_grades.id=plafon_histories.ts_grade_id', 'inner')
                                            ->where(['plafon_histories.wilayah_detail_id' => $id])
                                            ->order_by('plafon_histories.id', 'desc')
                                            ->get()
                                            ->row();

                if($get_sisa_plafon){
                    $sisa_plafon = $get_sisa_plafon->plafon_to - $get_sisa_plafon->total;
                }

        return $sisa_plafon;

    }

    function selisih_date($x, $s)
    {
        $start = strtotime($x);
        $end   = strtotime($s);
        $diff  = $end - $start;

        $hours = floor($diff / (60 * 60));
        $minutes = $diff - $hours * (60 * 60);
        return  $hours .  ' Jam, ' . floor( $minutes / 60 ) . ' Menit';
    }

    //edit by hasan 21-12-2018
    function get_ts_order_hutang($id)
    {
      $detail_ts_order = $this->db->select(['orders.id as order_id',
                        'orders.created',
                        'IFNULL(orders.totalAmountInCart, 0) as totalcart',
                        '(IFNULL(orders.totalAmountInCart, 0) - IFNULL(orders.komisi, 0) - IFNULL(orders.nominal_promo, 0) + IFNULL(orders.ongkir, 0))  as totalAmountInCart',
                        'wilayah_details.title',
                        'orders.app_code',
                        'IFNULL(orders.ongkir, 0) as ongkir',
                        'IFNULL(orders.nominal_promo, 0) as nominal_promo'])
                     ->from('orders')
                     ->where(['member_id' => $id,
                        'app_code' => 2])
                     ->where_in('orders.payment_type_id', $this->payment_type_hutang)
                     ->where_in('order_status_code', $this->list_code_ts)
                     ->join('wilayah_details', 'wilayah_details.id = orders.member_id', 'inner')
                     ->get();

    return $detail_ts_order;
    }

  //  function get_cs_pending_hutang($id)
  //  {
  //    $customer_pending_order = $this->db->select(['IFNULL(orders.totalAmountInCart, 0) as totalAmountInCart',
    //                                 'IFNULL(orders.ongkir, 0) as ongkir',
    //                                 'orders.id as order_id'])
    //                   ->from('orders')
    //                   ->where(['orders.wilayah_detail_id' => $id,
    //                      'orders.app_code' => 1,
    //                      'orders.payment_type_id' => 1])
    //                   ->where_in('orders.order_status_code', $this->code_pending_hutang)
    //                   ->get();

    // return $customer_pending_order;
  //  }


    //edit by hasan 21-12-2018
    function get_cs_pending_hutang($id) //ini yg make fungsi siapa aja?
    {
      $customer_pending_order = $this->db->select(['IFNULL(orders.totalAmountInCart, 0) as totalAmountInCart',
                                     'IFNULL(orders.ongkir, 0) as ongkir',
                                     'IFNULL(orders.nominal_promo, 0) as nominal_promo',
                                     'CAST(( (sum(order_details.profit_ts)) + ((orders.ongkir + orders.ongkir_tambahan) * orders.index_ongkir_ts) ) AS SIGNED) as komisi',
                                     'orders.id as order_id'])
                       ->from('orders')
                       ->join('order_details', 'order_details.order_id = orders.id and order_details.status_detail = 1', 'inner')
                       ->where(['orders.wilayah_detail_id' => $id,
                          'orders.app_code' => 1,
                          'orders.payment_type_id' => 1])
                       ->where_in('orders.order_status_code', $this->code_pending_hutang)
                       ->group_by('order_details.order_id')
                       ->get();

      return $customer_pending_order;
    }

    function total_ts_order_hutang($id)
    {
      $customer_pending_order = $this->db->select(['IFNULL( ( sum(orders.totalAmountInCart) + sum(orders.ongkir)) - sum(IFNULL(orders.komisi,0)) - sum(IFNULL(orders.nominal_promo,0)), 0) as total'])
                       ->from('orders')
                       ->where(['orders.member_id' => $id,
                          'orders.app_code' => 2])
                       ->where_in('orders.payment_type_id', $this->payment_type_hutang)
                         ->where_in('order_status_code', $this->list_code_ts)
                       ->get()
                       ->row();

    return $customer_pending_order;
    }

}

?>
