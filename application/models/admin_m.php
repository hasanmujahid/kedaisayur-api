<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_m extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        
    }
	
	function template($filename, $data, $data1, $data2, $data3){
		$sess=$this->session->userdata('administrator');
		$dataX['email']=$sess['email'];
		$dataX['status']=$sess['status'];
		$this->load->view('controlPanel/administrator/template/dashboard/header');
		$this->load->view('controlPanel/administrator/template/menu', $dataX);
		$this->load->view('controlPanel/administrator/template/'.$sess['status'], $data1);
		$this->load->view('controlPanel/administrator/'.$filename, $data);
		$this->load->view('controlPanel/administrator/template/dashboard/footer', $data3);
		$this->load->view('controlPanel/administrator/'.$data2['js'], $data3);
	}
	function template2($filename, $data, $data1, $data2, $data3){
		$sess=$this->session->userdata('administrator');
		$dataX['email']=$sess['email'];
		$dataX['status']=$sess['status'];
		$this->load->view('administrator/template/table/header');
		$this->load->view('administrator/template/menu', $dataX);
		$this->load->view('administrator/template/'.$sess['status'], $data1);
		$this->load->view('administrator/'.$filename, $data);
		$this->load->view('administrator/template/table/footer', $data3);
		$this->load->view('administrator/'.$data2['js'], $data3);
	}
	public function get_login_admin2($title, $class, $class1)
	{
		$session_data = $this->session->userdata('administrator');
		$data['status'] = $session_data['status'];
		if(!$this->session->userdata('administrator'))
		{
			redirect('admins/login', 'refresh');
		}
		
		else if($data['status']!='administrator')
		{
			redirect('admins/login', 'refresh');
		}
		else
		{
			 $data['email'] = $session_data['email'];
			 $data['status'] = $session_data['status'];
			 $data['id'] = $session_data['id_administrator'];
			 $data['active'] = 'active';
			 $data['title']=$title;
			 $data['class']=$class;
			 $data['class1']=$class1;
			 return $data;
		}

	}
	
	function edit_service()
	{
		$data_x=array(
			'id_service' => $this->input->post('id_service'),
			'service' => $this->input->post('service_detail'),
			'price' => $this->input->post('price'),
			'primary' => $this->input->post('primary'),
			'duration' => $this->input->post('duration'),
		);
		$this->db->where('id', $this->input->post('id'));
		
		if($this->db->update('t_service_detail', $data_x))
		{ return true; } else { return false; }
	}
	
	function edit_general_service()
	{
		$data_x=array(
			'service' => $this->input->post('service'),
			'detail1' => $this->input->post('detail1'),
		);
		$this->db->where('id', $this->input->post('id'));
		
		if($this->db->update('t_service', $data_x))
		{ return true; } else { return false; }
	}
	function add_terapis($image)
	{
		$com_code = md5(uniqid(rand()));
		$data_x = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'password' => md5($this->input->post('password')),
			'phone' => $this->input->post('phone'), 
			'gender' => $this->input->post('gender'),
			'address' => $this->input->post('address'),
			'com_code' => $com_code,
			'id_service' => $this->input->post('id_service'),
			'os' => 'ANDROID',
			'status' => 'available',
			'image' => 'assets/web/images/terapis/'.$image,
			'active' => 'active',
			'created' => date('d-m-Y', NOW())
		);
		if($this->db->insert('t_terapis', $data_x))$val=true;
		
		$x=$this->db->query("select id from t_terapis where email='".$this->input->post('email')."' ");
		foreach($x->result() as $t){}
		$count =count($this->input->post('city'));
		$city=$this->input->post('city');
		if($count > 0)
			{
				for($i=0; $i<$count; $i++)	
				{
					$data_x2[] = array(
							'id_terapis' => $t->id,
							'id_city' => $city[$i],
						);
				}
				$this->db->insert_batch('t_terapis_available', $data_x2);
			}
		$count2 =count($this->input->post('service'));
		$service=$this->input->post('service');
		if($count2 > 0)
			{
				for($i=0; $i<$count2; $i++)	
				{
					$data_x3[] = array(
							'id_terapis' => $t->id,
							'id_service' => $service[$i],
						);
				}
				$this->db->insert_batch('t_terapis_service', $data_x3);
			}						
											/*
											$report='<table style="	border: 2px solid #EFEFEF; celpadding:10; cellspacing:0; width:600;" align="center">
												 <tr>
												  <td bgcolor="#f7f248" align="center"> 
														<img src="'.base_url().'assets/web/images/logo-obeijings3.png" alt="Obeijings Logo" width="288" height="130" style="display: block; padding-top:20px; padding-bottom:20px;" border="0" />
												  </td> 
												 </tr>
												 <tr>
												  <td bgcolor="#ffffff"  >
												  <center>
												  <h3>Anda telah kami daftarkan menjadi Terapis pad obeijings,</h3>
												 <br/><br/>
												  <p>Silahkan aktifkan email anda dengan meng-klik link di bawah lalu install aplikasi obeijings dan gunakan email & password berikut,</p>
												  
												   <h4><a href="'.base_url().'index/emailTerapisActivation'.'" target="_new"> Aktifkan Email </a></ht><br/>
												  
												  <h4>Email : '.$this->input->post('email').'</h4>
												  <h4>Password : '.$this->input->post('password').'</h4><br/>
												  <br/>
												  </td>
												 </tr>
												 <tr> 
												  <td bgcolor="#f7f248"> 
												  <table border="0" cellpadding="10" cellspacing="0" width="100%">
													 <tr>
													  <td style="color: #000000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px;">
														&reg; Obeijings, copyright 2016<br/>
														Subcribe newsletter <a targer="_new" href="'.base_url().'">O-Beijings</a> 
													  </td>
													  <td>
													   <td align="right">
														 <table border="0" cellpadding="0" cellspacing="0">
														  <tr>
														   <td>
															<a href="https://instagram.com/obeijings">
															 <img src="'.base_url().'assets/web/images/fb.png" alt="Instagram" width="38" height="38" style="display: block;" border="0" />
															</a>
														   </td>
														   <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
														   <td>
															<a href="https://www.facebook.com/obejings">
															 <img src="'.base_url().'assets/web/images/ig.png" alt="Facebook" width="38" height="38" style="display: block;" border="0" />
															</a>
														   </td>
														  </tr>
														 </table>
														</td>
													  </td>
													 </tr>
													</table>
												  </td>
												
												 </tr>
												</table>'
											  ;
											 
											 $ci = get_instance();
											 $ci->load->library('email');
											 $config['protocol'] = "smtp";
											 $config['smtp_host'] = "ssl://smtp.gmail.com";
											 $config['smtp_port'] = "465";
											 $config['smtp_user'] = "obeijings@gmail.com"; 
											 $config['smtp_pass'] = "obeijings123";
											 $config['charset'] = "utf-8";
											 $config['mailtype'] = "html";
											 $config['newline'] = "\r\n";
											 $ci->email->initialize($config);
											 $ci->email->from('obeijings@gmail.com');
											 $ci->email->to($this->input->post('email'));
											 $ci->email->subject('Obeijings Email Activation');
											 $ci->email->message($report);
											 $ci->email->send();
											 */
		if($val==true) return true;
		else return false;
	}
	
	function edit_terapis()
	{
		$data_x = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'), 
			'gender' => $this->input->post('gender'),
			'address' => $this->input->post('address'),
			'id_service' => $this->input->post('id_service')
		);
		$this->db->where('id', $this->input->post('id'));
		if($this->db->update('t_terapis', $data_x)) { $val=true; }
		
		$count =count($this->input->post('city'));
		$city=$this->input->post('city');
		if($count > 0)
			{
				$this->db->where('id_terapis', $this->input->post('id'));
				$this->db->delete('t_terapis_available');	
				for($i=0; $i<$count; $i++)	
				{
					$data_x2[] = array(
							'id_terapis' => $this->input->post('id'),
							'id_city' => $city[$i],
						);
				}
				$this->db->insert_batch('t_terapis_available', $data_x2);
			}	
		$count2 =count($this->input->post('service'));
		$service=$this->input->post('service');
		if($count2 > 0)
			{
				$this->db->where('id_terapis', $this->input->post('id'));
				$this->db->delete('t_terapis_service');	
				for($i=0; $i<$count2; $i++)	
				{
					$data_x3[] = array(
							'id_terapis' => $this->input->post('id'),
							'id_service' => $service[$i],
						);
				}
				$this->db->insert_batch('t_terapis_service', $data_x3);
			}	
			
		if($val==true) return true;
		else return false;
	}
}