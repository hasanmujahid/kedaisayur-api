<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_m extends CI_Model {
    
	
    public function __construct(){
        parent::__construct();
		 date_default_timezone_set('Asia/Jakarta');
        //parent::set_table('user','id_user');
    }
	
	function addCustomer($name, $email, $password, $phone , $gender, $os, $com_code)
	{
		
		$data_x = array(
			'name' => $name,
			'email' => $email,
			'password' => md5($password),
			'phone' => $phone, 
			'gender' => $gender, 
			'os' => $os,
			'com_code' => $com_code, 
			'created' => date('d-m-Y', NOW())
		);
		if($this->db->insert('t_customer', $data_x)) return true;
		else return false;
	}
	function addCustomerLogin($id_customer)
	{
		
		$data_x = array(
			'id_customer' => $id_customer,
			'date' => date('d-m-Y', NOW()),
			'time' => date('H:i:s', NOW()),
		); 
		if($this->db->insert('t_customer_login', $data_x)) return true;
		else return false;
	}
	function addTerapis($name, $email, $password, $phone, $gender, $service ,$os, $com_code)
	{
		$data_x = array(
			'name' => $name,
			'email' => $email,
			'password' => md5($password),
			'phone' => $phone, 
			'gender' => $gender,
			'com_code' => $com_code,
			'service' => $service,
			'os' => $os,
			'created' => date('d-m-Y', NOW())
		);
		if($this->db->insert('t_terapis', $data_x)) return true;
		else return false;
	}
	function addTerapisLogin($id_customer)
	{
		
		$data_x = array(
			'id_terapis' => $id_customer,
			'date' => date('d-m-Y', NOW()),
			'time' => date('H:i:s', NOW()),
		); 
		if($this->db->insert('t_terapis_login', $data_x)) return true;
		else return false;
	}
	function editCustomer($name, $email, $phone , $gender, $id)
	{
		
		$data_x = array(
			'name' => $name,
			'email' => $email,
			'phone' => $phone, 
			'gender' => $gender, 
		);
		$this->db->where('id', $id);
		if($this->db->update('t_customer', $data_x)) return true;
		else return false;
	} 
	
	function editPasswordCustomer($new_password, $id)
	{
		
		$data_x = array(
			'password' => md5($new_password),
		);
		$this->db->where('id', $id);
		if($this->db->update('t_customer', $data_x)) return true;
		else return false;
	}
	
	function editPasswordTerapis($new_password, $id)
	{
		
		$data_x = array(
			'password' => md5($new_password),
		);
		$this->db->where('id', $id);
		if($this->db->update('t_terapis', $data_x)) return true;
		else return false;
	}
	
	function editPasswordCustomer2($new_password, $email)
	{
		
		$data_x = array(
			'password' => md5($new_password),
		);
		$this->db->where('email', $email);
		if($this->db->update('t_customer', $data_x)) return true;
		else return false;
	}
	
	function addOrder($id_order, $id_customer, $address, $lat, $long, $a, $id_service_primary, $getArea, $time_order, $r_gender) 
	{
		$gen="";
		if($a['results'][0]['address_components'][$getArea[0]]['long_name']==null)
		{
			$prov='0';
		}
		else{
			$prov=$a['results'][0]['address_components'][$getArea[0]]['long_name'];
		}
		if($id_service_primary==1){
			$gen=$r_gender;
		}
		$data_x = array(
			'id' => $id_order,
			'id_customer' => $id_customer,
			'time_order' => $time_order,
			'r_gender' => $gen,
			'date' => date('d-m-Y', NOW()),
			'time' => date('H:i', NOW()),
			'lattitude' => $lat,
			'longitude' => $long,
			'address' => $address,
			'id_service_primary' => $id_service_primary,
			'address_convert' => $a['results'][0]['formatted_address'],
			'province' => $prov,
			'city' => $a['results'][0]['address_components'][$getArea[1]]['long_name'],
			'districs' => $a['results'][0]['address_components'][$getArea[2]]['long_name'],
		);
		if($this->db->insert('t_order', $data_x)) return true;
		else return false;
	}
	
	function addOrderDetail($id_order, $id_service, $id_service_additional, $price, $price_additional, $duration1, $duration2)
	{
		$count =count($id_service_additional);
		$query1=$this->db->query("select price, duration from t_service_detail where id='".$id_service."' ");	
		
		foreach($query1->result() as $q1)
		
		if($q1->duration==0){
			$main_price=$q1->price;
		}else{
			$main_price= ($duration1/$q1->duration)*$q1->price;
		}
		$data_x = array(
			'id_order' => $id_order,
			'id_service' => $id_service,
			'price' => $main_price,
			'duration' => $duration1,
		);
			if($id_service_additional!=null)
			{
			if($count > 0)
			{
				for($i=0; $i<$count; $i++)	
				{
					$query=$this->db->query("select price, duration from t_service_detail where id='".$id_service_additional[$i]."' ");
					foreach($query->result() as $q)
					$data_x2[] = array(
							'id_order' => $id_order,
							'id_service' => $id_service_additional[$i],
							'price' => $q->price,
							'duration' => $q->duration,
						);
				}
				$this->db->insert_batch('t_order_detail', $data_x2);
			}
			} 
		if($this->db->insert('t_order_detail', $data_x)) return true;
		else return false;
	}
	function addCustomerRating($rating, $comment, $id_order)
	{
		$data_x = array(
			'id_order' => $id_order,
			'rating' => $rating,
			'comment' => $comment,
			'date' => date('d-m-Y', NOW()),
			'time' => date('H:i:s', NOW()),
		);
		if($this->db->insert('t_customer_rating', $data_x)) return true;
		else return false;
	}
	function addTerapisRating($rating, $comment, $id_order)
	{
		$data_x = array(
			'id_order' => $id_order,
			'rating' => $rating,
			'comment' => $comment,
			'date' => date('d-m-Y', NOW()),
			'time' => date('H:i:s', NOW()),
		);
		if($this->db->insert('t_terapis_rating', $data_x)) return true;
		else return false;
	}
	
	function updateGCM($id, $gcm)
	{
		$data_x = array(
			'gcm' => $gcm,
		);
		$this->db->where('id', $id);
		if($this->db->update('t_terapis', $data_x)) return true;
		else return false;
	}
	
	function updateToken($id, $token)
	{
		$data_x = array(
			'token' => $token,
		);
		$this->db->where('id', $id);
		if($this->db->update('t_customer', $data_x)) return true;
		else return false;
	}
	
	function updateComCode($com_code)
	{
		$data_x = array(
			'com_code' => '0',
		);
		$this->db->where('com_code', $com_code);
		if($this->db->update('t_customer', $data_x)) return true;
		else return false;
	}
	
	function editTerapis($name, $email, $phone , $gender, $id, $id_service)
	{
		
		$data_x = array(
			'name' => $name,
			'email' => $email,
			'phone' => $phone, 
			'gender' => $gender, 
			'id_service' => $id_service, 
		);
		$this->db->where('id', $id);
		if($this->db->update('t_terapis', $data_x)) return true;
		else return false;
	}
	function cancelOrder($id_order)
	{
			$data_x = array('status' => '5',); 
			$this->db->where('id', $id_order);
			if($this->db->update('t_order', $data_x)) return true;
			else return false;
			   
	}
	
	function editCity($id_terapis, $id_city)
	{
		$count =count($this->input->post('id_city'));
		$city=$this->input->post('id_city');
		if($count > 0)
			{
				$this->db->where('id_terapis', $id_terapis);
				$this->db->delete('t_terapis_available');	
				for($i=0; $i<$count; $i++)	
				{
					$data_x2[] = array(
							'id_terapis' => $id_terapis,
							'id_city' => $city[$i],
						);
				}
				$this->db->insert_batch('t_terapis_available', $data_x2);
				return true;
			}
		else return false;

	}
	
	function editAvailableService($id_terapis, $id_service)
	{
		$count =count($this->input->post('id_service'));
		$service=$this->input->post('id_service');
		if($count > 0)
			{
				$this->db->where('id_terapis', $id_terapis);
				$this->db->delete('t_terapis_service');	
				for($i=0; $i<$count; $i++)	
				{
					$data_x2[] = array(
							'id_terapis' => $id_terapis,
							'id_service' => $service[$i]
						);
				}
				$this->db->insert_batch('t_terapis_service', $data_x2);
				return true;
			}
		else return false;

	}
	
	function updateStatusTerapis($id_terapis, $id_order, $status)
	{
		$this->db->where('id', $id_terapis);
		$data_x=array('status' => $status, 'id_order' => $id_order);
		if($this->db->update('t_terapis', $data_x)) { return true;}
		else{ return false;}
	}
	
}
?>